# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

## [Unreleased]

---

## [0.2.0] - 2020-03-30

### Changed
- Upgrade to PHP 8.0

---

## [0.1.0] - 2021-03-28

Last version compatible with PHP 7
