<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\InfoExtractor\TestTraits;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;

trait TestInfoExtractorTrait
{
    public function buildTestExtractor(): InfoExtractor
    {
        return new class () implements InfoExtractor {
            public function getInfo(string $filePath): UniversalCollection
            {
                $result = UniversalCollection::createEmpty();
                $result->add('filesize', filesize($filePath));
                $result->add('filename', basename($filePath));
                return $result;
            }

            public function getInfoSummary(string $filePath): UniversalCollection
            {
                $result = UniversalCollection::createEmpty();
                $result->add('filename', basename($filePath));
                return $result;
            }
        };
    }
}
