<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\InfoExtractor;

use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Smbx2\Smbx2LevelInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Fileinfo\InfoExtractor\Factory\InfoExtractorSupplier;
use FlyingAnvil\Fileinfo\InfoExtractor\SmartInfoExtractor;
use FlyingAnvil\Fileinfo\Test\InfoExtractor\TestTraits\TestInfoExtractorTrait;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/**
 * @covers \FlyingAnvil\Fileinfo\InfoExtractor\SmartInfoExtractor
 */
class SmartInfoExtractorTest extends TestCase
{
    /** @var ContainerInterface */
    private $container;

    /** @var InfoExtractorSupplier */
    private $supplier;

    use TestInfoExtractorTrait;

    protected function setUp(): void
    {
        $this->container = $this->createMock(ContainerInterface::class);
        $this->supplier  = new InfoExtractorSupplier($this->container);

        $this->container->expects($this->once())->method('get')
            ->with(Smbx2LevelInfoExtractor::class)->willReturn($this->buildTestExtractor());
    }

    public function testCanConfigureCustomExtractorsForExtension(): void
    {
        $this->supplier->addExtensionDefinition(
            FileExtension::createFromString('txt'),
            Smbx2LevelInfoExtractor::class,
        );

        $filePath = __DIR__ . '/_files/text.txt';

        $smart = new SmartInfoExtractor($this->supplier);
        $info  = $smart->getInfoByExtension($filePath);

        self::assertSame([
            'filesize' => 20,
            'filename' => 'text.txt',
        ], $info->toArray());
    }

    public function testCanConfigureCustomExtractorsForContext(): void
    {
        $context  = Context::createFromKnown(Context::DOCUMENT_TEXT_PLAIN);
        $this->supplier->addContextDefinition(
            $context,
            Smbx2LevelInfoExtractor::class,
        );

        $filePath = __DIR__ . '/_files/text.txt';

        $smart = new SmartInfoExtractor($this->supplier);
        $info  = $smart->getInfoByContext($filePath, $context);

        self::assertSame([
            'filesize' => 20,
            'filename' => 'text.txt',
        ], $info->toArray());
    }

    public function testCanExtractSummaryByExtension(): void
    {
        $this->supplier->addExtensionDefinition(
            FileExtension::createFromString('txt'),
            Smbx2LevelInfoExtractor::class,
        );

        $filePath = __DIR__ . '/_files/text.txt';

        $smart = new SmartInfoExtractor($this->supplier);
        $info  = $smart->getInfoSummaryByExtension($filePath);

        self::assertSame([
            'filename' => 'text.txt',
        ], $info->toArray());
    }

    public function testCanExtractSummaryByContext(): void
    {
        $context  = Context::createFromKnown(Context::DOCUMENT_TEXT_PLAIN);
        $this->supplier->addContextDefinition(
            $context,
            Smbx2LevelInfoExtractor::class,
        );

        $filePath = __DIR__ . '/_files/text.txt';

        $smart = new SmartInfoExtractor($this->supplier);
        $info  = $smart->getInfoSummaryByContext($filePath, $context);

        self::assertSame([
            'filename' => 'text.txt',
        ], $info->toArray());
    }
}
