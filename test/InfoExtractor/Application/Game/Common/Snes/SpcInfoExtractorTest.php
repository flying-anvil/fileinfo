<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\InfoExtractor\Application\Game\Common\Snes;

use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Snes\SpcInfoExtractor;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\Fileinfo\InfoExtractor\Application\Game\Common\Snes\SpcInfoExtractor
 */
class SpcInfoExtractorTest extends TestCase
{
    public function testCanGetInfo(): void
    {
        $spcFile = __DIR__ . '/_files/At Dooms Gate (E1M1) - DOOM.spc';

        if (!file_exists($spcFile)) {
            self::markTestSkipped(sprintf(
                'File \'%s\' does not exist, put it into the appropriate place or ignore this!',
                $spcFile,
            ));

            return;
        }

        $extractor = new SpcInfoExtractor();
        $info      = $extractor->getInfo($spcFile);
        $expected  = json_decode(
            file_get_contents(__DIR__ . '/_files/expectedFullE1M1.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        self::assertSame($info->toArray(), $expected);
    }

    public function testCanGetInfoSummary(): void
    {
        $spcFile = __DIR__ . '/_files/At Dooms Gate (E1M1) - DOOM.spc';

        if (!file_exists($spcFile)) {
            self::markTestSkipped(sprintf(
                'File \'%s\' does not exist, put it into the appropriate place or ignore this!',
                $spcFile,
            ));

            return;
        }

        $extractor = new SpcInfoExtractor();
        $info      = $extractor->getInfoSummary($spcFile);
        $expected  = json_decode(
            file_get_contents(__DIR__ . '/_files/expectedSummaryE1M1.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        self::assertSame($info->toArray(), $expected);
    }

    public function testCannotGetInfoAboutInvalidSpc(): void
    {
        $extractor = new SpcInfoExtractor();
        $info      = $extractor->getInfo(__FILE__);

        self::assertEmpty($info->toArray());
    }
}
