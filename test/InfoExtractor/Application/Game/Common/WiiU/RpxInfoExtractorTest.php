<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\InfoExtractor\Application\Game\Common\WiiU;

use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\WiiU\RpxInfoExtractor;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\Fileinfo\InfoExtractor\Application\Game\Common\WiiU\RpxInfoExtractor
 */
class RpxInfoExtractorTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testCanGetInfo(string $rpxFile, array $expectedInfo): void
    {
        if (!file_exists($rpxFile)) {
            self::markTestSkipped(sprintf(
                'File \'%s\' does not exist, put it into the appropriate place or ignore this!',
                $rpxFile,
            ));

            return;
        }

        $extractor = new RpxInfoExtractor();
        $info      = $extractor->getInfo($rpxFile);

        self::assertSame($expectedInfo, $info->toArrayRecursive());
    }

    /**
     * @dataProvider provider
     */
    public function testCanGetSummary(string $rpxFile, array $expectedInfo): void
    {
        if (!file_exists($rpxFile)) {
            self::markTestSkipped(sprintf(
                'File \'%s\' does not exist, put it into the appropriate place or ignore this!',
                $rpxFile,
            ));

            return;
        }

        $extractor = new RpxInfoExtractor();
        $info      = $extractor->getInfoSummary($rpxFile);
        unset($expectedInfo['developmentInfos']);

        self::assertSame($expectedInfo, $info->toArrayRecursive());
    }

    /**
     * @noinspection JsonEncodingApiUsageInspection
     */
    public function provider(): array
    {
        return [
            [__DIR__ . '/_files/Turbo.rpx', json_decode(file_get_contents(__DIR__ . '/_files/expectedTurbo.json'), true)],
            [__DIR__ . '/_files/rs10_production.rpx', json_decode(file_get_contents(__DIR__ . '/_files/expectedRs10.json'), true)],
            [__DIR__ . '/_files/fake.rpx', json_decode(file_get_contents(__DIR__ . '/_files/expectedFake.json'), true)],
        ];
    }
}
