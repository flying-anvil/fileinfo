<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Test\InfoExtractor;

use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Smbx2\Smbx2LevelInfoExtractor;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\Fileinfo\InfoExtractor\Application\Game\Smbx2\Smbx2LevelInfoExtractor
 */
class Smbx2LevelInfoExtractorTest extends TestCase
{
    /** @var Smbx2LevelInfoExtractor */
    private $extractor;

    protected function setUp(): void
    {
        $this->extractor = new Smbx2LevelInfoExtractor();
    }

    public function testCanGetInfo(): void
    {
        $file     = __DIR__ . '/_files/Sticky Grounds.lvlx';
        $expected = json_decode(
            file_get_contents(__DIR__ . '/_files/expected.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        $result = $this->extractor->getInfo($file)->toArrayRecursive();

        self::assertEquals($expected, $result);
    }

    public function testCanGetSummary(): void
    {
        $file     = __DIR__ . '/_files/Sticky Grounds.lvlx';
        $expected = json_decode(
            file_get_contents(__DIR__ . '/_files/expectedSummary.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        $result = $this->extractor->getInfoSummary($file)->toArrayRecursive();

        self::assertEquals($expected, $result);
    }
}
