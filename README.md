# FileInfo

Get information about files. Focused on exotic file types.

## Supported Filetypes

| extension       | description                                                |
|-----------------|------------------------------------------------------------|
| **Application** |                                                            |
| _**Dev Tools**_ |                                                            |
| diff            | Output of the `diff` command (and `git diff`)              |
| patch           | Output of the `diff` command (and `git diff`)              |
| _**Games**_     |                                                            |
| lvlx            | SMBX2 Level                                                |
| gma             | GMod Mod                                                   |
| d2s             | Diablo 2 savegame                                          |
| _**ROMs**_      |                                                            |
| rpx             | Wii U Application                                          |
| spc             | SPC700 Dump                                                |
| smc             | SNES Rom                                                   |
| sfc             | Super Famicom Rom                                          |
| z64             | Nintendo 64 Rom                                            |
| nds             | Nintendo DS Rom                                            |
| gba             | Nintendo GameBoy Advance Rom                               |
| gb              | Nintendo GameBoy Rom                                       |
| nes             | NES ROM                                                    |
| **Archive**     |                                                            |
| zip             | Lossless compressed archive                                |
| jar             | Java Archivce                                              |
| **Media**       |                                                            |
| _**Audio**_     |                                                            |
| xm              | Extended Module Audio File                                 |
| mod             | Module Audio File                                          |
| wav             | Uncompressed Wave audio                                    |
| _**Video**_     |                                                            |
| bik             | Bink Video/Audio Container                                 |
| bik2            | Bink Video/Audio Container                                 |
| bk2             | Bink Video/Audio Container                                 |
| _**Image**_     |                                                            |
| png             | Portable Network Graphic                                   |
| bmp             | Bitmap                                                     |
| gif             | Graphics Interchange Format                                |
| jpg             | Joint Photographic (Experts) Group                         |
| jpeg            | Joint Photographic Experts Group                           |
| jfif            | JPEG File Interchange Format                               |
| aseprite        | Aseprite project file                                      |
| **Geometry**    |                                                            |
| obj             | Wavefront OBJ                                              |
| **Misc**        |                                                            |
| url             | Windows shortcuts that point to a webstite                 |
|                 |                                                            |
|                 |                                                            |

## Restore Extension From Content

Can scan files to try to recognize the type and its file extension.
Works for a ton of types.

## CLI Usage

This package contains a script (`fileinfo`) that can be run from the CLI.  
Usage: `fileinfo [options] <file> [<file>...] [options]`  
See `fileinfo --help` for more information

List of supported options:

| short | long        | description                                            |
|-------|-------------|--------------------------------------------------------|
| h     | help        | Prints help text                                       |
| v     | version     | Prints version text                                    |
| a     | all         | Show all possible information                          |
|       | full        | Same as all                                            |
| e     | extension   | Override extension                                     |
| c     | context     | Use context instead of extension                       |
| r     | reconstruct | Try to reconstruct extension for file by content       |
| f     | format      | Format of the output                                   |
| d     | debug       | Throw Exceptions (if not specified, print the message) |

Does work with multiple input files (`fileinfo samples/*`), however,
the output differs and might not be considered stable when multiple files are shown.

## Development Notes

### Adding a new FileInfoExtractor

1. Create a new class in `src/InfoExtractor/Extractor/[…]`
2. Implement `InfoExtractor`
   - Try to use keys from `InfoCommonKeys`
3. Create a new `KnownExtension` and `Context`
4. Register it in `DefaultDefinition`

### Adding a new ExtensionReader

For simple, non-offset magic bytes, add an entry to `FileExtensionMagicByteSupplier::DEFAULT_SIMPLE_MAGIC`.

For more complex ones:  
1. Create a new class in `src/FileExtension/Reader/[…]`
2. Implement `FileExtensionMagicByteReader`
3. Register it in `FileExtensionMagicByteSupplier::DEFAULT_READERS`

### Acquiring sample files

Run the following command and give it an extension: `bin/download-samples.php $extension`  
`--all` can be used to download all available samples.

Note that not all extensions have samples to download, due to practical or copyright reasons.

### Local docker testing

`dcd run --rm php bin/fileinfo ./samples/$file`
