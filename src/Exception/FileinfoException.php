<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Exception;

use FlyingAnvil\Libfa\Exception\FlyingAnvilException;

class FileinfoException extends FlyingAnvilException
{
}
