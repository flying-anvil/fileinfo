<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Exception;

use Exception;

class FileException extends FileinfoException
{
}
