<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Command;

use FlyingAnvil\Fileinfo\FileExtension\FileExtensionFactory;
use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\OutputFormat;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;

class ReconstructExtensionCommand implements CommandInterface
{
    private ConsoleWriter $output;

    public function __construct(
        private string $file,
        private FileExtensionFactory $fileExtensionFactory,
        private bool $oneOfMany,
    ) {
        $this->output = new ConsoleWriter();
    }

    public function run(): int
    {
        $extension = $this->fileExtensionFactory->createFromMagicBytes($this->file);

        if ($this->oneOfMany) {
            if (!$extension) {
                $extension = '';
            }

            $this->output->writeln(sprintf(
                '%s: %s',
                $this->file,
                $extension,
            ));

            return MainCommand::ERROR_NONE;
        }

        if (!$extension) {
            return MainCommand::ERROR_EXECUTION;
        }

        $this->output->writeln($extension);

        return MainCommand::ERROR_NONE;
    }
}
