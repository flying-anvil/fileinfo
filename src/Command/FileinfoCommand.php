<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Command;

use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\OutputFormat;
use FlyingAnvil\Fileinfo\InfoExtractor\Factory\InfoExtractorSupplier;
use FlyingAnvil\Fileinfo\InfoExtractor\SmartInfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Formatter\KeyValueFormatter;

class FileinfoCommand implements CommandInterface
{
    private SmartInfoExtractor $smartExtractor;
    private ConsoleWriter $output;

    public function __construct(
        private string $file,
        private bool $full,
        private OutputFormat $format,
        private FileExtension $extension,
        private ?Context $context,
        private bool $oneOfMany,
    ) {
        $supplier = new InfoExtractorSupplier();
        $supplier->addDefaultExtensionDefinitions();
        $supplier->addDefaultContextDefinitions();

        $this->smartExtractor = new SmartInfoExtractor($supplier);
        $this->output         = new ConsoleWriter();
    }

    public function run(): int
    {
        $info = $this->getInfo();

        if ($this->oneOfMany) {
            $this->output->writeln($this->file);

            $this->output->writeln($this->formatInfo($info) . PHP_EOL);
            return MainCommand::ERROR_NONE;
        }

        $this->output->writeln($this->formatInfo($info));
        return MainCommand::ERROR_NONE;
    }

    private function getInfo(): UniversalCollection
    {
        if (!$this->context) {
            if ($this->full) {
                return $this->smartExtractor->getInfoByExtension($this->file, $this->extension);
            }

            return $this->smartExtractor->getInfoSummaryByExtension($this->file, $this->extension);
        }

        if ($this->full) {
            return $this->smartExtractor->getInfoByContext($this->file, $this->context);
        }

        return $this->smartExtractor->getInfoSummaryByContext($this->file, $this->context);
    }

    public function formatInfo(UniversalCollection $info): string
    {
        switch ($this->format->toString()) {
            case OutputFormat::TABLE:
            case OutputFormat::KEY_VALUE:
                $formatter = new KeyValueFormatter();
                return $formatter->formatKeyValueTable($info->toArray());
            case OutputFormat::JSON:
                return json_encode($info->toArrayRecursive(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
        }

        return 'Format not supported';
    }
}
