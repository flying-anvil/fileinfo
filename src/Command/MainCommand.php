<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Command;

use Exception;
use FlyingAnvil\Fileinfo\FileExtension\FileExtensionFactory;
use FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\FileExtensionMagicByteSupplier;
use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\OutputFormat;
use FlyingAnvil\Libfa\DataObject\Exception\InvalidExtensionException;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use Generator;
use GetOpt\ArgumentException;
use GetOpt\ArgumentException\Missing;
use GetOpt\GetOpt;
use GetOpt\Operand;
use GetOpt\Option;
use Throwable;

class MainCommand
{
    private const OPTION_HELP      = 'help';
    private const OPTION_VERSION   = 'version';
    private const ARGUMENT_FILE    = 'file';
    private const OPTION_FULL      = 'full';
    private const OPTION_ALL       = 'all';
    private const OPTION_EXTENSION = 'extension';
    private const OPTION_CONTEXT   = 'context';
    private const OPTION_RECONSTRUCT_EXTENSION = 'reconstruct';
    private const OPTION_FORMAT    = 'format';
    private const OPTION_DEBUG     = 'debug';

    public const ERROR_NONE      = 0;
    public const ERROR_ARGUMENTS = 1;
    public const ERROR_EXECUTION = 2;

    public const NAME = 'fileinfo';
    public const VERSION = '1.0';

    private ConsoleWriter $output;
    private FileExtensionFactory $fileExtensionFactory;

    public function __construct(
        private GetOpt $getOpt,
    ) {
        $this->output = new ConsoleWriter();

        $magicReaderSupplier = new FileExtensionMagicByteSupplier();
        $magicReaderSupplier->addDefaultReaderDefinitions();
        $magicReaderSupplier->addDefaultSimpleDefinitions();

        $this->fileExtensionFactory = new FileExtensionFactory($magicReaderSupplier);
    }

    public function run(array $argv): int
    {
        $this->configure();
        if (!$this->parse($argv)) {
            return self::ERROR_ARGUMENTS;
        }

        if ($this->getOpt->getOption(self::OPTION_VERSION)) {
            $this->output->writeln(sprintf(
                "%s %s",
                self::NAME,
                self::VERSION,
            ));

            return self::ERROR_NONE;
        }

        if ($this->getOpt->getOption(self::OPTION_HELP)) {
            $this->output->write($this->getOpt->getHelpText());
            return self::ERROR_NONE;
        }

        $files     = $this->getOpt->getOperand(self::ARGUMENT_FILE);
        $fileCount = count($files);
        if ($fileCount > 0) {
            $format = $this->getOpt->getOption(self::OPTION_FORMAT);
            try {
                $format = $this->getFormat($format, false);
            } catch (Exception $exception) {
                fwrite(STDERR, $exception->getMessage() . PHP_EOL);
                return self::ERROR_ARGUMENTS;
            }

            $commands = $this->getCommands($files, $format);

            if (!$commands) {
                return self::ERROR_ARGUMENTS;
            }

            $successes = 0;
            $lastCode  = self::ERROR_EXECUTION;
            foreach ($commands as $command) {
                try {
                    $lastCode = $command->run();
                    if ($lastCode === self::ERROR_NONE) {
                        $successes++;
                    }
                } catch (Throwable $throwable) {
                    $debugLevel = $this->getOpt->getOption(self::OPTION_DEBUG);

                    if ($debugLevel > 0) {
                        throw $throwable;
                    }

                    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
                    return self::ERROR_EXECUTION;
                }
            }

            if ($fileCount > 1 && $successes > 0) {
                return self::ERROR_NONE;
            }

            return $lastCode;
        }

        $this->output->write($this->getOpt->getHelpText());
        return self::ERROR_NONE;
    }

    private function configure(): void
    {
        $this->getOpt->addOptions([
            Option::create('v', self::OPTION_VERSION, GetOpt::NO_ARGUMENT)
                ->setDescription('Show version information and quit'),

            Option::create('h', self::OPTION_HELP, GetOpt::NO_ARGUMENT)
                ->setDescription('Show this help and quit'),

            Option::create(null, self::OPTION_FULL, GetOpt::NO_ARGUMENT)
                ->setDescription('Show all possible information (default format is json)')
                ->setDefaultValue(false),

            Option::create('a', self::OPTION_ALL, GetOpt::NO_ARGUMENT)
                ->setDescription('Show all possible information (default format is json)')
                ->setDefaultValue(false),

            Option::create('e', self::OPTION_EXTENSION, GetOpt::REQUIRED_ARGUMENT)
                ->setDescription('Override extension'),

            Option::create('c', self::OPTION_CONTEXT, GetOpt::REQUIRED_ARGUMENT)
                ->setDescription('Use context instead of extension'),

            Option::create('r', self::OPTION_RECONSTRUCT_EXTENSION, GetOpt::NO_ARGUMENT)
                ->setDescription('Try to reconstruct extension for file by content')
                ->setDefaultValue(false),

            Option::create('f', self::OPTION_FORMAT, GetOpt::REQUIRED_ARGUMENT)
                ->setDescription(sprintf(
                    'Format of the output (%s)',
                    implode(', ', OutputFormat::getAllFormats()),
                )),

            Option::create('d', self::OPTION_DEBUG)
                ->setDescription('Debug')
                ->setDefaultValue(0),
        ]);

        $this->getOpt->addOperands([
            Operand::create(self::ARGUMENT_FILE, Operand::MULTIPLE | Operand::REQUIRED)
                ->setDescription('File to get info about')
                ->setValidation('file_exists'),
        ]);

        $this->getOpt->getHelp()->setTexts([
            'usage-operands' => 'arguments|options',
            'operands-title' => 'Arguments:' . PHP_EOL,
        ]);
    }

    private function parse(array $argv): bool
    {
        try {
            try {
                $this->getOpt->process($argv);
            } catch (Missing $exception) {
                // catch missing exceptions if help is requested
                if (!$this->getOpt->getOption(self::OPTION_HELP)) {
                    throw $exception;
                }
            }
        } catch (ArgumentException $exception) {
            fwrite(STDERR, $exception->getMessage() . PHP_EOL);
            $this->output->write($this->getOpt->getHelpText());
            return false;
        }

        return true;
    }

    /**
     * @param string[] $files
     * @return Generator<CommandInterface> | CommandInterface[]
     */
    private function getCommands(array $files, OutputFormat $format): ?Generator
    {
        $multiple = count($files) > 1;

        if ($this->getOpt->getOption(self::OPTION_RECONSTRUCT_EXTENSION)) {
            foreach ($files as $file) {
                yield new ReconstructExtensionCommand($file, $this->fileExtensionFactory, $multiple);
            }

            return;
        }

        $full              = (bool)$this->getOpt->getOption(self::OPTION_FULL) || (bool)$this->getOpt->getOption(self::OPTION_ALL);
        $extensionOverride = $this->getOpt->getOption(self::OPTION_EXTENSION);
        $context           = $this->getOpt->getOption(self::OPTION_CONTEXT);

        try {
            $context   = $this->getContext($context);
        } catch (Exception $exception) {
            fwrite(STDERR, $exception->getMessage() . PHP_EOL);
            return null;
        }

        foreach ($files as $file) {
            try {
                $extension = $this->getExtension($file, $extensionOverride);
            } catch (Exception $exception) {
                fwrite(STDERR, $exception->getMessage() . PHP_EOL);
                return null;
            }

            yield new FileinfoCommand($file, $full, $format, $extension, $context, $multiple);
        }
    }

    private function getExtension(string $filePath, ?string $rawExtension = null): FileExtension
    {
        if ($rawExtension !== null) {
            return FileExtension::createFromString($rawExtension);
        }

        try {
            $extension = FileExtension::createFromFilePath($filePath);
        } catch (InvalidExtensionException $_) {
            $extension = $this->fileExtensionFactory->createFromMagicBytes($filePath);

            if ($extension === null) {
                throw new InvalidExtensionException(sprintf(
                    'The file (%s) has no guessable extension',
                    basename($filePath),
                ));
            }
        }

        return $extension;
    }

    private function getContext(?string $context): ?Context
    {
        if ($context !== null) {
            return Context::createFromKnown($context);
        }

        return null;
    }

    private function getFormat(?string $format, bool $full): OutputFormat
    {
        if ($format !== null) {
            return OutputFormat::create($format);
        }

        // If full, use json due to nested values
//        if ($full) {
//            return OutputFormat::create(OutputFormat::JSON);
//        }

        return OutputFormat::create(OutputFormat::KEY_VALUE);
    }
}
