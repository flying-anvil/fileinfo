<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Command;

interface CommandInterface
{
    public function run(): int;
}
