<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Samples;

class SampleDownloadStatus
{
    public const STATUS_DOWNLOADED     = 0;
    public const STATUS_PRESENT        = 1;
    public const STATUS_NOT_CONFIGURED = 2;
    public const STATUS_ERROR          = 3;

    private function __construct(
        private int $status,
        private string $message,
        private string $targetPath,
    ) {}

    public static function create(int $status, string $message, string $targetPath): self
    {
        return new self($status, $message, $targetPath);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getTargetPath(): string
    {
        return $this->targetPath;
    }
}
