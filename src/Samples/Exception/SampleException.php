<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Samples\Exception;

use Exception;

class SampleException extends Exception
{
}
