<?php

/** @noinspection PhpComposerExtensionStubsInspection */

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Samples;

use FlyingAnvil\Fileinfo\FileExtension\KnownExtensions;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\X11ColorPalette;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Utils\Console\ConsoleWriter;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorableOutput;
use FlyingAnvil\Libfa\Wrapper\File;
use RarArchive;
use Throwable;
use ZipArchive;

class SampleDownloader
{
    private const OUTPUT_DIRECTORY = __DIR__ . '/../../samples/';

    // Application
    //   Games
    private const SAMPLE_LVLX = 'https://gitlab.com/flying-anvil/smbx-scripts/-/raw/master/level.lvlx';

    //   ROMs
    private const SAMPLE_SPC  = 'https://dl.smwcentral.net/10526/fire%26ice%20v1.0.1.zip';
    private const SAMPLE_GB   = 'http://www.nordloef.com/gbdemos/pocket.rar';
    private const SAMPLE_GBA  = 'https://files.scene.org/get/parties/2003/assembly03/mobile_demo/evol__by_karillon+komplex_1.zip';
    private const SAMPLE_GBA2 = 'ftp://untergrund.net/users/sensenstahl/others/gba/gba-niccc_update.zip';

    // Media
    //   Audio
    private const SAMPLE_XM   = 'https://api.modarchive.org/downloads.php?moduleid=149252';
    private const SAMPLE_MOD  = 'https://api.modarchive.org/downloads.php?moduleid=118568';
    private const SAMPLE_MOD2 = 'https://api.modarchive.org/downloads.php?moduleid=89428';

    //   Video
    private const SAMPLE_BINK = 'http://samples.mplayerhq.hu/game-formats/bink/original.bik';

    // Geometry
    private const SAMPLE_OBJ = 'https://people.sc.fsu.edu/~jburkardt/data/obj/gourd.obj';

    public function downloadAll(): void
    {
        $output  = new ConsoleWriter();
        $palette = X11ColorPalette::getPalette();

        echo 'Downloading samples', PHP_EOL;

        $allKnownExtension = KnownExtensions::getAllKnownExtension();
        $longestLength     = $this->getLongestLength($allKnownExtension);

        foreach ($allKnownExtension as $extension) {
            echo sprintf(
                '| %s | ',
                str_pad($extension, $longestLength, ' ', STR_PAD_RIGHT)
            );

            try {
                $result = $this->download(FileExtension::createFromString($extension));
//            } catch (SampleException $exception) {
//                $output->write(ColorableOutput::create(
//                    $exception->getMessage(),
//                    $palette->resolveName(HtmlColorPalette::COLOR_OLIVE),
//                ) . PHP_EOL);
//
//                continue;
            } catch (Throwable $error) {
                $result = SampleDownloadStatus::create(SampleDownloadStatus::STATUS_ERROR, $error->getMessage(), '');

//                $output->write(ColorableOutput::create(
//                    $error->getMessage(),
//                    $palette->resolveName(X11ColorPalette::COLOR_RED),
//                ) . PHP_EOL);
            }

            switch ($result->getStatus()) {
                case SampleDownloadStatus::STATUS_DOWNLOADED:
                    $output->write(ColorableOutput::create(
                        $result->getMessage(),
                        $palette->resolveName(X11ColorPalette::COLOR_LIME_GREEN),
                    ) . PHP_EOL);
                    break;
                case SampleDownloadStatus::STATUS_PRESENT:
                    $output->write(ColorableOutput::create(
                        $result->getMessage(),
                        $palette->resolveName(X11ColorPalette::COLOR_DARK_OLIVE_GREEN),
                    ) . PHP_EOL);
                    break;
                case SampleDownloadStatus::STATUS_NOT_CONFIGURED:
                    $output->write(ColorableOutput::create(
                        $result->getMessage(),
                        $palette->resolveName(X11ColorPalette::COLOR_DIM_GRAY),
                    ) . PHP_EOL);
                    break;
                case SampleDownloadStatus::STATUS_ERROR:
                    $output->write(ColorableOutput::create(
                        $result->getMessage(),
                        $palette->resolveName(X11ColorPalette::COLOR_CRIMSON),
                    ) . PHP_EOL);
                    break;
            }
        }
    }

    public function download(FileExtension $extension): SampleDownloadStatus
    {
        switch ($extension->getExtension()) {
            // Application
            //   Games
            case KnownExtensions::APPLICATION_GAME_SMBX2_LEVEL:
                return $this->simpleDownload(self::SAMPLE_LVLX, 'smbx2.lvlx');

            //   ROMs
            case KnownExtensions::APPLICATION_GAME_COMMON_SNES_SOUNDTRACK:
                return $this->archiveZipExtract(self::SAMPLE_SPC, 'fire&ice/03 fireice.spc', 'fire-and-ice.spc');
            case KnownExtensions::APPLICATION_GAME_COMMON_GB_ROM:
                return $this->archiveRarExtract(self::SAMPLE_GB, 'pocket.gb', 'pocket.gb');
            case KnownExtensions::APPLICATION_GAME_COMMON_GBA_ROM:
                return $this->archiveZipExtract(self::SAMPLE_GBA, 'Evol__by_Karillon+Komplex_1.gba', 'evol.gba');
//                return $this->archiveZipExtract(self::SAMPLE_GBA2, 'gba-niccc.gba', 'gba-niccc.gba');

            // Media

            //   Audio
            case KnownExtensions::MEDIA_AUDIO_EXTENDED_MODULE:
                return $this->simpleDownload(self::SAMPLE_XM . '', 'unreeeal_superhero_3.xm');
            case KnownExtensions::MEDIA_AUDIO_MODULE:
                return $this->simpleDownload(self::SAMPLE_MOD . '', 'fire-and-ice.mod');
//                return $this->simpleDownload(self::SAMPLE_MOD2 . '', 'still-alive-p-e.mod');

            //   Video
            case KnownExtensions::MEDIA_VIDEO_BINK:
                return $this->simpleDownload(self::SAMPLE_BINK, 'original.bik');

            // Geometry
            case KnownExtensions::GEOMETRY_OBJ:
                return $this->simpleDownload(self::SAMPLE_OBJ, 'gourd.obj');
        }

        return SampleDownloadStatus::create(
            SampleDownloadStatus::STATUS_NOT_CONFIGURED,
            'not configured',
            '',
        );
    }

    private function simpleDownload(string $source, string $targetName): SampleDownloadStatus
    {
        if (File::load(self::OUTPUT_DIRECTORY . $targetName)->exists()) {
            return SampleDownloadStatus::create(
                SampleDownloadStatus::STATUS_PRESENT,
                'present',
                $targetName,
            );
        }

        file_put_contents(
            self::OUTPUT_DIRECTORY . $targetName,
            file_get_contents($source),
        );

        return SampleDownloadStatus::create(
            SampleDownloadStatus::STATUS_DOWNLOADED,
            'downloaded',
            $targetName,
        );
    }

    private function archiveZipExtract(string $source, string $path, $targetName): SampleDownloadStatus
    {
        if (File::load(self::OUTPUT_DIRECTORY . $targetName)->exists()) {
            return SampleDownloadStatus::create(
                SampleDownloadStatus::STATUS_PRESENT,
                'present',
                $targetName,
            );
        }

        if (!extension_loaded('zip')) {
            return SampleDownloadStatus::create(
                SampleDownloadStatus::STATUS_ERROR,
                'missing extension "zip"',
                $targetName,
            );
        }

        $tempFile = tempnam(sys_get_temp_dir(), 'fileinfo-sample-');
        copy($source, $tempFile);

        $archive = new ZipArchive();
        $archive->open($tempFile);

        file_put_contents(self::OUTPUT_DIRECTORY . $targetName, $archive->getFromName($path));
        unlink($tempFile);

        return SampleDownloadStatus::create(
            SampleDownloadStatus::STATUS_DOWNLOADED,
            'downloaded',
            $targetName,
        );
    }

    private function archiveRarExtract(string $source, string $path, $targetName): SampleDownloadStatus
    {
        if (File::load(self::OUTPUT_DIRECTORY . $targetName)->exists()) {
            return SampleDownloadStatus::create(
                SampleDownloadStatus::STATUS_PRESENT,
                'present',
                $targetName,
            );
        }

        if (!extension_loaded('rar')) {
            return SampleDownloadStatus::create(
                SampleDownloadStatus::STATUS_ERROR,
                'missing extension "rar"',
                $targetName,
            );
        }

        $tempDir = sys_get_temp_dir();

        $tempFile = tempnam($tempDir, 'fileinfo-sample-');
        copy($source, $tempFile);

        $archive = RarArchive::open($tempFile);
        $entry = $archive->getEntry($path);

        $entry->extract($tempDir);
        File::load($tempDir . '/' . $path)->move(self::OUTPUT_DIRECTORY . $targetName);
        unlink($tempFile);

        return SampleDownloadStatus::create(
            SampleDownloadStatus::STATUS_DOWNLOADED,
            'downloaded',
            $targetName,
        );
    }

    private function getLongestLength(array $strings): int
    {
        $max = 0;
        foreach ($strings as $string) {
            $max = max($max, mb_strlen($string));
        }

        return $max;
    }
}
