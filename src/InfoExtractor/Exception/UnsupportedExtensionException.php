<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Exception;

use FlyingAnvil\Fileinfo\Exception\FileinfoException;

class UnsupportedExtensionException extends FileinfoException
{
}
