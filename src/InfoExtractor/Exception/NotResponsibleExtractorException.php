<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Exception;

use FlyingAnvil\Fileinfo\Exception\FileinfoException;
use Throwable;

class NotResponsibleExtractorException extends FileinfoException
{
    public const HEADER_MISMATCH          = 'Header Mismatch';
    public const MAGIC_MISMATCH           = 'Magic Mismatch';
    public const SUSPICIOUS_HEADER_LENGTH = 'Suspicious header length';
    public const VERSION_NOT_SUPPORTED    = 'Version not supported';

    public static function headerMismatch($code = 0, Throwable $previous = null): self
    {
        return new self(self::HEADER_MISMATCH, $code, $previous);
    }

    public static function magicMismatch($code = 0, Throwable $previous = null): self
    {
        return new self(self::MAGIC_MISMATCH, $code, $previous);
    }

    public static function suspiciousHeaderLength($code = 0, Throwable $previous = null): self
    {
        return new self(self::SUSPICIOUS_HEADER_LENGTH, $code, $previous);
    }

    public static function versionNotSupported($code = 0, Throwable $previous = null): self
    {
        return new self(self::VERSION_NOT_SUPPORTED, $code, $previous);
    }
}
