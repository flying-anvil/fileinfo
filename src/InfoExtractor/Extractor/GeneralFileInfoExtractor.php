<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class GeneralFileInfoExtractor implements InfoExtractor
{
    public function getInfo(string $filePath): UniversalCollection
    {
        var_dump(self::class);
        $file = File::load($filePath);

        $result = UniversalCollection::createEmpty();

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }
}
