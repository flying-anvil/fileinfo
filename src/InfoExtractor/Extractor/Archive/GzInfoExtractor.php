<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive;

use DateTimeImmutable;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class GzInfoExtractor implements InfoExtractor
{
    public const GZ_MAGIC = "\x1F\x8B";

    private const OFFSET_COMPRESSION   = 0x02;
    private const OFFSET_FLAGS         = 0x03;
    private const OFFSET_MODIFIED_DATE = 0x04;
    private const OFFSET_OS            = 0x09;
    private const OFFSET_ORIGINAL_NAME = 0x0A;

    private const COMPRESSION_MAPPING = [
        0 => 'copy',
        1 => 'compressed',
        2 => 'packed',
        3 => 'LZH',
        4 => 'reserved',
        5 => 'reserved',
        6 => 'reserved',
        7 => 'reserved',
        8 => 'deflate',
    ];

    private const OS_MAPPING = [
        0   => 'FAT',
        1   => 'AmigaOS',
        2   => 'VMS',
        3   => 'Unix',
        4   => 'VM',
        5   => 'Atari TOS',
        6   => 'HPFS',
        7   => 'Macintosh',
        8   => 'Z-System',
        9   => 'CP/M',
        10  => 'TOPS-20',
        11  => 'NTFS',
        12  => 'QDOS',
        13  => 'Acorn RISC OS',
        255 => 'Unknown',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $flags     = $this->readFlags($file);
        $timestamp = $this->readModifiedDateTimestamp($file);
        $datetime  = new DateTimeImmutable("@$timestamp");

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::COMPRESSION_METHOD, $this->translateCompressionMethod($this->readCompressionMethod($file)));
        $result->add(InfoCommonKeys::FLAGS, $flags);
        $result->add(InfoCommonKeys::TIMESTAMP, $this->readModifiedDateTimestamp($file));
        $result->add(InfoCommonKeys::DATETIME, $datetime->format(DATE_ATOM));
        $result->add(InfoCommonKeys::OPERATING_SYSTEM, $this->translateOperatingSystem($this->readOperatingSystem($file)));
        $result->add(InfoCommonKeys::UNCOMPRESSED_SIZE, $this->readUncompressedSize($file));
        $result->add(InfoCommonKeys::ORIGINAL_NAME, $this->readOriginalFileName($file));

        $this->readOriginalFileName($file);

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $timestamp = $this->readModifiedDateTimestamp($file);
        $datetime  = new DateTimeImmutable("@$timestamp");

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::DATETIME, $datetime->format('Y-m-d H:i:s'));
        $result->add(InfoCommonKeys::UNCOMPRESSED_SIZE, $this->readUncompressedSize($file));
        $result->add(InfoCommonKeys::ORIGINAL_NAME, $this->readOriginalFileName($file));

        return $result;
    }

    public function verify(File $file): bool
    {
        $file->rewind();
        $header = $file->read(2);

        return $header === self::GZ_MAGIC;
    }

    public function readCompressionMethod(File $file): int
    {
        $file->seek(self::OFFSET_COMPRESSION);
        return $file->readUnsignedByte();
    }

    public function translateCompressionMethod(int $compressionMethod): string
    {
        return self::COMPRESSION_MAPPING[$compressionMethod] ?? 'unknown';
    }

    public function readFlags(File $file): UniversalCollection
    {
        $file->seek(self::OFFSET_FLAGS);
        $rawFlags = $file->readUnsignedByte();

        $hasAscii          = (bool)($rawFlags & 0b00000001);
        $hasCrc16          = (bool)($rawFlags & 0b00000010);
        $hasAdditionalInfo = (bool)($rawFlags & 0b00000100);
        $hasOriginalName   = (bool)($rawFlags & 0b00001000);
        $hasComment        = (bool)($rawFlags & 0b00010000);
        $reserved1         = (bool)($rawFlags & 0b00100000);
        $reserved2         = (bool)($rawFlags & 0b01000000);
        $reserved3         = (bool)($rawFlags & 0b10000000);

        $flags = UniversalCollection::createEmpty();
        $flags->add('hasAscii', $hasAscii);
        $flags->add('hasCrc16', $hasCrc16);
        $flags->add('hasAdditionalInfo', $hasAdditionalInfo);
        $flags->add('hasOriginalName', $hasOriginalName);
        $flags->add('hasComment', $hasComment);
        $flags->add('reserved1', $reserved1);
        $flags->add('reserved2', $reserved2);
        $flags->add('reserved3', $reserved3);

        return $flags;
    }

    public function readModifiedDateTimestamp(File $file): int
    {
        $file->seek(self::OFFSET_MODIFIED_DATE);
        return $file->readUnsignedLongLittleEndian();
    }

    public function readOperatingSystem(File $file): int
    {
        $file->seek(self::OFFSET_OS);
        return $file->readUnsignedByte();
    }

    public function translateOperatingSystem(int $os): string
    {
        return self::OS_MAPPING[$os] ?? 'unknown';
    }

    /**
     * @param File $file
     * @return int Is limited to 32-bit values
     */
    public function readUncompressedSize(File $file): int
    {
        $file->seek($file->getFileSize() - 4);
        return $file->readUnsignedLongLittleEndian();
    }

    private function readOriginalFileName(File $file): ?string
    {
        if ($this->readFlags($file)->get('hasOriginalName', false)) {
            $file->seek(self::OFFSET_ORIGINAL_NAME);
            return $file->readNullTerminatedSequence(255);
        }

        return null;
    }
}
