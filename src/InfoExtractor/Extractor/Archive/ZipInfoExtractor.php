<?php

/**
 * @noinspection PhpComposerExtensionStubsInspection
 */

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive;

use FlyingAnvil\Fileinfo\InfoExtractor\CacheInterface;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\DataObject\Counter\MultiCounter;
use FlyingAnvil\Libfa\Wrapper\File;
use ZipArchive;

class ZipInfoExtractor implements InfoExtractor, CacheInterface
{
    public const ZIP_MAGIC = 'PK';

    private const COUNT_DIRECTORIES       = 'directories';
    private const COUNT_FILES             = 'files';
    private const COUNT_COMPRESSED_SIZE   = 'compressedSize';
    private const COUNT_UNCOMPRESSED_SIZE = 'uncompressedSize';

    private UniversalCollection $cache;

    public function __construct()
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        $valid = $this->verify($file);
        $file->close();

        if (!$valid) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        if (!extension_loaded('zip')) {
            throw new NotResponsibleExtractorException('Missing zip extension');
        }

        $result = UniversalCollection::createEmpty();

        $result->add(InfoCommonKeys::UNCOMPRESSED_SIZE, $this->readUncompressedSize($filePath));
        $result->add(InfoCommonKeys::COMPRESSED_SIZE, $this->readCompressedSize($filePath));
        $result->add(InfoCommonKeys::COMPRESSION_RATIO, $this->calculateCompressionRatio($filePath));
        $result->add(InfoCommonKeys::FILE_COUNT, $this->readFileCount($filePath));
        $result->add(InfoCommonKeys::DIRECTORY_COUNT, $this->readDirectoryCount($filePath));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    public function verify(File $file): bool
    {
        $header = $file->read(strlen(self::ZIP_MAGIC));

        return $header === self::ZIP_MAGIC;
    }

    public function readFileCount(string $filePath): int
    {
        $this->cacheCounts($filePath);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($filePath);
        return $counts->get(self::COUNT_FILES);
    }

    public function readDirectoryCount(string $filePath): int
    {
        $this->cacheCounts($filePath);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($filePath);
        return $counts->get(self::COUNT_DIRECTORIES);
    }

    public function readCompressedSize(string $filePath): int
    {
        $this->cacheCounts($filePath);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($filePath);
        return $counts->get(self::COUNT_COMPRESSED_SIZE);
    }

    public function readUncompressedSize(string $filePath): int
    {
        $this->cacheCounts($filePath);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($filePath);
        return $counts->get(self::COUNT_UNCOMPRESSED_SIZE);
    }

    public function calculateCompressionRatio(string $filePath): float
    {
        return $this->readCompressedSize($filePath) / $this->readUncompressedSize($filePath);
    }

    private function cacheCounts(string $filePath): void
    {
        if ($this->cache->has($filePath)) {
            return;
        }

        $zip = new ZipArchive();
        $zip->open($filePath);

        $entryCount = $zip->count();
        $counter    = MultiCounter::createEmpty();
        for ($i = 0; $i < $entryCount; $i++)
        {
            $stat = $zip->statIndex($i);

            $itemKey = [
               true  => self::COUNT_DIRECTORIES,
               false => self::COUNT_FILES,
            ][$stat['size'] === 0];

            $counter->increment($itemKey);
            $counter->increment(self::COUNT_COMPRESSED_SIZE, $stat['comp_size']);
            $counter->increment(self::COUNT_UNCOMPRESSED_SIZE, $stat['size']);
        }

        $this->cache->add($filePath, $counter);
    }

    public function clearCache(): void
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function clearSpecificCache(string $filePath): void
    {
        $this->cache->remove($filePath);
    }
}
