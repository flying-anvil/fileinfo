<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class ModuleInfoExtractor implements InfoExtractor
{
    private const OFFSET_TITLE   = 0x00;
    private const OFFSET_COMMENT = 0x20;

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        $result->add(InfoCommonKeys::SONG_TITLE, $this->readTitle($file));
        $result->add(InfoCommonKeys::COMMENT, $this->readComment($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    public function readTitle(File $file): string
    {
        $file->seek(self::OFFSET_TITLE);

        return rtrim($file->read(20));
    }

    public function readComment(File $file): string
    {
        $file->seek(self::OFFSET_COMMENT);

        return preg_replace( '/[^[:print:]]/', '', rtrim($file->readNullTerminatedSequence(22)));
    }
}
