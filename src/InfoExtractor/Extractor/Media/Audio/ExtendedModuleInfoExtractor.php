<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * https://www.fileformat.info/format/xm/corion.htm
 */
class ExtendedModuleInfoExtractor implements InfoExtractor
{
    public const XM_MAGIC = 'Extended Module: ';

    private const OFFSET_TITLE = 0x11;
    private const OFFSET_TRACKER = 0x26;
    private const OFFSET_TRACKER_VERSION = 0x3A;
    private const OFFSET_HEADER_SIZE = 0x3C;
    private const OFFSET_SONG_PATTERN_LENGTH = 0x40;
    private const OFFSET_RESTART_POSITION = 0x42;
    private const OFFSET_CHANNEL_COUNT = 0x44;
    private const OFFSET_PATTERN_COUNT = 0x46;
    private const OFFSET_INSTRUMENT_COUNT = 0x48;
    private const OFFSET_FLAGS = 0x4A;
    private const OFFSET_DEFAULT_TEMPO = 0x4C;
    private const OFFSET_DEFAULT_BPM = 0x4E;

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::SONG_TITLE, $this->readTitle($file));
        $result->add('tracker', $this->readTracker($file));
        $result->add('trackerRevision', $this->readTrackerRevision($file));
        $result->add(InfoCommonKeys::HEADER_SIZE, $this->readHeaderSize($file));
        $result->add('songPatternLength', $this->readSongPatternLength($file));
        $result->add('restartPosition', $this->readRestartPosition($file));
        $result->add('channelCount', $this->readChannelCount($file));
        $result->add('patternCount', $this->readPatternCount($file));
        $result->add('instrumentCount', $this->readInstrumentCount($file));
        $result->add(InfoCommonKeys::FLAGS, $this->readFlags($file));
        $result->add('defaultTempo', $this->readDefaultTempo($file));
        $result->add('defaultBpm', $this->readDefaultBpm($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    public function verify(File $file): bool
    {
        return $this->readMagic($file) === self::XM_MAGIC;
    }

    public function readMagic(File $file): string
    {
        $file->rewind();

        return $file->read(17);
    }

    public function readTitle(File $file): string
    {
        $file->seek(self::OFFSET_TITLE);

        return rtrim($file->read(20));
    }

    public function readTracker(File $file): string
    {
        $file->seek(self::OFFSET_TRACKER);

        return rtrim($file->read(20));
    }

    public function readTrackerRevision(File $file): string
    {
        $file->seek(self::OFFSET_TRACKER_VERSION);

        $hi  = ord($file->readChar());
        $low = ord($file->readChar());

        return $hi . '.' . $low;
    }

    public function readHeaderSize(File $file): int
    {
        $file->seek(self::OFFSET_HEADER_SIZE);

        $rawSize = $file->read(4);
        return unpack('V', $rawSize)[1];
    }

    public function readSongPatternLength(File $file): int
    {
        $file->seek(self::OFFSET_SONG_PATTERN_LENGTH);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readRestartPosition(File $file): int
    {
        $file->seek(self::OFFSET_RESTART_POSITION);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readChannelCount(File $file): int
    {
        $file->seek(self::OFFSET_CHANNEL_COUNT);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readPatternCount(File $file): int
    {
        $file->seek(self::OFFSET_PATTERN_COUNT);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readInstrumentCount(File $file): int
    {
        $file->seek(self::OFFSET_INSTRUMENT_COUNT);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readFlags(File $file): int
    {
        $file->seek(self::OFFSET_FLAGS);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readDefaultTempo(File $file): int
    {
        $file->seek(self::OFFSET_DEFAULT_TEMPO);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }

    public function readDefaultBpm(File $file): int
    {
        $file->seek(self::OFFSET_DEFAULT_BPM);

        $rawCount = $file->read(2);
        return unpack('v', $rawCount)[1];
    }
}
