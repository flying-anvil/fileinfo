<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;

class Mp3InfoExtractor implements InfoExtractor
{
    public function getInfo(string $filePath): UniversalCollection
    {
        throw new NotResponsibleExtractorException('Not yet implemented');
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }
}
