<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class WaveInfoExtractor implements InfoExtractor
{
    public const MAGIC_RIFF = 'RIFF';
    public const MAGIC_WAVE = 'WAVE';

    private const OFFSET_MAGIC_RIFF      = 0x00;
    private const OFFSET_MAGIC_WAVE      = 0x08;
    private const OFFSET_CHANNEL_COUNT   = 0x16;
    private const OFFSET_SAMPLE_RATE     = 0x18;
    private const OFFSET_BYTE_RATE       = 0x1C;
    private const OFFSET_BITS_PER_SAMPLE = 0x22;

    private const OFFSET_DATA_DESCRIPTOR = 0x24;

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = $this->loadFile($filePath);

        $channelCount  = $this->readChannelCount($file);
        $sampleRate    = $this->readSampleRate($file);
        $bitsPerSample = $this->readBitsPerSample($file);

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::CHANNEL_COUNT, $channelCount);
        $result->add(InfoCommonKeys::SAMPLE_RATE, $sampleRate);
        $result->add(InfoCommonKeys::BITS_PER_SAMPLE, $bitsPerSample);

        $byteRate = $this->readByteRate($file);
        $result->add(InfoCommonKeys::BYTE_RATE, $byteRate);
        $result->add(InfoCommonKeys::BIT_RATE, $byteRate * 8);

        $dataSize = $this->readDataSize($file);
        $result->add('dataSize', $dataSize);

        $result->add(InfoCommonKeys::DURATION, $this->calculateDuration(
            $dataSize,
            $sampleRate,
            $channelCount,
            $bitsPerSample,
        ));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = $this->loadFile($filePath);

        $channelCount  = $this->readChannelCount($file);
        $sampleRate    = $this->readSampleRate($file);
        $bitsPerSample = $this->readBitsPerSample($file);

        $result = UniversalCollection::createEmpty();

        $dataSize = $this->readDataSize($file);
        $result->add(InfoCommonKeys::DURATION, $this->calculateDuration(
            $dataSize,
            $sampleRate,
            $channelCount,
            $bitsPerSample,
        ));

        $result->add(InfoCommonKeys::CHANNEL_COUNT, $channelCount);
        $result->add(InfoCommonKeys::SAMPLE_RATE, $sampleRate);
        $result->add(InfoCommonKeys::BITS_PER_SAMPLE, $bitsPerSample);

        return $result;
    }

    private function loadFile(string $filePath): File
    {
        $file = File::load($filePath);
        $file->open();

        $valid = $this->verify($file);

        if (!$valid) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        return $file;
    }

    public function verify(File $file): bool
    {
        $file->seek(self::OFFSET_MAGIC_RIFF);

        if ($file->read(4) !== self::MAGIC_RIFF) {
            return false;
        }

        $file->seek(self::OFFSET_MAGIC_WAVE);

        /** @noinspection IfReturnReturnSimplificationInspection */
        if ($file->read(4) !== self::MAGIC_WAVE) {
            return false;
        }

        return true;
    }

    public function readChannelCount(File $file): int
    {
        $file->seek(self::OFFSET_CHANNEL_COUNT);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readSampleRate(File $file): int
    {
        $file->seek(self::OFFSET_SAMPLE_RATE);
        return $file->readUnsignedLongLittleEndian();
    }

    public function readByteRate(File $file): int
    {
        $file->seek(self::OFFSET_BYTE_RATE);
        return $file->readUnsignedLongLittleEndian();
    }

    public function readBitsPerSample(File $file): int
    {
        $file->seek(self::OFFSET_BITS_PER_SAMPLE);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readDataSize(File $file): int
    {
        $file->seek(self::OFFSET_DATA_DESCRIPTOR);

        while ($file->read(4) !== 'data') {
            $junkSize = $file->readUnsignedLongLittleEndian();

            $file->seek($junkSize, SEEK_CUR);
        }

        return $file->readUnsignedLongLittleEndian();
    }

    public function calculateDuration(int $dataSize, int $sampleRate, int $channelCount, int $bitsPerSample): float|int
    {
        return $dataSize / ($sampleRate * $channelCount * $bitsPerSample * .125);
    }
}
