<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Video;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * @see https://wiki.multimedia.cx/index.php/Bink_Container
 */
class BinkInfoExtractor implements InfoExtractor
{
    public const BINK_MAGIC_BIK = 'BIK';
    public const BINK_MAGIC_KB2 = 'KB2';

    private const OFFSET_CODEC_VERSION = 0x03;
    private const OFFSET_FILE_SIZE = 0x04;
    private const OFFSET_FRAME_COUNT = 0x08;
    private const OFFSET_LARGEST_FRAME = 0x0c;
    private const OFFSET_FRAME_COUNT_AGAIN = 0x10;
    private const OFFSET_RESOLUTION_X = 0x14;
    private const OFFSET_RESOLUTION_Y = 0x18;
    private const OFFSET_FRAME_RATE = 0x1C;
    private const OFFSET_FLAGS = 0x24;
    private const OFFSET_AUDIO_TRACKS = 0x28;

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $binkVersion = $this->readHeader($file) === self::BINK_MAGIC_BIK ? 1 : 2;

        $result = UniversalCollection::createEmpty();
        $result->add('binkVersion', $binkVersion);
        $result->add('codecVersion', $this->readCodecVersion($file));
        $result->add(InfoCommonKeys::FILE_SIZE, $this->readFileSize($file));
        $result->add(InfoCommonKeys::FRAME_COUNT, $this->readFrameCount($file));
        $result->add(InfoCommonKeys::DURATION, $this->calculateDuration($file));
        $result->add('largestFrameSize', $this->readLargestFrameSize($file));
        $result->add(InfoCommonKeys::FRAME_COUNT . 'Again', $this->readFrameCountAgain($file));
        $result->add(InfoCommonKeys::RESOLUTION_X, $this->readResolutionX($file));
        $result->add(InfoCommonKeys::RESOLUTION_Y, $this->readResolutionY($file));
        $result->add(InfoCommonKeys::FRAME_RATE, $this->readFrageRate($file));
        $result->add(InfoCommonKeys::FLAGS, $this->readFlags($file));
        $result->add(InfoCommonKeys::AUDIO_TRACKS, $this->readAudioTracks($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::FILE_SIZE, $this->readFileSize($file));
        $result->add(InfoCommonKeys::FRAME_COUNT, $this->readFrameCount($file));
        $result->add(InfoCommonKeys::DURATION, $this->calculateDuration($file));
        $result->add(InfoCommonKeys::RESOLUTION_X, $this->readResolutionX($file));
        $result->add(InfoCommonKeys::RESOLUTION_Y, $this->readResolutionY($file));
        $result->add(InfoCommonKeys::FRAME_RATE, $this->readFrageRate($file));
        $result->add(InfoCommonKeys::AUDIO_TRACKS, $this->readAudioTracks($file));

        return $result;
    }

    public function verify(File $file): bool
    {
        $header = $this->readHeader($file);
        return $header === self::BINK_MAGIC_BIK || $header === self::BINK_MAGIC_KB2;
    }

    public function readHeader(File $file): string
    {
        $file->rewind();

        return $file->read(3);
    }

    public function readCodecVersion(File $file): int
    {
        $file->seek(self::OFFSET_CODEC_VERSION);

        $rawCodecVersion = $file->read(1);
        return ord($rawCodecVersion);

        // TODO: Replace with match statement
//        return [
//            'a' => 0x62,
//            'd' => 0x64,
//            'f' => 0x66,
//            'g' => 0x67,
//            'h' => 0x68,
//            'i' => 0x69,
//        ][$rawCodecVersion];
    }

    public function readFileSize(File $file): int
    {
        $file->seek(self::OFFSET_FILE_SIZE);
        $rawFileSize = $file->read(4);

        return unpack('V', $rawFileSize)[1];
    }

    public function readFrameCount(File $file): int
    {
        $file->seek(self::OFFSET_FRAME_COUNT);
        $rawFrameCount = $file->read(4);

        return unpack('V', $rawFrameCount)[1];
    }

    public function readLargestFrameSize(File $file): int
    {
        $file->seek(self::OFFSET_LARGEST_FRAME);
        $rawSize = $file->read(4);

        return unpack('V', $rawSize)[1];
    }

    public function readFrameCountAgain(File $file)
    {
        $file->seek(self::OFFSET_FRAME_COUNT_AGAIN);
        $rawFrameCount = $file->read(4);

        return unpack('V', $rawFrameCount)[1];
    }

    public function readResolutionX(File $file): int
    {
        $file->seek(self::OFFSET_RESOLUTION_X);
        $rawResolution = $file->read(4);

        return unpack('V', $rawResolution)[1];
    }

    public function readResolutionY(File $file): int
    {
        $file->seek(self::OFFSET_RESOLUTION_Y);
        $rawResolution = $file->read(4);

        return unpack('V', $rawResolution)[1];
    }

    public function readFrageRate(File $file): int
    {
        $file->seek(self::OFFSET_FRAME_RATE);
        $rawResolution = $file->read(4);

        return unpack('V', $rawResolution)[1];
    }

    public function readFlags(File $file): UniversalCollection
    {
        $file->seek(self::OFFSET_FLAGS);
        $rawFlags = unpack('V', $file->read(4))[1];
        $hasAlphaPlane = ($rawFlags & 0b00000000000000100000000000000000) > 0;
        $isGrayscale   = ($rawFlags & 0b00000000000100000000000000000000) > 0;

        $flags = UniversalCollection::createEmpty();
        $flags->add('hasAlphaPlane', $hasAlphaPlane);
        $flags->add('isGrayscale', $isGrayscale);

        return $flags;
    }

    public function readAudioTracks(File $file): int
    {
        $file->seek(self::OFFSET_AUDIO_TRACKS);
        $rawAudioTracks = $file->read(4);

        return unpack('V', $rawAudioTracks)[1];
    }

    public function calculateDuration(File $file): float
    {
        $frameCount = $this->readFrameCount($file);
        $frameRate  = $this->readFrageRate($file);

        return $frameCount / $frameRate;
    }
}
