<?php

/** @noinspection PhpComposerExtensionStubsInspection */

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Image;

use FlyingAnvil\Fileinfo\FileExtension\KnownExtensions;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class CommonImageInfoExtractor implements InfoExtractor
{
    private const SUPPORTED_TYPES = [
        KnownExtensions::MEDIA_IMAGE_PNG  => true,
        KnownExtensions::MEDIA_IMAGE_BMP  => true,
        KnownExtensions::MEDIA_IMAGE_GIF  => true,
        KnownExtensions::MEDIA_IMAGE_JPG  => true,
        KnownExtensions::MEDIA_IMAGE_JPEG => true,
        KnownExtensions::MEDIA_IMAGE_JFIF => true,
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        if (!extension_loaded('gd')) {
            return UniversalCollection::createEmpty();
        }

        $file      = File::load($filePath);
        $extension = $file->getExtension()->getExtension();

        if (!(self::SUPPORTED_TYPES[$extension] ?? false)) {
            return UniversalCollection::createEmpty();
        }

        $info = getimagesize($file->getFilePath());
        if (!$info) {
            return UniversalCollection::createEmpty();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::RESOLUTION_X, $info[0]);
        $result->add(InfoCommonKeys::RESOLUTION_Y, $info[1]);

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    private function getPngInfo(File $file): UniversalCollection
    {

        var_dump($size);
        var_dump($info);
        die;

    }
}
