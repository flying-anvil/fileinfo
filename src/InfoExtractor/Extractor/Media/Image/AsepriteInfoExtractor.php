<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Image;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class AsepriteInfoExtractor implements InfoExtractor
{
    public const ASEPRITE_MAGIC = "\xE0\xA5";

    private const OFFSET_MAGIC          = 0x04;
    private const OFFSET_FRAME_COUNT    = 0x06;
    private const OFFSET_WIDTH          = 0x08;
    private const OFFSET_HEIGHT         = 0x0A;
    private const OFFSET_COLOR_DEPTH    = 0x0C;
    private const OFFSET_FLAGS          = 0x0E;
    private const OFFSET_FRAME_DURATION = 0x12;
    private const OFFSET_TRANSPARENT_COLOR_INDEX = 0x1C;
    private const OFFSET_COLOR_COUNT    = 0x20;
    private const OFFSET_PIXEL_WIDTH    = 0x22;
    private const OFFSET_PIXEL_HEIGHT   = 0x23;
    private const OFFSET_GRID_POS_Y     = 0x24;
    private const OFFSET_GRID_POS_X     = 0x26;
    private const OFFSET_GRID_WIDTH     = 0x28;
    private const OFFSET_GRID_HEIGHT    = 0x2A;

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = $this->loadFile($filePath);

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::FILE_SIZE, $this->readFileSize($file));
        $result->add(InfoCommonKeys::FRAME_COUNT, $this->readFrameCount($file));
        $result->add(InfoCommonKeys::RESOLUTION_X, $this->readResolutionX($file));
        $result->add(InfoCommonKeys::RESOLUTION_Y, $this->readResolutionY($file));

        $result->add(InfoCommonKeys::COLOR_DEPTH, $this->readColorDepth($file));
        $result->add(InfoCommonKeys::FLAGS, $this->readFlags($file));
        $result->add(InfoCommonKeys::FRAME_DURATION, $this->readFrameDuration($file));
        $result->add('transparentColorIndex', $this->readTransparentColorIndex($file));
        $result->add(InfoCommonKeys::COLOR_COUNT, $this->readColorCount($file));

        $pixelWidth = $this->readPixelWidth($file);
        $pixelHeight = $this->readPixelHeight($file);

        $result->add(InfoCommonKeys::PIXEL_WIDTH, $pixelWidth);
        $result->add(InfoCommonKeys::PIXEL_HEIGHT, $pixelHeight);
        $result->add(InfoCommonKeys::PIXEL_RATIO, $this->calculatePixelRation($pixelWidth, $pixelHeight));
        $result->add('gridPositionX', $this->readGridPositionX($file));
        $result->add('gridPositionY', $this->readGridPositionY($file));
        $result->add('gridWidth', $this->readGridWidth($file));
        $result->add('gridHeight', $this->readGridHeight($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = $this->loadFile($filePath);

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::FILE_SIZE, $this->readFileSize($file));
        $result->add(InfoCommonKeys::FRAME_COUNT, $this->readFrameCount($file));
        $result->add(InfoCommonKeys::RESOLUTION_X, $this->readResolutionX($file));
        $result->add(InfoCommonKeys::RESOLUTION_Y, $this->readResolutionY($file));

        $result->add(InfoCommonKeys::COLOR_DEPTH, $this->readColorDepth($file));
        $result->add(InfoCommonKeys::COLOR_COUNT, $this->readColorCount($file));

        return $result;
    }

    private function loadFile(string $filePath): File
    {
        $file = File::load($filePath);
        $file->open();

        $valid = $this->verify($file);

        if (!$valid) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        return $file;
    }

    public function verify(File $file): bool
    {
        $file->seek(self::OFFSET_MAGIC);
        $data = $file->read(2);

        return $data === self::ASEPRITE_MAGIC;
    }

    public function readFileSize(File $file): int
    {
        $file->rewind();
        return $file->readUnsignedLongLittleEndian();
    }

    public function readFrameCount(File $file): int
    {
        $file->seek(self::OFFSET_FRAME_COUNT);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readResolutionX(File $file): int
    {
        $file->seek(self::OFFSET_WIDTH);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readResolutionY(File $file): int
    {
        $file->seek(self::OFFSET_HEIGHT);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readColorDepth(File $file): int
    {
        $file->seek(self::OFFSET_COLOR_DEPTH);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readFlags(File $file): UniversalCollection
    {
        $file->seek(self::OFFSET_FLAGS);

        $rawFlags = $file->readUnsignedShortLittleEndian();

        $flags = UniversalCollection::createEmpty();
        $flags->add('validLayerOpacity', (bool)($rawFlags & 1));

        return $flags;
    }

    public function readFrameDuration(File $file): int
    {
        $file->seek(self::OFFSET_FRAME_DURATION);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readTransparentColorIndex(File $file): int
    {
        $file->seek(self::OFFSET_TRANSPARENT_COLOR_INDEX);
        return $file->readUnsignedShortLittleEndian();
    }

    public function readColorCount(File $file): int
    {
        $file->seek(self::OFFSET_COLOR_COUNT);
        $colorCount = $file->readUnsignedShortLittleEndian();

        if ($colorCount === 0) {
            return 256;
        }

        return $colorCount;
    }

    public function readPixelWidth(File $file): int
    {
        $file->seek(self::OFFSET_PIXEL_WIDTH);
        return $file->readUnsignedByte();
    }

    public function readPixelHeight(File $file): int
    {
        $file->seek(self::OFFSET_PIXEL_HEIGHT);
        return $file->readUnsignedByte();
    }

    public function calculatePixelRation(int $pixelWidth, int $pixelHeight): float
    {
        if ($pixelWidth === 0 || $pixelHeight === 0) {
            return 1.;
        }

        return $pixelWidth / $pixelHeight;
    }

    public function readGridPositionX(File $file): int
    {
        $file->seek(self::OFFSET_GRID_POS_X);

        $posX = $file->readUnsignedShortLittleEndian();
        if ($posX >= 32768) {
            $posX -= 65536;
        }

        return $posX;
    }

    public function readGridPositionY(File $file): int
    {
        $file->seek(self::OFFSET_GRID_POS_Y);

        $posY = $file->readUnsignedShortLittleEndian();
        if ($posY >= 32768) {
            $posY -= 65536;
        }

        return $posY;
    }

    public function readGridWidth(File $file): int
    {
        $file->seek(self::OFFSET_GRID_WIDTH);
        $width = $file->readUnsignedShortLittleEndian();

        if ($width === 0) {
            return 16;
        }

        return $width;
    }

    public function readGridHeight(File $file): int
    {
        $file->seek(self::OFFSET_GRID_HEIGHT);
        $height = $file->readUnsignedShortLittleEndian();

        if ($height === 0) {
            return 16;
        }

        return $height;
    }
}
