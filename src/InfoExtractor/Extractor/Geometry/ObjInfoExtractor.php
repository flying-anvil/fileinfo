<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Geometry;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\DataObject\Counter\MultiCounter;
use FlyingAnvil\Libfa\Wrapper\File;

class ObjInfoExtractor implements InfoExtractor
{
    private const IDENTIFIER_VERTEX = 'v ';
    private const IDENTIFIER_FACE   = 'f ';
    private const IDENTIFIER_OBJECT = 'o ';
    private const IDENTIFIER_GROUP  = 'g ';
    private const IDENTIFIER_LINE   = 'l ';

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        return $this->readData($file);
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        return $this->readData($file);
    }

    private function readData(File $file): UniversalCollection
    {
        $file->rewind();
        $counts = MultiCounter::createWithInitialCounts([
            InfoCommonKeys::VERTICES => 0,
            InfoCommonKeys::FACES    => 0,
            InfoCommonKeys::LINES    => 0,
            'groupCount'             => 0,
            'materialCount'          => 0,
        ]);
        $data   = UniversalCollection::createEmpty();

        foreach ($file as $line) {
            $line = trim($line);
            if ($line === '') {
                continue;
            }

            $fistTwoChar = substr($line, 0, 2);

            switch ($fistTwoChar) {
                case self::IDENTIFIER_VERTEX:
                    $counts->increment(InfoCommonKeys::VERTICES);
                    break;
                case self::IDENTIFIER_FACE:
                    $counts->increment(InfoCommonKeys::FACES);
                    break;
                case self::IDENTIFIER_LINE:
                    $counts->increment(InfoCommonKeys::LINES);
                    break;
                case self::IDENTIFIER_GROUP:
                    $counts->increment('groupCount');
                    break;
                case self::IDENTIFIER_OBJECT:
                    $data->add('objectName', substr($line, 2));
                    break;
            }

            if (str_starts_with($line, 'mtllib')) {
                $counts->increment('materialCount');
            }
        }

        $data->merge($counts);
        return $data;
    }
}
