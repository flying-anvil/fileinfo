<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\N64;

use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\N64\SM64SaveInfoExtractor as Extractor;

class SM64Translator
{
    # region Translation Mapping

    private const MAPPING_BOB_OMB_BATTLEFIELD = [
        1 => 'Big Bob-Omb on the Summit',
        2 => 'Footrace with Koopa the Quick',
        3 => 'Shoot to the Island in the Sky',
        4 => 'Find the 8 Red Coins',
        5 => 'Mario Wings to the Sky',
        6 => 'Behind Chain Comp\'s Gate',
    ];

    private const MAPPING_WHOMPS_FORTRESS = [
        1 => 'Chip off Whomp\'s Block',
        2 => 'To the Top of the Fortress',
        3 => 'Shoot into the Wild Blue',
        4 => 'Red Coins on the floating Islad',
        5 => 'Fall onto the caged Island',
        6 => 'Blast Away the Wall',
    ];

    private const MAPPING_JOLLY_ROGER_BAY = [
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_COOL_COOL_MOUNTAIN = [
        1 => 'Slip Slidin\' Away',
        2 => 'Li\'l Penguin Lost',
        3 => 'Big Penguin Race',
        4 => 'Frosty Slide for 8 Red Coins',
        5 => 'Snowman\'s Lot his Head',
        6 => 'Wall Kick Will Work',
    ];

    private const MAPPING_BIG_BOOS_HAUNT = [
        1 => 'Go on a Ghost Hunt',
        2 => 'Ride Big Boo\'s Merry-Go-Round',
        3 => 'Secret of the Haunted Books',
        4 => 'Seek the 8 Red Coins',
        5 => 'Big Boo\'s Balcony',
        6 => 'Eye to Eye in the Secret Room',
    ];

    private const MAPPING_HAZY_MAZE_CAVE = [
        1 => 'Swimming Beast in the Cavern',
        2 => 'Elevate for 8 Red Coins',
        3 => 'Metal-Head MArio can Move',
        4 => 'Navigating the Toxic Maze',
        5 => 'A-maze-ing Emergency Exit',
        6 => 'Watch for Rolling Rocks',
    ];

    private const MAPPING_LETHAL_LAVA_LAND = [
        1 => 'Boil the Big Bully',
        2 => 'Bully the Bullies',
        3 => '8-Coin Puzzle with 15 Pieces',
        4 => 'Red-Hot Log Rolling',
        5 => 'Hot-Foot-It into the Volcano',
        6 => 'Elevator Tour in the Volcano',
    ];

    private const MAPPING_SHIFTING_SAND_LAND = [
        1 => 'In the Talons of the Big Bird',
        2 => 'Shining atop the Pyramid',
        3 => 'Inside the Ancient Pyramid',
        4 => 'Stand Tall on the Four Pillars',
        5 => 'Free Flying for 8 Red Coins',
        6 => 'Pyramid Puzzle',
    ];

    private const MAPPING_DIRE_DIRE_DOCKS = [
        1 => 'Board Bowser\'s Sub',
        2 => 'Chests in the Current',
        3 => 'Pole-Jumping for 8 Red Coins',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_SNOWMANS_LAND = [
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_WET_DRY_WORLD = [
        1 => 'Shocking Arrow Lifts!',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_TALL_TALL_MOUNTAIN = [
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_TINY_HUGE_ISLAND = [
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_TICK_TOCK_CLOCK = [
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    private const MAPPING_RAINBOW_RIDE = [
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
    ];

    # endregion

    private const MAPPING_MAPPING = [
        Extractor::COURSE_BOB_OMB_BATTLEFIELD => self::MAPPING_BOB_OMB_BATTLEFIELD,
        Extractor::COURSE_WHOMPS_FORTRESS     => self::MAPPING_WHOMPS_FORTRESS,
        Extractor::COURSE_JOLLY_ROGER_BAY     => self::MAPPING_JOLLY_ROGER_BAY,
        Extractor::COURSE_COOL_COOL_MOUNTAIN  => self::MAPPING_COOL_COOL_MOUNTAIN,
        Extractor::COURSE_BIG_BOOS_HAUNT      => self::MAPPING_BIG_BOOS_HAUNT,
        Extractor::COURSE_HAZY_MAZE_CAVE      => self::MAPPING_HAZY_MAZE_CAVE,
        Extractor::COURSE_LETHAL_LAVA_LAND    => self::MAPPING_LETHAL_LAVA_LAND,
        Extractor::COURSE_SHIFTING_SAND_LAND  => self::MAPPING_SHIFTING_SAND_LAND,
        Extractor::COURSE_DIRE_DIRE_DOCKS     => self::MAPPING_DIRE_DIRE_DOCKS,
        Extractor::COURSE_SNOWMANS_LAND       => self::MAPPING_SNOWMANS_LAND,
        Extractor::COURSE_WET_DRY_WORLD       => self::MAPPING_WET_DRY_WORLD,
        Extractor::COURSE_TALL_TALL_MOUNTAIN  => self::MAPPING_TALL_TALL_MOUNTAIN,
        Extractor::COURSE_TINY_HUGE_ISLAND    => self::MAPPING_TINY_HUGE_ISLAND,
        Extractor::COURSE_TICK_TOCK_CLOCK     => self::MAPPING_TICK_TOCK_CLOCK,
        Extractor::COURSE_RAINBOW_RIDE        => self::MAPPING_RAINBOW_RIDE,
    ];

    public function translateStarIndexToName(int $courseId, int $starIndex): string
    {
        $mapping = self::MAPPING_MAPPING[$courseId] ?? [];

        $isMainCourse = $mapping !== [];
        if ($isMainCourse) {
            if ($starIndex === 7) {
                return '100 Coin Star';
            }

            return $mapping[$starIndex] ?? 'Unknown';
        }

        return sprintf(
            'Castle Secret Star %d',
            $starIndex,
        );
    }

    private const MAPPING_COURSE_NAME_TO_SHORT = [
        'Bob-omb Battlefield' => 'BOB',
        'Whomp\'s Fortress'   => 'WF',
        'Jolly Roger Bay'     => 'JRB',
        'Cool, Cool Mountain' => 'CCM',
        'Big Boo\'s Haunt'    => 'BBH',
        'Hazy Maze Cave'      => 'HMC',
        'Lethal Lava Land'    => 'LLL',
        'Shifting Sand Land'  => 'SSL',
        'Dire, Dire Docks'    => 'DDD',
        'Snowman\'s Land'     => 'SL',
        'Wet-Dry World'       => 'WDW',
        'Tall, Tall Mountain' => 'TTM',
        'Tiny-Huge Island'    => 'THI',
        'Tick Tock Clock'     => 'TTC',
        'Rainbow Ride'        => 'RR',

        'Bowser in the Dark World' => 'BitDW',
        'Bowser in the Fire Sea'   => 'BitFS',
        'Bowser in the Sky'        => 'BitS',

        'Bowser in the Dark World Battle' => 'BitDWB',
        'Bowser in the Fire Sea Battle'   => 'BitFSB',
        'Bowser in the Sky Battle'        => 'BitSB',

        'Castle'                       => 'C',
        'Castle Grounds'               => 'CG',
        'Castle Courtyard'             => 'CC',
        'The Princess\'s Secret Slide' => 'TPSS',
        'Cavern of the Metal Cap'      => 'CotMC',
        'Tower of the Wing Cap'        => 'TotWC',
        'Vanish Cap Under the Moat'    => 'VCutM',
        'Wing Mario Over the Rainbow'  => 'WMOtR',
        'The Secret Aquarium'          => 'SA',
        'The End'                      => 'End',
    ];

    public function translateCourseNameToShort(string $courseName): string
    {
        return self::MAPPING_COURSE_NAME_TO_SHORT[$courseName] ?? '?';
    }
}
