<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\N64;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class SM64SaveInfoExtractor implements InfoExtractor
{
    public const SAVEGAME_A        = 0x0000;
    public const SAVEGAME_A_BACKUP = 0x0038;
    public const SAVEGAME_B        = 0x0070;
    public const SAVEGAME_B_BACKUP = 0x00A8;
    public const SAVEGAME_C        = 0x00E0;
    public const SAVEGAME_C_BACKUP = 0x0118;
    public const SAVEGAME_D        = 0x0150;
    public const SAVEGAME_D_BACKUP = 0x0188;

    private const OFFSET_SAVEGAME_CHECKSUM = 0x36;

    private const OFFSET_SAVEGAME_LOST_CAP_COURSE = 0x00;
    private const OFFSET_SAVEGAME_LOST_CAP_AREA   = 0x00;
    private const OFFSET_SAVEGAME_LOST_CAP_X      = 0x00;
    private const OFFSET_SAVEGAME_LOST_CAP_Y      = 0x00;
    private const OFFSET_SAVEGAME_LOST_CAP_Z      = 0x00;

    private const OFFSET_SAVEGAME_FLAGS_1 = 0x0B;
    private const OFFSET_SAVEGAME_FLAGS_2 = 0x0A;
    private const OFFSET_SAVEGAME_FLAGS_3 = 0x09;

    private const OFFSET_GLOBAL                  = 0x01C0;
    private const OFFSET_GLOBAL_BACKUP           = 0x01E0;
    private const OFFSET_GLOBAL_SOUND_SETTING    = 0x10;
    private const OFFSET_GLOBAL_LANGUAGE_SETTING = 0x12;
    private const OFFSET_GLOBAL_MAGIC            = 0x1C;
    private const OFFSET_GLOBAL_CHECKSUM         = 0x1E;

    private const VALID_SAVEGAMES = [
        self::SAVEGAME_A        => true,
        self::SAVEGAME_A_BACKUP => true,
        self::SAVEGAME_B        => true,
        self::SAVEGAME_B_BACKUP => true,
        self::SAVEGAME_C        => true,
        self::SAVEGAME_C_BACKUP => true,
        self::SAVEGAME_D        => true,
        self::SAVEGAME_D_BACKUP => true,
    ];

    public const COURSE_BIG_BOOS_HAUNT            = 0x04;
    public const COURSE_COOL_COOL_MOUNTAIN        = 0x05;
    public const COURSE_INSIDE_CASTLE             = 0x06;
    public const COURSE_HAZY_MAZE_CAVE            = 0x07;
    public const COURSE_SHIFTING_SAND_LAND        = 0x08;
    public const COURSE_BOB_OMB_BATTLEFIELD       = 0x09;
    public const COURSE_SNOWMANS_LAND             = 0x0A;
    public const COURSE_WET_DRY_WORLD             = 0x0B;
    public const COURSE_JOLLY_ROGER_BAY           = 0x0C;
    public const COURSE_TINY_HUGE_ISLAND          = 0x0D;
    public const COURSE_TICK_TOCK_CLOCK           = 0x0E;
    public const COURSE_RAINBOW_RIDE              = 0x0F;
    public const COURSE_CASTLE_GROUNDS            = 0x10;
    public const COURSE_BOWSER_DARK_WORLD         = 0x11;
    public const COURSE_VANISH_CAP                = 0x12;
    public const COURSE_BOWSERS_FIRE_SEA          = 0x13;
    public const COURSE_SECRET_AQUARIUM           = 0x14;
    public const COURSE_BOWSER_SKY                = 0x15;
    public const COURSE_LETHAL_LAVA_LAND          = 0x16;
    public const COURSE_DIRE_DIRE_DOCKS           = 0x17;
    public const COURSE_WHOMPS_FORTRESS           = 0x18;
    public const COURSE_END_CAKE_PICTURE          = 0x19;
    public const COURSE_CASTLE_COURTYARD          = 0x1A;
    public const COURSE_PEACH_SECRET_SLIDE        = 0x1B;
    public const COURSE_METAL_CAP                 = 0x1C;
    public const COURSE_WING_CAP                  = 0x1D;
    public const COURSE_BOWSER_DARK_WORLD_BATTLE  = 0x1E;
    public const COURSE_RAINBOW_CLOUDS            = 0x1F;
    public const COURSE_BOWSERS_FIRE_SEA_BATTLE   = 0x21;
    public const COURSE_BOWSER_SKY_BATTLE         = 0x22;
    public const COURSE_TALL_TALL_MOUNTAIN        = 0x24;

    private const MAPPING_COURSE_ID_NAME = [
        self::COURSE_BOB_OMB_BATTLEFIELD => 'Bob-omb Battlefield',
        self::COURSE_WHOMPS_FORTRESS     => 'Whomp\'s Fortress',
        self::COURSE_JOLLY_ROGER_BAY     => 'Jolly Roger Bay',
        self::COURSE_COOL_COOL_MOUNTAIN  => 'Cool, Cool Mountain',
        self::COURSE_BIG_BOOS_HAUNT      => 'Big Boo\'s Haunt',
        self::COURSE_HAZY_MAZE_CAVE      => 'Hazy Maze Cave',
        self::COURSE_LETHAL_LAVA_LAND    => 'Lethal Lava Land',
        self::COURSE_SHIFTING_SAND_LAND  => 'Shifting Sand Land',
        self::COURSE_DIRE_DIRE_DOCKS     => 'Dire, Dire Docks',
        self::COURSE_SNOWMANS_LAND       => 'Snowman\'s Land',
        self::COURSE_WET_DRY_WORLD       => 'Wet-Dry World',
        self::COURSE_TALL_TALL_MOUNTAIN  => 'Tall, Tall Mountain',
        self::COURSE_TINY_HUGE_ISLAND    => 'Tiny-Huge Island',
        self::COURSE_TICK_TOCK_CLOCK     => 'Tick Tock Clock',
        self::COURSE_RAINBOW_RIDE        => 'Rainbow Ride',

        self::COURSE_BOWSER_DARK_WORLD => 'Bowser in the Dark World',
        self::COURSE_BOWSERS_FIRE_SEA  => 'Bowser in the Fire Sea',
        self::COURSE_BOWSER_SKY        => 'Bowser in the Sky',

        self::COURSE_BOWSER_DARK_WORLD_BATTLE => 'Bowser in the Dark World Battle',
        self::COURSE_BOWSERS_FIRE_SEA_BATTLE  => 'Bowser in the Fire Sea Battle',
        self::COURSE_BOWSER_SKY_BATTLE        => 'Bowser in the Sky Battle',

        self::COURSE_INSIDE_CASTLE      => 'Castle',
        self::COURSE_CASTLE_GROUNDS     => 'Castle Grounds',
        self::COURSE_CASTLE_COURTYARD   => 'Castle Courtyard',
        self::COURSE_PEACH_SECRET_SLIDE => 'The Princess\'s Secret Slide',
        self::COURSE_METAL_CAP          => 'Cavern of the Metal Cap',
        self::COURSE_WING_CAP           => 'Tower of the Wing Cap',
        self::COURSE_VANISH_CAP         => 'Vanish Cap Under the Moat',
        self::COURSE_RAINBOW_CLOUDS     => 'Wing Mario Over the Rainbow',
        self::COURSE_SECRET_AQUARIUM    => 'The Secret Aquarium',
        self::COURSE_END_CAKE_PICTURE   => 'The End',
    ];

    private const MAPPING_COURSE_ID_HIGHSCORE_OFFSET = [
        self::COURSE_BOB_OMB_BATTLEFIELD => 0x25,
        self::COURSE_WHOMPS_FORTRESS     => 0x26,
        self::COURSE_JOLLY_ROGER_BAY     => 0x27,
        self::COURSE_COOL_COOL_MOUNTAIN  => 0x28,
        self::COURSE_BIG_BOOS_HAUNT      => 0x29,
        self::COURSE_HAZY_MAZE_CAVE      => 0x2A,
        self::COURSE_LETHAL_LAVA_LAND    => 0x2B,
        self::COURSE_SHIFTING_SAND_LAND  => 0x2C,
        self::COURSE_DIRE_DIRE_DOCKS     => 0x2D,
        self::COURSE_SNOWMANS_LAND       => 0x2E,
        self::COURSE_WET_DRY_WORLD       => 0x2F,
        self::COURSE_TALL_TALL_MOUNTAIN  => 0x30,
        self::COURSE_TINY_HUGE_ISLAND    => 0x31,
        self::COURSE_TICK_TOCK_CLOCK     => 0x32,
        self::COURSE_RAINBOW_RIDE        => 0x33,
    ];

    private const MAPPING_COURSE_ID_LEVEL_FLAGS_OFFSET = [
        self::COURSE_INSIDE_CASTLE       => 0x08,
        self::COURSE_BOB_OMB_BATTLEFIELD => 0x0C,
        self::COURSE_WHOMPS_FORTRESS     => 0x0D,
        self::COURSE_JOLLY_ROGER_BAY     => 0x0E,
        self::COURSE_COOL_COOL_MOUNTAIN  => 0x0F,
        self::COURSE_BIG_BOOS_HAUNT      => 0x10,
        self::COURSE_HAZY_MAZE_CAVE      => 0x11,
        self::COURSE_LETHAL_LAVA_LAND    => 0x12,
        self::COURSE_SHIFTING_SAND_LAND  => 0x13,
        self::COURSE_DIRE_DIRE_DOCKS     => 0x14,
        self::COURSE_SNOWMANS_LAND       => 0x15,
        self::COURSE_WET_DRY_WORLD       => 0x16,
        self::COURSE_TALL_TALL_MOUNTAIN  => 0x17,
        self::COURSE_TINY_HUGE_ISLAND    => 0x18,
        self::COURSE_TICK_TOCK_CLOCK     => 0x19,
        self::COURSE_RAINBOW_RIDE        => 0x1A,
        self::COURSE_BOWSER_DARK_WORLD   => 0x1B,
        self::COURSE_BOWSERS_FIRE_SEA    => 0x1C,
        self::COURSE_BOWSER_SKY          => 0x1D,
        self::COURSE_PEACH_SECRET_SLIDE  => 0x1E,
        self::COURSE_METAL_CAP           => 0x1F,
        self::COURSE_WING_CAP            => 0x20,
        self::COURSE_VANISH_CAP          => 0x21,
        self::COURSE_RAINBOW_CLOUDS      => 0x22,
        self::COURSE_SECRET_AQUARIUM     => 0x23,
        self::COURSE_END_CAKE_PICTURE    => 0x24,
    ];

    private const MAIN_COURSES = [
        self::COURSE_BOB_OMB_BATTLEFIELD => self::COURSE_BOB_OMB_BATTLEFIELD,
        self::COURSE_WHOMPS_FORTRESS     => self::COURSE_WHOMPS_FORTRESS,
        self::COURSE_JOLLY_ROGER_BAY     => self::COURSE_JOLLY_ROGER_BAY,
        self::COURSE_COOL_COOL_MOUNTAIN  => self::COURSE_COOL_COOL_MOUNTAIN,
        self::COURSE_BIG_BOOS_HAUNT      => self::COURSE_BIG_BOOS_HAUNT,
        self::COURSE_HAZY_MAZE_CAVE      => self::COURSE_HAZY_MAZE_CAVE,
        self::COURSE_LETHAL_LAVA_LAND    => self::COURSE_LETHAL_LAVA_LAND,
        self::COURSE_SHIFTING_SAND_LAND  => self::COURSE_SHIFTING_SAND_LAND,
        self::COURSE_DIRE_DIRE_DOCKS     => self::COURSE_DIRE_DIRE_DOCKS,
        self::COURSE_SNOWMANS_LAND       => self::COURSE_SNOWMANS_LAND,
        self::COURSE_WET_DRY_WORLD       => self::COURSE_WET_DRY_WORLD,
        self::COURSE_TALL_TALL_MOUNTAIN  => self::COURSE_TALL_TALL_MOUNTAIN,
        self::COURSE_TINY_HUGE_ISLAND    => self::COURSE_TINY_HUGE_ISLAND,
        self::COURSE_TICK_TOCK_CLOCK     => self::COURSE_TICK_TOCK_CLOCK,
        self::COURSE_RAINBOW_RIDE        => self::COURSE_RAINBOW_RIDE,
    ];

    /** @var SM64Translator */
    private $translator;

    public function __construct()
    {
        $this->translator = new SM64Translator();
    }

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw new NotResponsibleExtractorException('Magic Mismatch');
        }

        $result = UniversalCollection::createEmpty();
        $result->add('globalData', $this->readGlobalData($file));

        $savegameData = UniversalCollection::createEmpty();

        $letter = 'a';
        foreach (array_keys(self::VALID_SAVEGAMES) as $index => $savegame) {
            $currentSavegameData = $this->readSavegame($file, $savegame);

            $suffix = '';

            if ($index % 2 !== 0) {
                $suffix = 'Backup';
            }

            $key = $letter . $suffix;

            $savegameData->add($key, $currentSavegameData);

            if ($suffix === 'Backup') {
                $letter++;
            }

            // TODO: remove break
            break;
        }

        $result->add('savegameData', $savegameData);

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw new NotResponsibleExtractorException('Magic Mismatch');
        }

        $result = UniversalCollection::createEmpty();

        $letter        = 'A';
        $maxHighscores = [];
        $savegames     = [
            self::SAVEGAME_A,
            self::SAVEGAME_B,
            self::SAVEGAME_C,
            self::SAVEGAME_D,
        ];

        foreach ($savegames as $savegame) {
            $result->add('starCountSavegame' . $letter, $this->readStarCount($file, $savegame));

            $highscores = $this->readHighScores($file, $savegame);
            foreach ($highscores as $courseName => $highscore) {
                $maxHighscores[$courseName] = max($maxHighscores[$courseName] ?? 0, $highscore);
            }

            $letter++;
        }

        foreach ($maxHighscores as $courseName => $highscore) {
            $result->add('highscore' . $this->translator->translateCourseNameToShort($courseName), $highscore);
        }

        return $result;
    }

    public function verify(File $file): bool
    {
        foreach (array_keys(self::VALID_SAVEGAMES) as $savegame) {
            $file->seek($savegame + 0x34);
            if ($file->read(2) !== 'DA') {
                return false;
            }
        }

        $globalOffsets = [
            self::OFFSET_GLOBAL + self::OFFSET_GLOBAL_MAGIC,
            self::OFFSET_GLOBAL_BACKUP + self::OFFSET_GLOBAL_MAGIC,
        ];

        foreach ($globalOffsets as $offset) {
            $file->seek($offset);
            if ($file->read(2) !== 'HI') {
                return false;
            }
        }

        return true;
    }

    public function readGlobalData(File $file): UniversalCollection
    {
        $data = UniversalCollection::createEmpty();

        $data->add('soundSetting', $this->readSoundSetting($file));
        $data->add('languageSetting', $this->readLanguageSetting($file));
        $data->add('globalChecksum', $this->readGlobalChecksum($file));

        return $data;
    }

    # region Savegame Data

    /**
     * @param File $file
     * @param int $savegame one of the SAVEGAME constants of this class
     * @return UniversalCollection
     */
    public function readSavegame(File $file, int $savegame): UniversalCollection
    {
        $this->verifySavegameOffset($savegame);

        $data = UniversalCollection::createEmpty();
        $data->add('checksumData', $this->readChecksumData($file, $savegame));
        $data->add('lostCap', $this->readLostCap($file, $savegame));
        $data->add('levelData', $this->readLevelData($file, $savegame));
        $data->add('starCount', $this->readStarCount($file, $savegame));
        $data->add(InfoCommonKeys::FLAGS, $this->readFlags($file, $savegame));
        $data->add('highScores', $this->readHighScores($file, $savegame));

        return $data;
    }

    public function readChecksumData(File $file, int $savegame): UniversalCollection
    {
        $this->verifySavegameOffset($savegame);

        $file->seek($savegame + self::OFFSET_SAVEGAME_CHECKSUM);
        $readChecksum = $file->readUnsignedShortBigEndian();
        $calculatedChecksum = $this->calculateChecksum($file, $savegame);

        $data = UniversalCollection::createEmpty();
        $data->add('read', $readChecksum);
        $data->add('calculated', $calculatedChecksum);
        $data->add('valid', $readChecksum === $calculatedChecksum);

        return $data;
    }

    public function calculateChecksum(File $file, int $savegame): int
    {
        $this->verifySavegameOffset($savegame);

        $file->seek($savegame);

        $sum = 0;
        for ($i = 0; $i <= 0x35; $i++) {
            $sum += $file->readUnsignedByte();
        }

        return $sum;
    }

    public function readLostCap(File $file, int $savegame)
    {
        $this->verifySavegameOffset($savegame);

        $file->seek($savegame + self::OFFSET_SAVEGAME_LOST_CAP_COURSE);
        $courseId   = $file->readUnsignedByte();
        $courseName = [
            // Different Mapping than used by level flags
            0x08 => 'Shifting Sand Land',
            0x0A => 'Snowman\'s Land',
            0x24 => 'Tall, Tall Mountain',
        ][$courseId] ?? 'Unknown';

        $file->seek($savegame + self::OFFSET_SAVEGAME_LOST_CAP_AREA);
        $areaId = $file->readUnsignedByte();

        $file->seek($savegame + self::OFFSET_SAVEGAME_LOST_CAP_X);
        $positionX = $file->readUnsignedShortBigEndian();

        $file->seek($savegame + self::OFFSET_SAVEGAME_LOST_CAP_Y);
        $positionY = $file->readUnsignedShortBigEndian();

        $file->seek($savegame + self::OFFSET_SAVEGAME_LOST_CAP_Z);
        $positionZ = $file->readUnsignedShortBigEndian();

        $data = UniversalCollection::createEmpty();
        $data->add('courseId', $courseId);
        $data->add('courseName', $courseName);
        $data->add('areaId', $areaId);
        $data->add(InfoCommonKeys::COORDINATE_X, $positionX);
        $data->add(InfoCommonKeys::COORDINATE_Y, $positionY);
        $data->add(InfoCommonKeys::COORDINATE_Z, $positionZ);

        return $data;
    }

    private function readFlags(File $file, int $savegame)
    {
        $this->verifySavegameOffset($savegame);

        $flags = UniversalCollection::createEmpty();

        $file->seek($savegame + self::OFFSET_SAVEGAME_FLAGS_1);
        $flags1 = $file->readUnsignedByte();

        $flags->add('fileInUse',               (bool)($flags1 & 0b00000001));
        $flags->add('unlockedWingCap',         (bool)($flags1 & 0b00000010));
        $flags->add('unlockedMetalCap',        (bool)($flags1 & 0b00000100));
        $flags->add('unlockedVanishCap',       (bool)($flags1 & 0b00001000));
        $flags->add('hasKeyBasement',          (bool)($flags1 & 0b00010000));
        $flags->add('hasKeyUpstairs',          (bool)($flags1 & 0b00100000));
        $flags->add('keyDoorUnlockedBasement', (bool)($flags1 & 0b01000000));
        $flags->add('keyDoorUnlockedUpstairs', (bool)($flags1 & 0b10000000));

        $file->seek($savegame + self::OFFSET_SAVEGAME_FLAGS_2);
        $flags2 = $file->readUnsignedByte();

        $flags->add('direDireDocksPortalMoved',     (bool)($flags2 & 0b00000001));
        $flags->add('moatDrained',                  (bool)($flags2 & 0b00000010));
        $flags->add('starDoorSeenSecretSlide',      (bool)($flags2 & 0b00000100));
        $flags->add('starDoorSeenWhompsFortress',   (bool)($flags2 & 0b00001000));
        $flags->add('starDoorSeenCoolCoolMountain', (bool)($flags2 & 0b00010000));
        $flags->add('starDoorSeenJollyRogerBay',    (bool)($flags2 & 0b00100000));
        $flags->add('starDoorSeenBowserDarkWorld',  (bool)($flags2 & 0b01000000));
        $flags->add('starDoorSeenBowserFireSea',    (bool)($flags2 & 0b10000000));

        $file->seek($savegame + self::OFFSET_SAVEGAME_FLAGS_3);
        $flags3 = $file->readUnsignedByte();

        $flags->add('lostCapByLevelId',          (bool)($flags3 & 0b00000001));
        $flags->add('lostCapInShiftingSandLand', (bool)($flags3 & 0b00000010));
        $flags->add('lostCapTallTallMountain',   (bool)($flags3 & 0b00000100));
        $flags->add('lostCapSnowmansLand',       (bool)($flags3 & 0b00001000));
        $flags->add('starDoorSeen3rdFloor',      (bool)($flags3 & 0b00010000));
        $flags->add('unused1',                   (bool)($flags3 & 0b00100000));
        $flags->add('unused2',                   (bool)($flags3 & 0b01000000));
        $flags->add('unused3',                   (bool)($flags3 & 0b10000000));

        return $flags;
    }

    private function readLevelData(File $file, int $savegame): UniversalCollection
    {
        $this->verifySavegameOffset($savegame);

        $courseData = UniversalCollection::createEmpty();

        foreach (self::MAPPING_COURSE_ID_LEVEL_FLAGS_OFFSET as $course => $_) {
            $courseData->add(
                $this->courseIdToName($course),
                $this->readLevelDataById($file, $savegame, $course),
            );
        }

        return $courseData;
    }

    public function readLevelDataById(File $file, int $savegame, int $courseId): UniversalCollection
    {
        $this->verifySavegameOffset($savegame);
        $this->verifyCourseIdLevelFlags($courseId);

        $offset = self::MAPPING_COURSE_ID_LEVEL_FLAGS_OFFSET[$courseId];
        $file->seek($savegame + $offset);
        $flags = $file->readUnsignedByte();

        $starDataIndexed = UniversalCollection::createEmpty();
        $starDataIndexed->add('1', (bool)($flags & 0b00000001));
        $starDataIndexed->add('2', (bool)($flags & 0b00000010));
        $starDataIndexed->add('3', (bool)($flags & 0b00000100));
        $starDataIndexed->add('4', (bool)($flags & 0b00001000));
        $starDataIndexed->add('5', (bool)($flags & 0b00010000));
        $starDataIndexed->add('6', (bool)($flags & 0b00100000));
        $starDataIndexed->add('7', (bool)($flags & 0b01000000));

        $starDataNamed = UniversalCollection::createEmpty();
        $starDataNamed->add($this->starIndexToName($courseId, 1), (bool)($flags & 0b00000001));
        $starDataNamed->add($this->starIndexToName($courseId, 2), (bool)($flags & 0b00000010));
        $starDataNamed->add($this->starIndexToName($courseId, 3), (bool)($flags & 0b00000100));
        $starDataNamed->add($this->starIndexToName($courseId, 4), (bool)($flags & 0b00001000));
        $starDataNamed->add($this->starIndexToName($courseId, 5), (bool)($flags & 0b00010000));
        $starDataNamed->add($this->starIndexToName($courseId, 6), (bool)($flags & 0b00100000));
        $starDataNamed->add($this->starIndexToName($courseId, 7), (bool)($flags & 0b01000000));

        $data = UniversalCollection::createEmpty();
        $data->add('starDataIndexed', $starDataIndexed);
        $data->add('starDataNamed', $starDataNamed);
        $data->add('cannonUnlocked', null);

        return $data;
    }

    public function readStarCount(File $file, int $savegame): int
    {
        $this->verifySavegameOffset($savegame);

        $sum = 0;
        foreach (self::MAPPING_COURSE_ID_LEVEL_FLAGS_OFFSET as $course => $_) {
            $sum += $this->readCourseStarCount($file, $savegame, $course);
        }

        return $sum;
    }

    public function readCourseStarCount(File $file, int $savegame, int $courseId): int
    {
        $this->verifySavegameOffset($savegame);
        $this->verifyCourseIdLevelFlags($courseId);

        $offset = self::MAPPING_COURSE_ID_LEVEL_FLAGS_OFFSET[$courseId];
        $file->seek($savegame + $offset);
        $flags = $file->readUnsignedByte() & 0b01111111; // Remove cannon unlocked flag

        // Convert int to 01 string and count 1s
        return substr_count(decbin($flags), '1');
    }

    public function readHighScores(File $file, int $savegame): UniversalCollection
    {
        $this->verifySavegameOffset($savegame);

        $highscores = UniversalCollection::createEmpty();

        foreach (self::MAIN_COURSES as $course) {
            $highscores->add(
                $this->courseIdToName($course),
                $this->readCourseHighscore($file, $savegame, $course),
            );
        }

        return $highscores;
    }

    public function readCourseHighscore(File $file, int $savegame, int $courseId): int
    {
        $this->verifySavegameOffset($savegame);
        $this->verifyCourseIdHighscore($courseId);

        $offset = self::MAPPING_COURSE_ID_HIGHSCORE_OFFSET[$courseId];
        $file->seek($savegame + $offset);

        return $file->readUnsignedByte();
    }

    # endregion

    # region Global Data

    public function readSoundSetting(File $file): string
    {
        $file->seek(self::OFFSET_GLOBAL + self::OFFSET_GLOBAL_SOUND_SETTING);
        $rawData = $file->readUnsignedShortBigEndian();

        return match ($rawData) {
            0x00 => 'Stereo',
            0x01 => 'Mono',
            0x02 => 'Headset',

            default => 'Unknown',
        };
    }

    public function readLanguageSetting(File $file): string
    {
        $file->seek(self::OFFSET_GLOBAL + self::OFFSET_GLOBAL_LANGUAGE_SETTING);
        $rawData = $file->readUnsignedShortBigEndian();

        return match ($rawData) {
            0x00 => 'English',
            0x01 => 'French',
            0x02 => 'German',

            default => 'Unknown',
        };
    }

    public function readGlobalChecksum(File $file): int
    {
        $file->seek(self::OFFSET_GLOBAL + self::OFFSET_GLOBAL_CHECKSUM);
        return $file->readUnsignedShortBigEndian();
    }

    # endregion

    public function courseIdToName(int $courseId): string
    {
        return self::MAPPING_COURSE_ID_NAME[$courseId] ?? 'Unknown';
    }

    public function starIndexToName(int $courseId, int $starIndex): string
    {
        return $this->translator->translateStarIndexToName($courseId, $starIndex);
    }

    private function verifySavegameOffset(int $savegameOffset): void
    {
        if (!self::VALID_SAVEGAMES[$savegameOffset]) {
            // TODO: Throw better exception
            throw new \Exception('Invalid savegame given');
        }
    }

    private function verifyCourseIdHighscore(int $courseId): void
    {
        if (!isset(self::MAPPING_COURSE_ID_HIGHSCORE_OFFSET[$courseId])) {
            // TODO: Throw better exception
            throw new \Exception(sprintf(
                'CourseId %d has no highscore',
                $courseId,
            ));
        }
    }

    private function verifyCourseIdLevelFlags(int $course)
    {
        if (!isset(self::MAPPING_COURSE_ID_LEVEL_FLAGS_OFFSET[$course])) {
            // TODO: Throw better exception
            throw new \Exception(sprintf(
                'CourseId %d has no level flags',
                $course,
            ));
        }
    }
}
