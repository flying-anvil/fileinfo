<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Nds;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * https://dsibrew.org/wiki/DSi_Cartridge_Header
 * https://fabiensanglard.net/another_world_polygons_GBA/gbatech.html    (DS Cartridge Header)
 */
class NdsInfoExtractor implements InfoExtractor
{
    private const UNIT_CODES = [
        0x00 => 'NDS',
        0x02 => 'NDS+DSi',
        0x03 => 'DSi',
    ];

    private const REGIONS = [
        0x00 => 'Normal',
        0x40 => 'Korea',
        0x80 => 'China',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file));
        $result->add(InfoCommonKeys::GAME_CODE, $this->readGameCode($file));
        $result->add(InfoCommonKeys::MAKER_CODE, $this->readMakerCode($file));
        $result->add('unitCode', $this->readUnitCode($file));
        $result->add('encryptionSeedSelect', $this->readEncryptionSeedSelect($file));
        $result->add('deviceCapacity', $this->readDeviceCapacity($file));
//        $result->add('gameRevision', $this->readGameRevision($file)); // TODO: Only used by DSi titles
        $result->add('ndsRegion', $this->readNdsRegion($file));
        $result->add('romVersion', $this->readRomVersion($file));
        $result->add('internalFlags', $this->readInternalFlags($file));
        $result->add(InfoCommonKeys::ROM_SIZE, $this->readRomSize($file));
        $result->add('romHeaderSize', $this->readRomHeaderSize($file));

//        $result->add('nintendoLogo', $this->readNintendoLogo($file));
        $result->add(InfoCommonKeys::HEADER_CHECKSUM, $this->readHeaderChecksum($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file));
        $result->add('ndsRegion', $this->readNdsRegion($file));
        $result->add('romVersion', $this->readRomVersion($file));
        $result->add(InfoCommonKeys::ROM_SIZE, $this->readRomSize($file));

        return $result;
    }

    private function readGameTitle(File $file): string
    {
        $file->rewind();
        $title = $file->read(12);

        return rtrim($title);
    }

    /**
     * @noinspection OneTimeUseVariablesInspection
     * @noinspection PhpUnnecessaryLocalVariableInspection
     */
    private function readGameCode(File $file): string
    {
        $file->seek(0xC);
        $code = $file->read(4);

        return $code;
    }

    private function readMakerCode(File $file)
    {
        $file->seek(0x10);
        $rawCode = $file->read(2);

        return hexdec(bin2hex($rawCode));
    }

    private function readUnitCode(File $file): string
    {
        $file->seek(0x12);
        $rawCode = $file->read(1);

        return self::UNIT_CODES[(ord($rawCode))];
    }

    private function readEncryptionSeedSelect(File $file): int
    {
        $file->seek(0x13);
        $rawValue = $file->read(1);

        return ord($rawValue);
    }

    private function readDeviceCapacity(File $file): int
    {
        $file->seek(0x14);
        $capacityMagnitude = $file->read(1);
        $capacityMagnitude = ord($capacityMagnitude);

        $kb128 = 2 << 16;

        return $kb128 << $capacityMagnitude;
    }

    private function readNdsRegion(File $file): string
    {
        $file->seek(0x1D);
        $rawRegion = $file->read(1);
        $rawRegion = ord($rawRegion);

        return self::REGIONS[$rawRegion];
    }

    private function readRomVersion(File $file): int
    {
        $file->seek(0x1E);
        $rawVersion = $file->read(1);
        $rawVersion = ord($rawVersion);

        return 1 + $rawVersion;
    }

    private function readInternalFlags(File $file): UniversalCollection
    {
        $flags = UniversalCollection::createEmpty();

        $file->seek(0x1F);
        $rawFlags = $file->read(1);
        $rawFlags = ord($rawFlags);

        $flags->add('Autostart', ($rawFlags & 0b01000000) > 0);

        return $flags;
    }

    private function readRomSize(File $file): int
    {
        $file->seek(0x80);
        $romSize = $file->read(4);

        // BigEndian binary to LittleEndian uint
        $romSize = unpack('V', $romSize)[1];

        return $romSize;
    }

    /**
     * Should always be 0x4000 (16384)
     */
    private function readRomHeaderSize(File $file): int
    {
        $file->seek(0x84);
        $headerSize = $file->read(4);

        // BigEndian binary to LittleEndian uint
        $headerSize = unpack('V', $headerSize)[1];

        return $headerSize;
    }

    public function readNintendoLogo(File $file): string
    {
        $file->seek(0xC0);
        $nintendoLogo = $file->read(0x9C);

        return '0x' . bin2hex($nintendoLogo);
    }

    private function readHeaderChecksum(File $file): string
    {
        $file->seek(0x15E);
        $headerChecksum = $file->read(2);

        return '0x' . strtoupper(bin2hex($headerChecksum));
    }
}
