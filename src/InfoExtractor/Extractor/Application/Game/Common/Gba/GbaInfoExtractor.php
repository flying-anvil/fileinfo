<?php /** @noinspection DuplicatedCode */

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Gba;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * https://fabiensanglard.net/another_world_polygons_GBA/gbatech.html    (GBA Cartridge Header)
 */
class GbaInfoExtractor implements InfoExtractor
{
    public const GBA_MAGIC = 0x96;

    private const OFFSET_GAME_TITLE = 0xA0;
    private const OFFSET_GAME_CODE = 0xAC;
    private const OFFSET_MAGIC = 0xB2;
    private const OFFSET_MAKER_CODE = 0xB0;
    private const OFFSET_VERSION = 0xBC;
    private const OFFSET_HEADER_CHECKSUM = 0xBD;
    private const OFFSET_BOOTMODE = 0xC4;

    private const CARTRIDGE_FEATURES = [
        'A' => 'Older title',
        'B' => 'Newer title',
        'C' => 'Even newer title',
        'F' => 'Software NES emulator',
        'K' => 'Acceleration sensor',
        'P' => 'e-Reader',
        'R' => 'Rumble and gyro sensor',
        'U' => 'RTC and solar sensor',
        'V' => 'Rumble',
    ];

    private const LOCALIZATION_MAPPING = [
        'J' => 'Japan',
        'E' => 'USA',
        'P' => 'Europe',
        'D' => 'Germany',
        'F' => 'France',
        'I' => 'Italy',
        'S' => 'Spain',
    ];

    private const BOOTMODE_MAPPING = [
        0x00 => 'Default',
        0x01 => 'Joybus',
        0x02 => 'Normal',
        0x03 => 'Miltiplay',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file = File::load($filePath);
        $file->open();

        if (!$this->validate($file)) {
            return $result;
        }

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file));

        $gameCode = $this->readGameCode($file);
        $result->add(InfoCommonKeys::GAME_CODE, $gameCode);
        $result->merge($this->parseGameCode($gameCode));

        $result->add(InfoCommonKeys::MAKER_CODE, $this->readMakerCode($file));
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($file));
        $result->add(InfoCommonKeys::HEADER_CHECKSUM, $this->readHeaderChecksum($file));
        $result->add('validHeader', $this->verifyHeader($file));
        $result->add('bootMode', $this->readBootmode($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file = File::load($filePath);
        $file->open();

        if (!$this->validate($file)) {
            return $result;
        }

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file));

        $features = $this->parseGameCode($this->readGameCode($file));
        $features->remove('titleAbbreviation');

        $result->merge($features);
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($file));

        return $result;
    }

    public function validate(File $file): bool
    {
        $file->seek(self::OFFSET_MAGIC);

        $rawCode = $file->read(1);
        return ord($rawCode) === self::GBA_MAGIC;
    }

    public function readGameTitle(File $file): string
    {
        $file->seek(self::OFFSET_GAME_TITLE);

        $rawTitle = $file->read(12);

        return rtrim($rawTitle);
    }

    public function readGameCode(File $file): string
    {
        $file->seek(self::OFFSET_GAME_CODE);

        return $file->read(4);
    }

    public function readMakerCode(File $file): string
    {
        $file->seek(self::OFFSET_MAKER_CODE);

        return $file->read(2);
    }

    public function readVersion(File $file): int
    {
        $file->seek(self::OFFSET_VERSION);

        return ord($file->read(1)) + 1;
    }

    public function readBootmode(File $file): string
    {
        $file->seek(self::OFFSET_BOOTMODE);

        $bootmode = ord($file->read(1));
        return self::BOOTMODE_MAPPING[$bootmode] ?? 'unknown';
    }

    public function parseGameCode(string $gameCode): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $result->add('cartridgeFeature', self::CARTRIDGE_FEATURES[$gameCode[0]] ?? 'unknown');
        $result->add('titleAbbreviation', substr($gameCode, 1, 2));
        $result->add(InfoCommonKeys::LOCALIZATION, self::LOCALIZATION_MAPPING[$gameCode[3]] ?? 'unknown');

        return $result;
    }

    public function verifyHeader(File $file): bool
    {
        $file->seek(self::OFFSET_GAME_TITLE);
        $check = 0;

        for ($i = self::OFFSET_GAME_TITLE; $i <= self::OFFSET_VERSION; $i++) {
            $check -= ord($file->readChar());
        }

        $check -= 0x19;
        $check &= 0xFF;
        $headerChecksum = hexdec($this->readHeaderChecksum($file));
        return $headerChecksum === $check;
    }

    public function readHeaderChecksum(File $file): string
    {
        $file->seek(self::OFFSET_HEADER_CHECKSUM);
        return '0x' . strtoupper(bin2hex($file->readChar()));
    }
}
