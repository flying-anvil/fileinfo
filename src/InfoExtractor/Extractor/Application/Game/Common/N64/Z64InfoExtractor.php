<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * http://forum.pj64-emu.com/showthread.php?t=2239
 * http://en64.shoutwiki.com/wiki/ROM
 */
class Z64InfoExtractor implements InfoExtractor
{
    private const COUNTRY_CODES = [
        '7' => 'Beta',
        'A' => 'Asian (NTSC)',
        'B' => 'Brazilian',
        'C' => 'Chinese',
        'D' => 'German',
        'E' => 'North America',
        'F' => 'French',
        'G' => 'Gateway 64 (NTSC)',
        'H' => 'Dutch',
        'I' => 'Italian',
        'J' => 'Japanese',
        'K' => 'Korean',
        'L' => 'Gateway 64 (PAL)',
        'N' => 'Canadian',
        'P' => 'European',
        'S' => 'Spanish',
        'U' => 'Australian',
        'W' => 'Scandinavian',
        'X' => 'European',
        'Y' => 'European',
        'Z' => 'Others',
    ];

    private const ROM_TYPES = [
        'N' => 'cart',
        'D' => '64DD disk',
        'C' => 'cartridge part of expandable game',
        'E' => '64DD expansion for cart',
        'Z' => 'Aleck64 cart',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $rom = File::load($filePath);
        $rom->open();

        $countryCode = $this->readCountryCode($rom);

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameName($rom));
        $result->add('romType', $this->readRomType($rom));
        $result->add(InfoCommonKeys::COUNTRY_CODE, $countryCode);
        $result->add(InfoCommonKeys::COUNTRY_CODE_TRANSLATION, $this->translateCountryCode($countryCode));

        $result->add('gameID', $this->readGameId($rom));
        $result->add('clockRateOverride', $this->readClockRateOverride($rom));
        $result->add('CRC1', $this->readCrc1($rom));
        $result->add('CRC2', $this->readCrc2($rom));
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($rom));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $rom = File::load($filePath);
        $rom->open();

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameName($rom));
        $result->add('romType', $this->readRomType($rom));

        $countryCode = $this->readCountryCode($rom);
        $result->add(InfoCommonKeys::COUNTRY_CODE, $countryCode);
        $result->add(InfoCommonKeys::COUNTRY_CODE_TRANSLATION, $this->translateCountryCode($countryCode));
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($rom));

        return $result;
    }

    public function readGameName(File $file): string
    {
        $file->seek(0x20);
        return rtrim($file->read(20));
    }

    public function readGameId(File $file): string
    {
        $file->seek(0x3C);
        return $file->read(2);
    }

    public function readCountryCode(File $file): string
    {
        $file->seek(0x3E);
        return $file->read(1);
    }

    public function readRomType(File $file): string
    {
        $file->seek(0x3B);
        $type = $file->read(1);

        return self::ROM_TYPES[$type] ?? 'Unknown';
    }

    // ===== ADVANCED ============================

    public function readClockRateOverride(File $file): int
    {
        $file->seek(0x4);
        $value = $file->read(4);
        $hex   = bin2hex($value);

        $hexNoLowerNybble = substr($hex, 0, -1);
        return hexdec($hexNoLowerNybble);
    }

    public function readCrc1(File $file): string
    {
       $file->seek(0x10);
       $crc1 = $file->read(4);

       return '0x' . strtoupper(bin2hex($crc1));
    }

    public function readCrc2(File $file): string
    {
       $file->seek(0x14);
       $crc1 = $file->read(4);

       return '0x' . strtoupper(bin2hex($crc1));
    }

    public function readVersion(File $file): string
    {
        $file->seek(0x3F);
        $version = $file->read(1);
        $version = bindec($version);

        // Version: (00 = 1.0, 15 = 2.5, etc.)
        $mergedVersion = (10 + $version) * .1;
        return number_format($mergedVersion, 1, '.', '');
    }

    public function translateCountryCode(string $code): string
    {
        return self::COUNTRY_CODES[$code] ?? 'Unknown';
    }
}
