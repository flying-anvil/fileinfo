<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64;

use FlyingAnvil\Fileinfo\InfoExtractor\CacheInterface;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class M64SequenceInfoExtractor implements InfoExtractor, CacheInterface
{
    // m64 files *should* start with 0xD3, but are also valid if not :/
    public const MAGIC = 0xD3;

    private const INDICATOR_NINST         = 0xD3;
    private const INDICATOR_MASTER_VOLUME = 0xDB;
    private const INDICATOR_TEMPO         = 0xDD;
    private const INDICATOR_LOOP_OFFSET   = 0xFB;

    private const CACHE_KEY_SEQUENCE_TYPE            = 'sequenceType';
    private const CACHE_KEY_NINST                    = 'NInst';
    private const CACHE_KEY_MASTER_VOLUME_ABSOLUTE   = InfoCommonKeys::MASTER_VOLUME . 'Absolute';
    private const CACHE_KEY_MASTER_VOLUME_NORMALIZED = InfoCommonKeys::MASTER_VOLUME . 'Normalized';
    private const CACHE_KEY_TEMPO                    = InfoCommonKeys::TEMPO;
    private const CACHE_KEY_LOOP                     = InfoCommonKeys::LOOP;
    private const CACHE_KEY_LOOP_OFFSET              = InfoCommonKeys::LOOP_OFFSET;
    private const CACHE_KEY_CHANNEL_COUNT            = InfoCommonKeys::CHANNEL_COUNT;
    private const CACHE_KEY_CHANNEL_OFFSETS          = 'channelOffsets';

    private UniversalCollection $cache;

    public function __construct()
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function getInfo(string $filePath, bool $strictVerification = true): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file, $strictVerification)) {
            throw new NotResponsibleExtractorException('Header Mismatch');
        }

        $this->writeCache($file);

        return $this->cache->get($filePath);
    }

    public function getInfoSummary(string $filePath, bool $strictVerification = true): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file, $strictVerification)) {
            throw new NotResponsibleExtractorException('Header Mismatch');
        }

        $this->writeCache($file);

        $result = UniversalCollection::createEmpty();

        $result->add(self::CACHE_KEY_NINST, $this->readNInst($file));
        $result->add(self::CACHE_KEY_MASTER_VOLUME_NORMALIZED, $this->readMasterVolumeNormalized($file));
        $result->add(self::CACHE_KEY_TEMPO, $this->readTempo($file));
        $result->add(self::CACHE_KEY_LOOP, $this->readLoop($file));
        $result->add(self::CACHE_KEY_CHANNEL_COUNT, $this->readChannelCount($file));

        return $result;
    }

    /*
     * I'm not sure how to verify it, as files *should*, but don't have to start with 0xD3
     */
    public function verify(File $file, bool $strict = false): bool
    {
        $file->rewind();

        return $file->readUnsignedByte() === self::MAGIC || !$strict;
    }

    public function readSequenceType(File $file): int
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_SEQUENCE_TYPE);
    }

    public function readNInst(File $file): ?int
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_NINST);
    }

    /**
     * @return int Volume in range [0,255]
     */
    public function readMasterVolumeAbsolute(File $file): int
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_MASTER_VOLUME_ABSOLUTE);
    }

    /**
     * @return int Volume in range [0,1]
     */
    public function readMasterVolumeNormalized(File $file): float
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_MASTER_VOLUME_NORMALIZED);
    }

    /**
     * @return int Tempo in BPM
     */
    public function readTempo(File $file): int
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_TEMPO);
    }

    public function readLoop(File $file): bool
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_LOOP);
    }

    public function readLoopOffset(File $file): int
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_LOOP_OFFSET);
    }

    public function readChannelCount(File $file): int
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_CHANNEL_COUNT);
    }

    /**
     * @return int[] Offsets for each channel
     */
    public function readChannelOffsets(File $file): array
    {
        $this->writeCache($file);

        /** @var UniversalCollection $counts */
        $data = $this->cache->get($file->getFilePath());
        return $data->get(self::CACHE_KEY_CHANNEL_OFFSETS);
    }

    private function writeCache(File $file): void
    {
        $filePath = $file->getFilePath();
        if ($this->cache->has($filePath)) {
            return;
        }

        $channelOffsets = UniversalCollection::createEmpty();
        $cacheData      = UniversalCollection::createFrom([
            self::CACHE_KEY_SEQUENCE_TYPE            => null,
            self::CACHE_KEY_NINST                    => null,
            self::CACHE_KEY_MASTER_VOLUME_ABSOLUTE   => 0,
            self::CACHE_KEY_MASTER_VOLUME_NORMALIZED => 0,
            self::CACHE_KEY_TEMPO                    => 0,
            self::CACHE_KEY_LOOP                     => false,
            self::CACHE_KEY_LOOP_OFFSET              => null,
            self::CACHE_KEY_CHANNEL_COUNT            => 0,
            self::CACHE_KEY_CHANNEL_OFFSETS          => [],
        ]);

        $file->rewind();

        $channelCount = 0;
        $byte         = 0x00;
        while ($byte !== 0xFF && !$file->isEof()) {
            if ($file->tell() > 0xFF) {
                throw new NotResponsibleExtractorException('Suspicious header length');
            }

            $byte = $file->readUnsignedByte();

            $isTrackData = ($byte >> 4) === 9;
            if ($isTrackData) {
                $channelCount++;

                $channel = $byte & 0x0F;
                $channelOffset = $file->readUnsignedShortBigEndian();

                $channelOffsets->add((string)$channel, $channelOffset);

                continue;
            }

            switch ($byte) {
                case self::INDICATOR_NINST:
                    $value = $file->readUnsignedByte();
                    $cacheData->add(self::CACHE_KEY_SEQUENCE_TYPE, $value);
                    $cacheData->add(self::CACHE_KEY_NINST, $value > 37 ? null : $value);
                    break;
                case self::INDICATOR_MASTER_VOLUME:
                    $masterVolume = $file->readUnsignedByte();

                    $cacheData->add(self::CACHE_KEY_MASTER_VOLUME_ABSOLUTE, $masterVolume);
                    $cacheData->add(self::CACHE_KEY_MASTER_VOLUME_NORMALIZED, $masterVolume / 0xFF);
                    break;
                case self::INDICATOR_TEMPO:
                    $cacheData->add(self::CACHE_KEY_TEMPO, $file->readUnsignedByte());
                    break;
                case self::INDICATOR_LOOP_OFFSET:
                    $cacheData->add(InfoCommonKeys::LOOP, true);
                    $cacheData->add(self::CACHE_KEY_LOOP_OFFSET, $file->readUnsignedByte());
                    break;
            }
        }

        $cacheData->add(self::CACHE_KEY_CHANNEL_COUNT, $channelCount);
        $cacheData->add(self::CACHE_KEY_CHANNEL_OFFSETS, $channelOffsets);

        $this->cache->add($filePath, $cacheData);
    }

    public function clearCache(): void
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function clearSpecificCache(string $filePath): void
    {
        $this->cache->remove($filePath);
    }
}
