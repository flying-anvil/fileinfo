<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64;

use DateTime;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class M64ReplayCaptureInfoExtractor implements InfoExtractor
{
    public const M64_MAGIC = "M64\x1A";

    private const OFFSET_VERSION           = 0x0004;
    private const OFFSET_TIMESTAMP         = 0x0008;
    private const OFFSET_FRAME_COUNT       = 0x000C;
    private const OFFSET_RE_RECORD_COUNT   = 0x0010;
    private const OFFSET_FRAMES_PER_SECOND = 0x0014;
    private const OFFSET_CONTROLLER_COUNT  = 0x0015;
    private const OFFSET_INPUT_SAMPLES_PER_CONTROLLER = 0x0018;
    private const OFFSET_MOVIE_START_TYPE  = 0x001C;
    private const OFFSET_CONTROLLER_FLAGS  = 0x0020;
    private const OFFSET_ROM_INTERNAL_NAME = 0x00C4;
    private const OFFSET_ROM_CRC32         = 0x00E4;
    private const OFFSET_ROM_COUNTRY_CODE  = 0x00E8;
    private const OFFSET_PLUGIN_VIDEO_NAME = 0x0122;
    private const OFFSET_PLUGIN_SOUND_NAME = 0x0162;
    private const OFFSET_PLUGIN_INPUT_NAME = 0x01A2;
    private const OFFSET_PLUGIN_RSP_NAME   = 0x01E2;
    private const OFFSET_AUTHOR_NAME       = 0x0222;
    private const OFFSET_DESCRIPTION       = 0x0300;

    private const MAPPING_MOVIE_START_TYPE = [
        0x01 => 'snapshot',
        0x02 => 'power-on',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::headerMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($file));

        $timestamp = $this->readTimestamp($file);
        $result->add(InfoCommonKeys::TIMESTAMP, $timestamp);
        $result->add(InfoCommonKeys::DATETIME, (new DateTime('@' . $timestamp))->format('Y-m-d H:i:s'));

        $viCount = $this->readVICount($file);
        $viRate  = $this->readVIPerSecond($file);
        $result->add(InfoCommonKeys::VI_COUNT, $viCount);
        $result->add(InfoCommonKeys::RE_RECORD_COUNT, $this->readReRecordCount($file));
        $result->add(InfoCommonKeys::VI_PER_SECOND, $viRate);
        $result->add(InfoCommonKeys::DURATION, $this->calculateDuration($viCount, $viRate));

        $result->add('controllerCount', $this->readControllerCount($file));
        $result->add('samplesPerController', $this->readInputSamplesPerController($file));
        $result->add('movieStartType', $this->readMovieStartType($file));
        $result->add(InfoCommonKeys::GAME_TITLE, $this->readRomInternalName($file));
        $result->add('CRC32', $this->readRomCrc32($file));

        $countryCode = $this->readRomCountryCode($file);
        $result->add(InfoCommonKeys::COUNTRY_CODE, $countryCode);
        $result->add(InfoCommonKeys::COUNTRY_CODE_TRANSLATION, (new Z64InfoExtractor())->translateCountryCode($countryCode));
        $result->add('pluginVideoName', $this->readPluginVideoName($file));
        $result->add('pluginSoundName', $this->readPluginSoundName($file));
        $result->add('pluginInputName', $this->readPluginInputName($file));
        $result->add('pluginRspName', $this->readPluginRspName($file));
        $result->add(InfoCommonKeys::AUTHOR, $this->readAuthorName($file));
        $result->add(InfoCommonKeys::DESCRIPTION, $this->readDescription($file));

        $result->add('controllerFlags', $this->readControllerFlags($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::headerMismatch();
        }

        $viCount = $this->readVICount($file);
        $viRate  = $this->readVIPerSecond($file);

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::DATETIME, (new DateTime('@' . $this->readTimestamp($file)))->format('Y-m-d H:i:s'));
        $result->add(InfoCommonKeys::VI_COUNT, $viCount);
        $result->add(InfoCommonKeys::DURATION, $this->calculateDuration($viCount, $viRate));
        $result->add(InfoCommonKeys::RE_RECORD_COUNT, $this->readReRecordCount($file));
        $result->add('controllerCount', $this->readControllerCount($file));
        $result->add(InfoCommonKeys::GAME_TITLE, $this->readRomInternalName($file));

        $countryCode = $this->readRomCountryCode($file);
        $result->add(InfoCommonKeys::COUNTRY_CODE, $countryCode);
        $result->add(InfoCommonKeys::COUNTRY_CODE_TRANSLATION, (new Z64InfoExtractor())->translateCountryCode($countryCode));
        $result->add(InfoCommonKeys::AUTHOR, $this->readAuthorName($file));
        $result->add(InfoCommonKeys::DESCRIPTION, $this->readDescription($file));

        return $result;
    }

    public function verify(File $file): bool
    {
        $file->rewind();
        return $file->read(4) === self::M64_MAGIC;
    }

    public function readVersion(File $file): int
    {
        $file->seek(self::OFFSET_VERSION);
        return $file->readUInt32LittleEndian();
    }

    public function readTimestamp(File $file): int
    {
        $file->seek(self::OFFSET_TIMESTAMP);
        return $file->readUInt32LittleEndian();
    }

    public function readVICount(File $file): int
    {
        $file->seek(self::OFFSET_FRAME_COUNT);
        return $file->readUInt32LittleEndian();
    }

    public function readReRecordCount(File $file): int
    {
        $file->seek(self::OFFSET_RE_RECORD_COUNT);
        return $file->readUInt32LittleEndian();
    }

    public function readVIPerSecond(File $file): int
    {
        $file->seek(self::OFFSET_FRAMES_PER_SECOND);
        return $file->readUnsignedByte();
    }

    public function readControllerCount(File $file): int
    {
        $file->seek(self::OFFSET_CONTROLLER_COUNT);
        return $file->readUnsignedByte();
    }

    public function readInputSamplesPerController(File $file): int
    {
        $file->seek(self::OFFSET_INPUT_SAMPLES_PER_CONTROLLER);
        return $file->readUInt32LittleEndian();
    }

    public function readMovieStartType(File $file): string
    {
        $file->seek(self::OFFSET_MOVIE_START_TYPE);
        $type = $file->readUInt16LittleEndian();
        return self::MAPPING_MOVIE_START_TYPE[$type] ?? 'invalid';
    }

    public function readControllerFlags(File $file): UniversalCollection
    {
        $file->seek(self::OFFSET_CONTROLLER_FLAGS);

        $rawFlags      = $file->readUInt32LittleEndian();
        $combinedFlags = UniversalCollection::createEmpty();
//        echo 'Flags:     ' . str_pad((string)decbin($rawFlags), 32,  '0', STR_PAD_LEFT) . PHP_EOL;

        for ($i = 0; $i < 4; $i++) {
            $bitMaskIsPresent = 1 << (0 + $i); // $rawBitMaskIsPresent < $i;
            $bitMaskHasMempak = 1 << (4 + $i); // $rawBitMaskHasMempak < $i;
            $bitMaskHasRumple = 1 << (8 + $i); // $rawBitMaskHasRumple < $i;

//            echo 'Present ' . ($i + 1) . ': ' . str_pad((string)decbin($bitMaskIsPresent), 32, '0', STR_PAD_LEFT) . PHP_EOL;
//            echo 'Mempak  ' . ($i + 1) . ': ' . str_pad((string)decbin($bitMaskHasMempak), 32, '0', STR_PAD_LEFT) . PHP_EOL;
//            echo 'Rumple  ' . ($i + 1) . ': ' . str_pad((string)decbin($bitMaskHasRumple), 32, '0', STR_PAD_LEFT) . PHP_EOL;

            $flags = UniversalCollection::createEmpty();
            $flags->add('isPresent',    (bool)($rawFlags & $bitMaskIsPresent));
            $flags->add('hasMempak',    (bool)($rawFlags & $bitMaskHasMempak));
            $flags->add('hasRumblepak', (bool)($rawFlags & $bitMaskHasRumple));

            $combinedFlags->add('controller' . ($i + 1), $flags);
        }

        return $combinedFlags;
    }

    public function readRomInternalName(File $file): string
    {
        $file->seek(self::OFFSET_ROM_INTERNAL_NAME);
        return rtrim($file->read(32));
    }

    public function readRomCrc32(File $file): int
    {
        $file->seek(self::OFFSET_ROM_CRC32);
        return $file->readUInt32LittleEndian();
    }

    public function readRomCountryCode(File $file): string
    {
        $file->seek(self::OFFSET_ROM_COUNTRY_CODE);
        return $file->readChar();
    }

    public function readPluginVideoName(File $file): string
    {
        $file->seek(self::OFFSET_PLUGIN_VIDEO_NAME);
        return rtrim($file->read(64));
    }

    public function readPluginSoundName(File $file): string
    {
        $file->seek(self::OFFSET_PLUGIN_SOUND_NAME);
        return rtrim($file->read(64));
    }

    public function readPluginInputName(File $file): string
    {
        $file->seek(self::OFFSET_PLUGIN_INPUT_NAME);
        return rtrim($file->read(64));
    }

    public function readPluginRspName(File $file): string
    {
        $file->seek(self::OFFSET_PLUGIN_RSP_NAME);
        return rtrim($file->read(64));
    }

    public function readAuthorName(File $file): string
    {
        $file->seek(self::OFFSET_AUTHOR_NAME);
        return rtrim($file->read(222));
    }

    public function readDescription(File $file): string
    {
        $file->seek(self::OFFSET_DESCRIPTION);
        return rtrim($file->read(256));
    }

    public function calculateDuration(int $viCount, int $viRate): float
    {
        return $viCount / $viRate;
    }
}
