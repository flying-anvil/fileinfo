<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Snes;

use DateTime;
use Exception;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * http://vspcplay.raphnet.net/spc_file_format.txt
 */
class SpcInfoExtractor implements InfoExtractor
{
    private const OFFSET_ID666_MARKER             = 0x23;
    private const OFFSET_MINOR_VERSION            = 0x24;
    private const OFFSET_SONG_TITLE               = 0x2E;
    private const OFFSET_GAME_TITLE               = 0x4E;
    private const OFFSET_AUTHOR_NAME              = 0x6E;
    private const OFFSET_COMMENT                  = 0x7E;
    private const OFFSET_CREATION_DATE            = 0x9E;
    private const OFFSET_DURATION                 = 0xA9;
    private const OFFSET_FADE_OUT_DURATION        = 0xAC;
    private const OFFSET_ORIGINAL_COMPOSER        = 0xB1;
    private const OFFSET_DEFAULT_CHANNEL_DISABLED = 0xD1;
    private const OFFSET_DUMP_TOOL                = 0xD2;

    private const DUMP_TOOLS = [
        0 => 'unknown',
        1 => 'ZSNES',
        2 => 'Snes9x',
    ];

    public const SPC_MAGIC = 'SNES-SPC700 Sound File Data v0.30';

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::SONG_TITLE, $this->readSongTitle($file));
        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file));
        $result->add('authorName', $this->readAuthorName($file));
        $result->add('originalComposer', $this->readOriginalComposer($file));
        $result->add(InfoCommonKeys::COMMENT, $this->readComment($file));
        $result->add('creationDate', $this->readCreationDate($file));

        $result->add('minorVersion', $this->readMinorVersion($file));

        $result->add(InfoCommonKeys::DURATION, $this->readDuration($file));
        $result->add('fadeOutDuration', $this->readFadeOutDuration($file));

        $result->add('containsID666', $this->readContainsId666($file));
        $result->add('defaultChannelDisabled', $this->readDisableDefaultChannel($file));
        $result->add('dumpTool', $this->readDumpTool($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::SONG_TITLE, $this->readSongTitle($file));
        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file));
        $result->add('authorName', $this->readAuthorName($file));
        $result->add('originalComposer', $this->readOriginalComposer($file));
        $result->add(InfoCommonKeys::DURATION, $this->readDuration($file));
        $result->add(InfoCommonKeys::COMMENT, $this->readComment($file));
        $result->add('creationDate', $this->readCreationDate($file));

        return $result;
    }

    public function verify(File $file): bool
    {
        return $this->readMagic($file) === self::SPC_MAGIC;
    }

    public function readMagic(File $file): string
    {
        $file->rewind();
        return $file->read(33);
    }

    public function readContainsId666(File $file): bool
    {
        $file->seek(self::OFFSET_ID666_MARKER);
        return $file->readUnsignedByte() === 0x1A;
    }

    public function readMinorVersion(File $file): int
    {
        $file->seek(self::OFFSET_MINOR_VERSION);
        return $file->readUnsignedByte();
    }

    public function readSongTitle(File $file): string
    {
        $file->seek(self::OFFSET_SONG_TITLE);
        return mb_convert_encoding(trim($file->read(32)), 'UTF-8', 'ascii');
    }

    public function readGameTitle(File $file): string
    {
        $file->seek(self::OFFSET_GAME_TITLE);
        return mb_convert_encoding(rtrim($file->read(32)), 'UTF-8', 'ascii');
    }

    public function readAuthorName(File $file): string
    {
        $file->seek(self::OFFSET_AUTHOR_NAME);
        return mb_convert_encoding(rtrim($file->read(16)), 'UTF-8', 'ascii');
    }

    public function readComment(File $file): string
    {
        $file->seek(self::OFFSET_COMMENT);
        return rtrim($file->read(32));
    }

    public function readCreationDate(File $file): string
    {
        $file->seek(self::OFFSET_CREATION_DATE);
        $rawDate = rtrim($file->read(11));

        // TODO: Check if date is binary

        if ($rawDate === '') {
            return '';
        }

        try {
            $date = new DateTime($rawDate);
            return $date->format('Y-m-d');
        } catch (Exception) {
            return '';
        }
    }

    /**
     * @param File $file
     * @return int Duration in seconds
     */
    public function readDuration(File $file): int
    {
        $file->seek(self::OFFSET_DURATION);
        $rawDuration = $file->read(3);
        $intDuration = (int)$rawDuration;

        // In case duration is binary (non standard)
        if ($intDuration === 0) {
            return unpack('v', $rawDuration)[1];
        }

        return $intDuration;
    }

    /**
     * @param File $file
     * @return int Fade out duration in milliseconds
     */
    public function readFadeOutDuration(File $file): int
    {
        $file->seek(self::OFFSET_FADE_OUT_DURATION);
        return (int)$file->read(5);
    }

    public function readOriginalComposer(File $file): string
    {
        $file->seek(self::OFFSET_ORIGINAL_COMPOSER);
        $originalComposer = rtrim($file->read(32));

        // TODO: Is this needed?
        $length = strlen($originalComposer);
        if ($length > 0 && ord($originalComposer[$length - 1]) === 0xFF) {
            return '';
        }

        return $originalComposer;
    }

    public function readDisableDefaultChannel(File $file): bool
    {
        $file->seek(self::OFFSET_DEFAULT_CHANNEL_DISABLED);
        return $file->readUnsignedByte() === 1;
    }

    public function readDumpTool(File $file): string
    {
        $file->seek(self::OFFSET_DUMP_TOOL);
        $toolUsed = $file->readUnsignedByte();

        return self::DUMP_TOOLS[$toolUsed] ?? 'other';
    }
}
