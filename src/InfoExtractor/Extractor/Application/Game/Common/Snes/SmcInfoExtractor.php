<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Snes;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * https://en.wikibooks.org/wiki/Super_NES_Programming/SNES_memory_map#The_SNES_header
 * TODO: Cache isHiRom and hasSmcHeader for file
 */
class SmcInfoExtractor implements InfoExtractor
{
    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file = File::load($filePath);
        $file->open();

        $smcHeaderCheck = filesize($file->getFilePath()) % 1024;

        // Has header, but is malformed
        if ($smcHeaderCheck !== 0 && $smcHeaderCheck !== 512) {
            return $result;
        }

        $hasSmcHeader = $this->hasSmcHeader($file);

        $result->add('hasSmcHeader', $hasSmcHeader);

        $isHiRom = $this->isHiRom($file, $hasSmcHeader);
        $result->add('isHiRom', $isHiRom);
        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file, $hasSmcHeader, $isHiRom));
        $result->add('romType', $this->readRomType($file, $hasSmcHeader, $isHiRom));
        $result->add(InfoCommonKeys::ROM_SIZE, $this->readRomSize($file, $hasSmcHeader, $isHiRom));
        $result->add('sramSize', $this->readSramSize($file, $hasSmcHeader, $isHiRom));
        $result->add('licenseId', $this->readLicenseId($file, $hasSmcHeader, $isHiRom));
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($file, $hasSmcHeader, $isHiRom));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file = File::load($filePath);
        $file->open();

        $smcHeaderCheck = filesize($file->getFilePath()) % 1024;

        // Has header, but is malformed
        if ($smcHeaderCheck !== 0 && $smcHeaderCheck !== 512) {
            return $result;
        }

        $hasSmcHeader = $this->hasSmcHeader($file);

        $isHiRom = $this->isHiRom($file, $hasSmcHeader);
        $result->add(InfoCommonKeys::GAME_TITLE, $this->readGameTitle($file, $hasSmcHeader, $isHiRom));
        $result->add(InfoCommonKeys::ROM_SIZE, $this->readRomSize($file, $hasSmcHeader, $isHiRom));
        $result->add(InfoCommonKeys::VERSION, $this->readVersion($file, $hasSmcHeader, $isHiRom));

        return $result;
    }

    public function hasSmcHeader(File $file): bool
    {
        return filesize($file->getFilePath()) % 1024 === 512;
    }

    /**
     * Usually the SMC header contains the size of the ROM byte 0 and 1,
     * and the type of cartridge in byte 2. The other bytes (509) left are set to zero.
     */
    public function readContainsSmcHeader(File $file): bool
    {
        $file->seek(0x3);
        $padding = $file->read(509);

        return $padding === str_repeat("\0", 509);
    }

    /**
     * Looks if the name is correct
     * Every ASCII character between (not including) 0x1F and 0x7F is valid.
     */
    public function isHiRom(File $file, bool $smcHeader): bool
    {
        $this->seek($file, 0xFFC0, $smcHeader);
//        echo 'Reading name from: ' . dechex($file->tell()), PHP_EOL;
        $name = $file->read(21);

        return (bool)preg_match('/^[\x20-\x7e]+$/', $name);
    }

    public function readGameTitle(File $file, bool $smcHeader, bool $isHiRom): string
    {
        $offset = $this->buildHiLoRomOffset(0xFFC0, $isHiRom);
        $this->seek($file, $offset, $smcHeader);

        return rtrim($file->read(21));
    }

    public function readRomType(File $file, bool $smcHeader, bool $isHiRom): string
    {
        $offset = $this->buildHiLoRomOffset(0xFFD6, $isHiRom);
        $this->seek($file, $offset, $smcHeader);

        $type = $file->read(1);

        return (string)hexdec(bin2hex($type));
    }

    public function readRomSize(File $file, bool $smcHeader, bool $isHiRom): int
    {
        $offset = $this->buildHiLoRomOffset(0xFFD7, $isHiRom);
        $this->seek($file, $offset, $smcHeader);

        $sizeMagnitude = $file->read(1);

        return (0x400 << ord($sizeMagnitude));
    }

    public function readSramSize(File $file, bool $smcHeader, bool $isHiRom): int
    {
        $offset = $this->buildHiLoRomOffset(0xFFD8, $isHiRom);
        $this->seek($file, $offset, $smcHeader);

        $sizeMagnitude = $file->read(1);

        return (0x400 << ord($sizeMagnitude));
    }

    public function readLicenseId(File $file, bool $smcHeader, bool $isHiRom): int
    {
        $offset = $this->buildHiLoRomOffset(0xFFD9, $isHiRom);
        $this->seek($file, $offset, $smcHeader);

        $binaryId = $file->read(2);

        return hexdec(bin2hex($binaryId));
    }

    public function readVersion(File $file, bool $smcHeader, bool $isHiRom): int
    {
        $offset = $this->buildHiLoRomOffset(0xFFDB, $isHiRom);
        $this->seek($file, $offset, $smcHeader);

        $version = $file->read(1);

        return 1 + ord($version);
    }

    // TODO: Checksum complement (0xFFDC)
    // TODO: Checksum (0xFFDE)

    private function seek(File $file, int $offset, bool $hasSmcHeader): void
    {
        $realOffset = $hasSmcHeader
            ? $offset + 512
            : $offset;

        $file->seek($realOffset);
    }

    private function buildHiLoRomOffset(int $offset, bool $isHiRom): int
    {
        return $isHiRom
            ? $offset
            : $offset - 0x8000;
    }
}
