<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Nes;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * @see https://wiki.nesdev.com/w/index.php/NES_2.0#Header
 */
class NesInfoExtractor implements InfoExtractor
{
    public const NES_MAGIC = "NES\x1A";

    private const OFFSET_PRG_ROM_SIZE = 0x04;
    private const OFFSET_CHR_ROM_SIZE = 0x04;
    private const OFFSET_FLAGS_6 = 0x06;
    private const OFFSET_FLAGS_7 = 0x07;

    public function getInfo(string $filePath): UniversalCollection
    {
        $file   = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add('prgRomSize', $this->readPrgRomSize($file));
        $result->add('chrRomSize', $this->readChrRomSize($file));
        $result->add(InfoCommonKeys::FLAGS, $this->readFlags($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    public function verify(File $file): bool
    {
        return $this->readHeader($file) === self::NES_MAGIC;
    }

    public function readHeader(File $file): string
    {
        $file->rewind();
        return $file->read(4);
    }

    public function readPrgRomSize(File $file): int
    {
        $file->seek(self::OFFSET_PRG_ROM_SIZE);
        return ord($file->readChar());
    }

    public function readChrRomSize(File $file): int
    {
        $file->seek(self::OFFSET_CHR_ROM_SIZE);
        return ord($file->readChar());
    }

    public function readFlags(File $file): UniversalCollection
    {
        $flags = UniversalCollection::createEmpty();

        // ===== Flags 6 =========================
        $file->seek(self::OFFSET_FLAGS_6);
        $rawFlags6 = ord($file->readChar());

        $hardWiredNametableMirroringType = ($rawFlags6 & 0b00000001) === 0
            ? 'horizontal'
            : 'vertical';

        $hasBattery = ($rawFlags6 & 0b00000010) === 1;
        $hasTrainer = ($rawFlags6 & 0b00000100) === 1;
        $hardWiredFourScreenMode = ($rawFlags6 & 0b000010000) === 1;

        $flags->add('hardWiredNametableMirroringType', $hardWiredNametableMirroringType);
        $flags->add('hasBattery', $hasBattery);
        $flags->add('hasTrainer', $hasTrainer);
        $flags->add('hardWiredFourScreenMode', $hardWiredFourScreenMode);

        // ===== Flags 7 =========================
        $file->seek(self::OFFSET_FLAGS_7);
        $rawFlags7 = ord($file->readChar());

        $consoleType = match ($rawFlags7 & 0b00000011) {
            0 => 'Nintendo Entertainment System',
            1 => 'Nintendo Vs. System',
            2 => 'Nintendo Playchoice 10',
            3 => 'Extended Console Type',

            default => 'Unknown',
        };

        $flags->add('consoleType', $consoleType);

        return $flags;
    }
}
