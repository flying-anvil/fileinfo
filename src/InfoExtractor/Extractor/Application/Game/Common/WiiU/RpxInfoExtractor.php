<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\WiiU;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class RpxInfoExtractor implements InfoExtractor
{
    // https://tcrf.net/User:Ehm/Project_Names
    public const RPX_NAME_TITLE_MAPPING = [
        'ProjectZ'        => 'Hyrule Warriors',
        'Gambit'          => 'Splatoon',
        'cross_f'         => 'Super Smash Bros. for Wii U',
        'bbq'             => 'Animal Crossing: amiibo Festival',
        'RedPro'          => 'New Super Mario Bros. U',
        'RDash'           => 'New Super Luigi U',
        'RedCarpet'       => 'Super Mario 3D World',
        'Kinopio'         => 'Captain Toad: Treasure Tracker',
        'tenst'           => 'Mario Party 10',
        'Turbo'           => 'Mario Kart 8',
        'Block'           => 'Super Mario Maker',
        'wiiUTennis'      => 'Mario Tennis: Ultra Smash',
        'gojika'          => 'Paper Mario: Color Splash',
        'cking'           => 'The Wind Waker HD',
        'surf'            => 'Twilight Princess HD',
        'Zelda'           => 'Twilight Princess HD',
        'U-King'          => 'Breath of the Wild',
        'ferrum_app'      => 'Pokkén Tournament',
        'uberMudds'       => 'Mutant Mudds Deluxe',
        'OgreBoot'        => 'Ryū Ga Gotoku 1 & 2 HD for Wii U',
        'Stainless'       => 'Tokyo Mirage Sessions ♯FE',
        'BayonetWU'       => 'Bayonetta',
        'PRJ_010'         => 'Bayonetta 2',
        'PRJ_030'         => 'Star Fox Zero',
        'PRJ_031'         => 'Star Fox Guard',
        'rs10_production' => 'Donkey Kong Country: Tropical Freeze',
        'rs10'            => 'Donkey Kong Country: Tropical Freeze',
//        'INTERNAL_NAME'   => 'PUBLIC_NAME',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file   = File::load($filePath);
        $file->open();

        // TODO: verify ELF header

        $devInfo = $this->getDevInfo($file);

        $buildFilePath = str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $devInfo->get('buildPath', 'unknown'));
        $projectName   = pathinfo($buildFilePath, PATHINFO_FILENAME);

        $devInfo->add('projectName', $projectName);
        $result->add(InfoCommonKeys::GAME_TITLE, self::RPX_NAME_TITLE_MAPPING[$projectName] ?? 'unknown');
        $result->add('developmentInfos', $devInfo);

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $info = $this->getInfo($filePath);
        $info->remove('developmentInfos');

        return $info;
    }

    private function getDevInfo(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $interestingLine = '';
        $lineNumber      = 0;

        while (($line = $file->readLine()) !== false) {
            if (str_contains($line, '.rpx')) {
                $interestingLine = $line;
            }

            if (++$lineNumber === 20) {
                break;
            }
        }

        if ($interestingLine === '') {
            return $result;
        }

        // ^@^@^@^@º^@^@Pxÿÿÿÿ^@^@^@^@^@^@^@<94>^@^@Qº^@^@ÌÑ^@^@^@^@^@^@^@^@D:\home\Jenkins\workspace\RedCarpet-master\Game\.result\bin\CAFE\Eu\Product\RedCarpet.rpx^@BUILD_TYPE^@NDEBUG^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^@^
        preg_match('/.*([A-Z]:.+)\0BUILD_TYPE\0([a-zA-Z]+)/', $interestingLine, $matches);

        [
            1 => $buildPath,
            2 => $buildType,
        ] = $matches;

        $result->add('buildPath', $buildPath);
        $result->add('buildType', $buildType);

        return $result;
    }

//    private function processFileInfo(string $filePath, UniversalCollection $result)
//    {
//        $fileVersionOutput = shell_exec('file --version');
//        $fileVersion       = substr($fileVersionOutput, 0, strpos($fileVersionOutput, "\n"));
//
//        switch ($fileVersion) {
//            case 'file-5.25':
//            case 'file-5.30':
//                // Turbo.rpx: ELF 32-bit MSB PowerPC or cisco 4500, version 1
//                break;
//            case 'file-5.35':
//                // Turbo.rpx: ELF 32-bit MSB executable, PowerPC or cisco 4500, version 1 (Cafe OS)
//
//        }
//    }
}
