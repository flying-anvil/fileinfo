<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Gb;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use FlyingAnvil\Libfa\Wrapper\File;

class GbInfoExtractor implements InfoExtractor
{
    private const OFFSET_LOGO             = 0x0104;
    private const OFFSET_TITLE            = 0x0134;
    private const OFFSET_LICENSEE_CODE    = 0x0144;
    private const OFFSET_SGB_FLAG         = 0x0146;
    private const OFFSET_CARTRIDGE_TYPE   = 0x0147;
    private const OFFSET_ROM_SIZE         = 0x0148;
    private const OFFSET_RAM_SIZE         = 0x0149;
    private const OFFSET_DESTINATION_CODE = 0x014A;
    private const OFFSET_ROM_VERSION      = 0x014C;
    private const OFFSET_HEADER_CHECKSUM  = 0x014D;
    private const OFFSET_CHECKSUM         = 0x014E;
    private const LOGO_MD5                = '8661ce8a0ebede95e8a131a0aa1717f6';

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file   = File::load($filePath);
        $file->open();

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readTitle($file));
        $result->add(InfoCommonKeys::LICENSEE, $this->translateLicenseeCode($this->readLicenseeCode($file)));
        $result->add('sgbSupport', $this->readSgbSupport($file));
        $result->add(InfoCommonKeys::CARTRIDGE_TYPE, $this->readCartridgeType($file));
        $result->add(InfoCommonKeys::ROM_SIZE, $this->readRomSize($file));
        $result->add('romBankCount', $this->readRomBankCount($file));
        $result->add(InfoCommonKeys::RAM_SIZE, $this->readRamSize($file));
        $result->add(InfoCommonKeys::DESTINATION, $this->readDestination($file));
        $result->add(InfoCommonKeys::VERSION, $this->readRomVersion($file));
        $result->add(InfoCommonKeys::HEADER_CHECKSUM, $this->readHeaderChecksum($file));
        $result->add(InfoCommonKeys::CHECKSUM, $this->readChecksum($file));
        $result->add('validHeader', $this->verifyHeader($file));
        $result->add('validLogo', $this->verifyLogo($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file   = File::load($filePath);
        $file->open();

        $result->add(InfoCommonKeys::GAME_TITLE, $this->readTitle($file));
        $result->add(InfoCommonKeys::LICENSEE, $this->translateLicenseeCode($this->readLicenseeCode($file)));
        $result->add(InfoCommonKeys::ROM_SIZE, $this->readRomSize($file));
        $result->add(InfoCommonKeys::VERSION, $this->readRomVersion($file));

        return $result;
    }

    public function readTitle(File $file): string
    {
        $file->seek(self::OFFSET_TITLE);
        return rtrim($file->readNullTerminatedSequence(11));
    }

    public function readLicenseeCode(File $file): string
    {
        $file->seek(self::OFFSET_LICENSEE_CODE);
        return $file->read(2);
    }

    public function translateLicenseeCode(string $code): string
    {
        if (strlen($code) !== 2) {
            throw new ValueException('Licensee code is not valid, must be 2 chars');
        }

        return match ($code) {
            '00' => 'none',
            '01' => 'Nintendo R&D1',
            '08' => 'Capcom',
            '13' => 'Electronic Arts',
            '18' => 'Hudson Soft',
            '19' => 'b-ai',
            '20' => 'kss',
            '22' => 'pow',
            '24' => 'PCM Complete',
            '25' => 'san-x',
            '28' => 'Kemco Japan',
            '29' => 'seta',
            '30' => 'Viacom',
            '31' => 'Nintendo',
            '32' => 'Bandai',
            '33' => 'Ocean/Acclaim',
            '34' => 'Konami',
            '35' => 'Hector',
            '37' => 'Taito',
            '38' => 'Hudson',
            '39' => 'Banpresto',
            '41' => 'Ubi Soft',
            '42' => 'Atlus',
            '44' => 'Malibu',
            '46' => 'angel',
            '47' => 'Bullet-Proof',
            '49' => 'irem',
            '50' => 'Absolute',
            '51' => 'Acclaim',
            '52' => 'Activision',
            '53' => 'American sammy',
            '54' => 'Konami',
            '55' => 'Hi tech entertainment',
            '56' => 'LJN',
            '57' => 'Matchbox',
            '58' => 'Mattel',
            '59' => 'Milton Bradley',
            '60' => 'Titus',
            '61' => 'Virgin',
            '64' => 'LucasArts',
            '67' => 'Ocean',
            '69' => 'Electronic Arts',
            '70' => 'Infogrames',
            '71' => 'Interplay',
            '72' => 'Broderbund',
            '73' => 'sculptured',
            '75' => 'sci',
            '78' => 'THQ',
            '79' => 'Accolade',
            '80' => 'misawa',
            '83' => 'lozc',
            '86' => 'tokuma shoten i*',
            '87' => 'tsukuda ori*',
            '91' => 'Chunsoft',
            '92' => 'Video system',
            '93' => 'Ocean/Acclaim',
            '95' => 'Varie',
            '96' => 'Yonezawa/s\'pal',
            '97' => 'Kaneko',
            '99' => 'Pack in soft',
            'A4' => 'Konami (Yu-Gi-Oh!)',

            default => 'none',
        };
    }

    public function readSgbSupport(File $file): bool
    {
        $file->seek(self::OFFSET_SGB_FLAG);
        $byte = ord($file->readChar());

        return $byte === 0x03;
    }

    public function readCartridgeType(File $file): string
    {
        $file->seek(self::OFFSET_CARTRIDGE_TYPE);
        $code = ord($file->readChar());

        return match ($code) {
            0x00 => 'ROM ONLY',
            0x01 => 'MBC1',
            0x02 => 'MBC1+RAM',
            0x03 => 'MBC1+RAM+BATTERY',
            0x05 => 'MBC2',
            0x06 => 'MBC2+BATTERY',
            0x08 => 'ROM+RAM',
            0x09 => 'ROM+RAM+BATTERY',
            0x0B => 'MMM01',
            0x0C => 'MMM01+RAM',
            0x0D => 'MMM01+RAM+BATTERY',
            0x0F => 'MBC3+TIMER+BATTERY',
            0x10 => 'MBC3+TIMER+RAM+BATTERY',
            0x11 => 'MBC3',
            0x12 => 'MBC3+RAM',
            0x13 => 'MBC3+RAM+BATTERY',
            0x19 => 'MBC5',
            0x1A => 'MBC5+RAM',
            0x1B => 'MBC5+RAM+BATTERY',
            0x1C => 'MBC5+RUMBLE',
            0x1D => 'MBC5+RUMBLE+RAM',
            0x1E => 'MBC5+RUMBLE+RAM+BATTERY',
            0x20 => 'MBC6',
            0x22 => 'MBC7+SENSOR+RUMBLE+RAM+BATTERY',
            0xFC => 'POCKET CAMERA',
            0xFD => 'BANDAI TAMA5',
            0xFE => 'HuC3',
            0xFF => 'HuC1+RAM+BATTERY',

            default => 'Unknown',
        };
    }

    public function readRomSize(File $file): int
    {
        $file->seek(self::OFFSET_ROM_SIZE);
        $byte = ord($file->readChar());

        if ($byte <= 0x08) {
            return 32768 << $byte;
        }

        $bankSize = 1 << 14;

        return match ($byte) {
            0x52 => 72 * $bankSize,
            0x53 => 80 * $bankSize,
            0x54 => 96 * $bankSize,

            default => 0,
        };
    }

    public function readRomBankCount(File $file): int
    {
        $file->seek(self::OFFSET_ROM_SIZE);
        $byte = ord($file->readChar());

        if ($byte <= 0x08) {
            return 2 ** ($byte + 1);
        }

        return match ($byte) {
            0x52 => 72,
            0x53 => 80,
            0x54 => 96,

            default => 0,
        };
    }

    public function readRamSize(File $file): int
    {
        $file->seek(self::OFFSET_RAM_SIZE);
        $byte = ord($file->readChar());

        $bankSize = 8192;
        return match ($byte) {
            0x00 => 0,
            0x01 => 2048,
            0x02 => $bankSize,
            0x03 => $bankSize * 4,
            0x04 => $bankSize * 16,
            0x05 => $bankSize * 8,

            default => 0,
        };
    }

    public function readDestination(File $file): string
    {
        $file->seek(self::OFFSET_DESTINATION_CODE);
        $byte = ord($file->readChar());

        return match ($byte) {
            0x00 => 'Japanese',
            0x01 => 'Non-Japanese',

            default => 'Unknown',
        };
    }

    public function readRomVersion(File $file): int
    {
        $file->seek(self::OFFSET_ROM_VERSION);
        return ord($file->readChar());
    }

    public function verifyHeader(File $file): bool
    {
        $file->seek(self::OFFSET_TITLE);
        $check = 0;

        for ($i = self::OFFSET_TITLE; $i <= self::OFFSET_ROM_VERSION; $i++) {
            $check = $check - ord($file->readChar()) - 1;

            if ($check < 0) {
                $check += 0x0100;
            }
        }

        $headerChecksum = hexdec($this->readHeaderChecksum($file));
        return $headerChecksum === $check;
    }

    public function readHeaderChecksum(File $file): string
    {
        $file->seek(self::OFFSET_HEADER_CHECKSUM);
        return '0x' . strtoupper(bin2hex($file->readChar()));
    }

    public function readChecksum(File $file): string
    {
        $file->seek(self::OFFSET_CHECKSUM);
        return '0x' . strtoupper(bin2hex($file->read(2)));
    }

    public function readLogo(File $file): string
    {
        $file->seek(self::OFFSET_LOGO);
        return $file->read(0x30);
    }

    public function verifyLogo(File $file): bool
    {
        $logo         = $this->readLogo($file);
        $logoChecksum = md5($logo);

        return $logoChecksum === self::LOGO_MD5;
    }
}
