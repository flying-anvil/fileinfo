<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Sauerbraten;

use FlyingAnvil\Fileinfo\InfoExtractor\CacheInterface;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive\GzInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class OgzInfoExtractor implements InfoExtractor, CacheInterface
{
    // Cannot use this for file type recognition, as ogz files are gzipped
    public const OGZ_MAGIC = 'OCTA';

    private const OFFSET_VERSION        = 0x04;
    private const OFFSET_HEADER_SIZE    = 0x08;
    private const OFFSET_WORLD_SIZE     = 0x0C;
    private const OFFSET_ENTITY_COUNT   = 0x10;
    private const OFFSET_PVS_COUNT      = 0x14;
    private const OFFSET_LIGHTMAP_COUNT = 0x18;
    private const OFFSET_BLENDMAP_COUNT = 0x1C;
    private const OFFSET_VAR_COUNT      = 0x20;
    private const OFFSET_UNKNOWN        = 0x24;

    private const VAR_TYPE_INT    = 0;
    private const VAR_TYPE_FLOAT  = 1;
    private const VAR_TYPE_STRING = 2;

    private const VARIABLE_TYPE_MAPPING = [
        self::VAR_TYPE_INT => 'int',
        self::VAR_TYPE_FLOAT => 'float',
        self::VAR_TYPE_STRING => 'string',
    ];

    private const CACHE_KEY_VAR_END             = 'varEnd';
    private const CACHE_KEY_GAME_IDENTIFIER_END = 'gameidentEnd';

    private GzInfoExtractor $gzipExtractor;
    private UniversalCollection $cache;

    public function __construct()
    {
        $this->gzipExtractor = new GzInfoExtractor();
        $this->cache         = UniversalCollection::createEmpty();
    }

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::loadWrapper('compress.zlib://', $filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();

        $result->add(InfoCommonKeys::VERSION, $this->readVersion($file));
        $result->add(InfoCommonKeys::HEADER_SIZE, $this->readHeaderSize($file));
        $result->add(InfoCommonKeys::WORLD_SIZE, $this->readWorldSize($file));
        $result->add(InfoCommonKeys::ENTITY_COUNT, $this->readEntityCount($file));
        $result->add('pvsCount', $this->readPvsCount($file));
        $result->add(InfoCommonKeys::LIGHTMAP_COUNT, $this->readLightmapCount($file));
        $result->add(InfoCommonKeys::BLENDMAP_COUNT, $this->readBlendmapCount($file));
        $result->add('varCount', $this->readVarCount($file));
        $result->add(InfoCommonKeys::UNKNOWN, $this->readUnknown($file));

        $result->add(InfoCommonKeys::VARIABLES, $this->readVariables($file));
        $result->add('gameIdentifier', $this->readGameIdentifier($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    public function verify(File $file): bool
    {
        $rawFile = File::load($file->getFilePath());
        $rawFile->open();

        $validGzip = $this->gzipExtractor->verify($rawFile);
        $file->rewind();

        return $validGzip && $file->read(4) === self::OGZ_MAGIC;
    }

    public function readVersion(File $file): int
    {
        $file->seek(self::OFFSET_VERSION);
        return $file->readUInt32LittleEndian();
    }

    public function readHeaderSize(File $file): int
    {
        $file->seek(self::OFFSET_HEADER_SIZE);
        return $file->readUInt32LittleEndian();
    }

    public function readWorldSize(File $file): int
    {
        $file->seek(self::OFFSET_WORLD_SIZE);
        return $file->readUInt32LittleEndian();
    }

    public function readEntityCount(File $file): int
    {
        $file->seek(self::OFFSET_ENTITY_COUNT);
        return $file->readUInt32LittleEndian();
    }

    public function readPvsCount(File $file): int
    {
        $file->seek(self::OFFSET_PVS_COUNT);
        return $file->readUInt32LittleEndian();
    }

    public function readLightmapCount(File $file): int
    {
        $file->seek(self::OFFSET_LIGHTMAP_COUNT);
        return $file->readUInt32LittleEndian();
    }

    public function readBlendmapCount(File $file): int
    {
        $file->seek(self::OFFSET_BLENDMAP_COUNT);
        return $file->readUInt32LittleEndian();
    }

    public function readVarCount(File $file): int
    {
        $file->seek(self::OFFSET_VAR_COUNT);
        return $file->readUInt32LittleEndian();
    }

    /**
     * Documentation does not contain information about header offset 0x24.
     * It possibly is a newer feature than the documented version 29 describes.
     *
     * @param File $file
     * @return int
     */
    private function readUnknown(File $file): int
    {
        $file->seek(self::OFFSET_UNKNOWN);
        return $file->readUInt32LittleEndian();
    }

    public function readVariables(File $file): UniversalCollection
    {
        $varStart = $this->readHeaderSize($file);
        $varCount = $this->readVarCount($file);
        $vars     = UniversalCollection::createEmpty();

        $file->seek($varStart);

        for ($i = 0; $i < $varCount; $i++) {
            $type = $file->readUnsignedByte();

            switch ($type) {
                case self::VAR_TYPE_INT:
                case self::VAR_TYPE_FLOAT:
                    // TODO
                    continue 2;
                case self::VAR_TYPE_STRING:
                    $nameLength = $file->readUInt16LittleEndian();
                    $name = $file->read($nameLength);

                    $contentLength = $file->readUInt16LittleEndian();
                    $content = $file->read($contentLength);

                    $var = [
                        'type' => $this->translateVariableType($type),
                        'name' => $name,
                        'content' => $content,
                    ];

                    $vars->add($name, $var);
                    continue 2;
            }
        }

        /** @var UniversalCollection $cache */
        $cache = $this->cache->get($file->getFilePath(), UniversalCollection::createEmpty());
        $cache->add(self::CACHE_KEY_VAR_END, $file->tell());

        return $vars;
    }

    public function translateVariableType(int $type): string
    {
        return self::VARIABLE_TYPE_MAPPING[$type] ?? 'Unknown';
    }

    public function readGameIdentifier(File $file): string
    {
        $cache = $this->getCacheForFile($file);
        if (!$cache->get(self::CACHE_KEY_VAR_END, false)) {
            $this->readVariables($file);
        }

        /** @var UniversalCollection $cache */
        $cache = $this->cache->get($file->getFilePath());
        $gameIdentifierStart = $cache->get(self::CACHE_KEY_VAR_END);

        $file->seek($gameIdentifierStart);
        $nameLength = $file->readUnsignedByte();
        $identifier = $file->read($nameLength);

        // Ends with additional null-byte
        $cache->add(self::CACHE_KEY_GAME_IDENTIFIER_END, $file->tell() + 1);

        return $identifier;
    }

    private function getCacheForFile(File $file): UniversalCollection
    {
        $cache = $this->cache->get($file->getFilePath(), null, true);
        if (!$cache) {
            $cache = UniversalCollection::createEmpty();
            $this->cache->add($file->getFilePath(), $cache);
        }

        return $this->cache;
    }

    public function clearCache(): void
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function clearSpecificCache(string $filePath): void
    {
        $this->cache->remove($filePath);
    }
}
