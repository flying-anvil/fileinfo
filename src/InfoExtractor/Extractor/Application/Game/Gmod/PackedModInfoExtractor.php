<?php /** @noinspection DuplicatedCode */

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Gmod;

use DateTime;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * https://github.com/whisperity/SharpGMad/blob/master/SharpGMad/Reader.cs
 */
class PackedModInfoExtractor implements InfoExtractor
{
    public const GMA_MAGIC = 'GMAD';

    private const OFFSET_VERSION = 0x04;
    private const OFFSET_STEAM_ID = 0x05;
    private const OFFSET_TIMESTAMP = 0x0D;

    // TODO: Check if needed
    private const OFFSET_CONTENT = 0x15;

    private const OFFSET_NAME = 0x16;

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            return $result;
        }

        $version = $this->readVersion($file);
        $result->add(InfoCommonKeys::VERSION, $version);
        $result->add('steamId', $this->readSteamId($file));

        $timestamp = $this->readTimestamp($file);
        $result->add(InfoCommonKeys::TIMESTAMP, $timestamp);
        $result->add(InfoCommonKeys::DATETIME, (new DateTime("@$timestamp"))->format('Y-m-d H:i:s'));
        $result->add(InfoCommonKeys::NAME, $this->readName($file));
        $result->add(InfoCommonKeys::META_DATA, $this->readMetaDate($file));
        $result->add(InfoCommonKeys::AUTHOR, $this->readAuthor($file));
        $result->add('addonVersion', $this->readAddonVersion($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            return $result;
        }

        $timestamp = $this->readTimestamp($file);
        $result->add(InfoCommonKeys::DATETIME, (new DateTime("@$timestamp"))->format('Y-m-d H:i:s'));
        $result->add(InfoCommonKeys::NAME, $this->readName($file));
        $result->add(InfoCommonKeys::AUTHOR, $this->readAuthor($file));
        $result->add('addonVersion', $this->readAddonVersion($file));

        return $result;
    }

    private function verify(File $file): bool
    {
        return $this->readHeader($file) === self::GMA_MAGIC;
    }

    private function readHeader(File $file): string
    {
        $file->rewind();
        return $file->read(4);
    }

    private function readVersion(File $file): int
    {
        $file->seek(self::OFFSET_VERSION);
        $rawVersion = $file->readChar();

        return ord($rawVersion);
    }

    private function readSteamId(File $file): int
    {
        $file->seek(self::OFFSET_STEAM_ID);
        $rawId = $file->read(8);

//        var_dump(unpack('Pid', "\0\0\0\0\0\0\0\0"));
//        die;

        return unpack('P', $rawId)[1];
    }

    private function readTimestamp(File $file): int
    {
        $file->seek(self::OFFSET_TIMESTAMP);
        $rawTimestamp = $file->read(8);

        return unpack('P', $rawTimestamp)[1];
    }

    private function readName(File $file): string
    {
        $file->seek(self::OFFSET_NAME);
        return $file->readNullTerminatedSequence();
    }

    private function readMetaDate(File $file): UniversalCollection
    {
        $file->seek(self::OFFSET_NAME);
        $file->readNullTerminatedSequence(); // Skip Name

        $rawMetaData = $file->readNullTerminatedSequence();
        $parsedMetaData = json_decode($rawMetaData, true, 512, JSON_THROW_ON_ERROR);

        return UniversalCollection::createFromRecursive($parsedMetaData);
    }

    private function readAuthor(File $file): string
    {
        $file->seek(self::OFFSET_NAME);
        $file->readNullTerminatedSequence(); // Skip Name
        $file->readNullTerminatedSequence(); // Skip MetaData

        $rawName = $file->readNullTerminatedSequence();
        return str_replace('Author Name', '', $rawName);
    }

    private function readAddonVersion(File $file): int
    {
        $file->seek(self::OFFSET_NAME);
        $file->readNullTerminatedSequence(); // Skip Name
        $file->readNullTerminatedSequence(); // Skip MetaData
        $file->readNullTerminatedSequence(); // Skip Author Name

        $rawVersion = $file->read(4);
        return unpack('V', $rawVersion)[1];
    }

    // TODO: Check if needed
    private function getPostContentOffset(File $file, int $version): int
    {
        $file->seek(self::OFFSET_CONTENT);

        if ($version > 1) {
//            $content = '';
//
//            while (empty($content)) {
                $content = $file->readNullTerminatedSequence();
//            }
        }

        return $file->tell();
    }
}
