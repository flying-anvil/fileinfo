<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2;

use DateTime;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject\CharacterClass;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject\CharacterStatus;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject\CharacterTitle;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject\Difficulty;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject\Skill\Skill;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

/**
 * https://github.com/krisives/d2s-format
 * https://web.archive.org/web/20161224202623/http://www.coreyh.org:80/diablo-2-files/documentation/d2s_save_file_format_1.13d.html
 * https://www.neoseeker.com/diablo-ii/faqs/26409-hex-edit.html
 * https://daancoppens.wordpress.com/2017/01/25/understanding-the-diablo-2-save-file-format-part-1/
 */
class Diablo2SaveInfoExtractor implements InfoExtractor
{
    public const D2S_MAGIC = "\x55\xAA\x55\xAA";

    private const OFFSET_VERSION_ID = 0x04;
    private const OFFSET_FILE_SIZE = 0x08;
    private const OFFSET_CHECKSUM = 0x0C;
    private const OFFSET_ACTIVE_WEAPON = 0x10;
    private const OFFSET_CHARACTER_NAME = 0x14;
    private const OFFSET_CHARACTER_STATUS = 0x24;
    private const OFFSET_CHARACTER_PROGRESSION = 0x25;
    private const OFFSET_CHARACTER_CLASS = 0x28;
    private const OFFSET_LEVEL = 0x2B;
    private const OFFSET_TIME = 0x30;

    private const OFFSET_HOTKEYS = 0x38;
    private const OFFSET_PRIMARY_SKILL = 0x78;
    private const OFFSET_DIFFICULTY = 0xA8;

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        $result = UniversalCollection::createEmpty();
        $result->add(InfoCommonKeys::VERSION, $this->readVersionId($file));
        $result->add('fileSize', $this->readFileSize($file));
        $result->add(InfoCommonKeys::CHECKSUM, $this->readChecksum($file));
        $result->add(InfoCommonKeys::CHECKSUM . 'Hex', $this->readChecksumHex($file));
        $result->add('activeWeapon', $this->readActiveWeapon($file));
        $result->add('characterName', $this->readCharacterName($file));
//        $result->add('characterStatus', $this->readCharacterStatus($file)->jsonSerialize());
        $result->merge($this->readCharacterStatus($file)->jsonSerialize());
        $result->add('characterTitle', $this->readCharacterTitle($file));
        $result->add('characterClass', $this->readCharacterClass($file));
        $result->add('level', $this->readLevel($file));

        $timestamp = $this->readTime($file);
        $result->add(InfoCommonKeys::TIMESTAMP, $timestamp);
        $result->add(InfoCommonKeys::DATETIME, (new DateTime("@$timestamp"))->format('Y-m-d H:i:s'));

//        $result->add('hotkeys', $this->readHotkeys($file));
        $result->add('primarySkill', $this->readPrimarySkill($file));
        $result->add('difficulty', $this->readDifficulty($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        if (!$this->verify($file)) {
            throw NotResponsibleExtractorException::magicMismatch();
        }

        return $this->getInfo($filePath);
    }

    public function verify(File $file): bool
    {
        return $this->readSignature($file) === self::D2S_MAGIC;
    }

    public function readSignature(File $file): string
    {
        $file->rewind();

        return $file->read(4);
    }

    public function readVersionId(File $file): int
    {
        $file->seek(self::OFFSET_VERSION_ID);
        return unpack('V', $file->read(4))[1];
    }

    public function readFileSize(File $file): int
    {
        $file->seek(self::OFFSET_FILE_SIZE);

        return unpack('V', $file->read(4))[1];
    }

    public function readChecksum(File $file): int
    {
        $file->seek(self::OFFSET_CHECKSUM);

        return unpack('V', $file->read(4))[1];
    }

    public function readChecksumHex(File $file): string
    {
        $file->seek(self::OFFSET_CHECKSUM);

        return '0x' . strtoupper(bin2hex($file->read(4)));
    }

    public function readActiveWeapon(File $file): int
    {
        $file->seek(self::OFFSET_ACTIVE_WEAPON);

        return unpack('V', $file->read(4))[1];
    }

    public function readCharacterName(File $file): string
    {
        $file->seek(self::OFFSET_CHARACTER_NAME);

        return rtrim($file->read(16));
    }

    public function readCharacterStatus(File $file): CharacterStatus
    {
        $file->seek(self::OFFSET_CHARACTER_STATUS);

        $rawStatus = ord($file->read(1));

        /*
         * ? ? Expansion ? Dead Hardcore ? ?
         * 0 1 2         3 4    5        6 7
         */

        return CharacterStatus::create(
            (bool)($rawStatus & 0b00000100),
            (bool)($rawStatus & 0b00001000),
            (bool)($rawStatus & 0b00100000),
        );
    }

    public function readCharacterTitle(
        File $file,
        CharacterClass $class = null,
        CharacterStatus $status = null
    ): CharacterTitle {
        if ($class === null) {
            $class = $this->readCharacterClass($file);
        }

        if ($status === null) {
            $status = $this->readCharacterStatus($file);
        }

        $file->seek(self::OFFSET_CHARACTER_PROGRESSION);
        $titleByte = ord($file->read(1));
        return CharacterTitle::createFromByte($titleByte, $class, $status);
    }

    public function readCharacterClass(File $file): CharacterClass
    {
        $file->seek(self::OFFSET_CHARACTER_CLASS);

        $classId = ord($file->read(1));
        return CharacterClass::createFromId($classId);
    }

    public function readLevel(File $file): int
    {
        $file->seek(self::OFFSET_LEVEL);

        return ord($file->read(1));
    }

    public function readTime(File $file): int
    {
        $file->seek(self::OFFSET_TIME);

        return unpack('V', $file->read(4))[1];
    }

    /**
     * TODO: implement
     */
    public function readHotkeys(File $file): int
    {
        $file->seek(self::OFFSET_HOTKEYS);

        return unpack('P', $file->read(64))[1];
    }

    public function readPrimarySkill(File $file, CharacterClass $class = null): Skill
    {
        if ($class === null) {
            $class = $this->readCharacterClass($file);
        }

        // TODO remove +4 which is for secondary skill
        $file->seek(self::OFFSET_PRIMARY_SKILL+4);
        $raw = $file->read(4);
        $id = unpack('V', $raw)[1];

        return Skill::createFromIdAndLevel($id);
    }

    public function readDifficulty(File $file): string
    {
        $file->seek(self::OFFSET_DIFFICULTY);

        $difficulty = Difficulty::createFromBytes(
            ord($file->read(1)),
            ord($file->read(1)),
            ord($file->read(1)),
        );

        return (string)$difficulty;
    }
}
