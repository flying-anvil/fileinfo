<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

final class CharacterClass implements DataObject, StringValue, Stringable
{
    public const NAME_AMAZON      = 'Amazon';
    public const NAME_SORCERESS   = 'Sorceress';
    public const NAME_NECROMANCER = 'Necromancer';
    public const NAME_PALADIN     = 'Paladin';
    public const NAME_BARBARIAN   = 'Barbarian';
    public const NAME_DRUID       = 'Druid';
    public const NAME_ASSASSIN    = 'Assassin';

    public const ID_AMAZON      = 0;
    public const ID_SORCERESS   = 1;
    public const ID_NECROMANCER = 2;
    public const ID_PALADIN     = 3;
    public const ID_BARBARIAN   = 4;
    public const ID_DRUID       = 5;
    public const ID_ASSASSIN    = 6;

    private const MAPPING_CHARACTER_CLASS = [
        self::ID_AMAZON      => self::NAME_AMAZON,
        self::ID_SORCERESS   => self::NAME_SORCERESS,
        self::ID_NECROMANCER => self::NAME_NECROMANCER,
        self::ID_PALADIN     => self::NAME_PALADIN,
        self::ID_BARBARIAN   => self::NAME_BARBARIAN,
        self::ID_DRUID       => self::NAME_DRUID,
        self::ID_ASSASSIN    => self::NAME_ASSASSIN,
    ];

    private function __construct(private string $class) {}

    public static function createFromString(string $class): self
    {
        if (!in_array($class, self::MAPPING_CHARACTER_CLASS, true)) {
            $class = 'Unknown';
        }

        return new self($class);
    }

    public static function createFromId(int $id): self
    {
        return new self(self::MAPPING_CHARACTER_CLASS[$id] ?? 'Unknown');
    }

    public function getName(): string
    {
        return $this->class;
    }

    public function getId(): int
    {
        return array_flip(self::MAPPING_CHARACTER_CLASS)[$this->class] ?? 0xFF;
    }

    public function jsonSerialize()
    {
        return $this->class;
    }

    public function toString(): string
    {
        return $this->class;
    }

    public function __toString(): string
    {
        return $this->class;
    }
}
