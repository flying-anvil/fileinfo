<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

final class CharacterTitle implements DataObject, StringValue, Stringable
{
    public const TITLE_NONE      = 'None';
    public const TITLE_SLAYER    = 'Slayer';
    public const TITLE_CHAMPION  = 'Champion';
    public const TITLE_PATRIARCH = 'Patriarch';
    public const TITLE_MATRIARCH = 'Matriarch';

    public const TITLE_HC_DESTROYER = 'Destroyer';
    public const TITLE_HC_CONQUEROR = 'Conqueror';
    public const TITLE_HC_GUARDIAN  = 'Guardian';

    private const GENDER_MAPPING = [
        CharacterClass::ID_AMAZON      => 'f',
        CharacterClass::ID_SORCERESS   => 'f',
        CharacterClass::ID_NECROMANCER => 'm',
        CharacterClass::ID_PALADIN     => 'm',
        CharacterClass::ID_BARBARIAN   => 'm',
        CharacterClass::ID_DRUID       => 'm',
        CharacterClass::ID_ASSASSIN    => 'f',
    ];

    private function __construct(private string $title) {}

    public static function createFromString(string $title): self
    {
        return new self($title);
    }

    public static function createFromByte(int $byte, CharacterClass $class, CharacterStatus $status): self
    {
        if ($byte >= 5) {
            $title = $status->isHardcore() ? self::TITLE_HC_DESTROYER : self::TITLE_SLAYER;
            return new self($title);
        }

        if ($byte >= 10) {
            $title = $status->isHardcore() ? self::TITLE_HC_CONQUEROR : self::TITLE_CHAMPION;
            return new self($title);
        }

        if ($byte >= 10) {
            $gender        = self::GENDER_MAPPING[$class->getId()];
            $softcoreTitle = $gender === 'm' ? self::TITLE_PATRIARCH : self::TITLE_MATRIARCH;
            $title         = $status->isHardcore() ? self::TITLE_HC_GUARDIAN : $softcoreTitle;

            return new self($title);
        }

        return new self(self::TITLE_NONE);
    }

    public function jsonSerialize()
    {
        return $this->title;
    }

    public function toString(): string
    {
        return $this->title;
    }

    public function __toString(): string
    {
        return $this->title;
    }
}
