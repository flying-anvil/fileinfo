<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject;

use FlyingAnvil\Libfa\Conversion\Describable;
use FlyingAnvil\Libfa\DataObject\DataObject;

final class CharacterStatus implements DataObject, Describable
{
    private function __construct(
        private bool $isHardcore,
        private bool $hasDied,
        private bool $isExpansion,
    ) {}

    public static function create(bool $isHardcore, bool $hasDied, bool $isExpansion): self
    {
        return new self($isHardcore, $hasDied, $isExpansion);
    }

    public function isHardcore(): bool
    {
        return $this->isHardcore;
    }

    public function setIsHardcore(bool $isHardcore): void
    {
        $this->isHardcore = $isHardcore;
    }

    public function isHasDied(): bool
    {
        return $this->hasDied;
    }

    public function setHasDied(bool $hasDied): void
    {
        $this->hasDied = $hasDied;
    }

    public function isExpansion(): bool
    {
        return $this->isExpansion;
    }

    public function setIsExpansion(bool $isExpansion): void
    {
        $this->isExpansion = $isExpansion;
    }

    public function jsonSerialize(): array
    {
        return [
            'isHardcore'  => $this->isHardcore,
            'hasDied'     => $this->hasDied,
            'isExpansion' => $this->isExpansion,
        ];
    }

    public function describe(): string
    {
        return sprintf(
            'The character is%s a hardcore character, has%s died and is%d an expansion character',
                $this->isHardcore ? '' : ' not',
                $this->hasDied ? '' : ' not',
                $this->isExpansion ? '' : ' not',
        );
    }
}
