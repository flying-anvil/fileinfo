<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;

final class BaseDifficulty implements DataObject
{
    private function __construct(
        private bool $isActive,
        private int $act,
    ) {}

    public static function createFromByte(int $byte): self
    {
        $isActive = (bool)($byte & 0b10000000);
        $act      = ($byte & 0b00000111) + 1;

        return new self($isActive, $act);
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getAct(): int
    {
        return $this->act;
    }

    public function jsonSerialize(): array
    {
        return [
            'isActive' => $this->isActive,
            'act'      => $this->act,
        ];
    }
}
