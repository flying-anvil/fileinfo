<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

/**
 * This is jank, but the save is like that :(
 */
final class Difficulty implements DataObject, StringValue, Stringable
{
    public const DIFFICULTY_NORMAL    = 'Normal';
    public const DIFFICULTY_NIGHTMARE = 'Nightmare';
    public const DIFFICULTY_HELL      = 'Hell';

    private const MAPPING_ACT_ROMAN = [
        1 => 'I',
        2 => 'II',
        3 => 'III',
        4 => 'IV',
        5 => 'V',
    ];

    /** @var BaseDifficulty[] */
    private array $difficulties;

    public function __construct(
        array $difficulties,
        private string $active
    ) {
        $this->difficulties = $difficulties;
    }

    public static function createFromBytes(int $normalByte, int $nightmareByte, int $hellByte): self
    {
        /** @var BaseDifficulty[] $difficulties */
        $difficulties = [
            self::DIFFICULTY_NORMAL    => BaseDifficulty::createFromByte($normalByte),
            self::DIFFICULTY_NIGHTMARE => BaseDifficulty::createFromByte($nightmareByte),
            self::DIFFICULTY_HELL      => BaseDifficulty::createFromByte($hellByte),
        ];

        $active = '';

        foreach ($difficulties as $name => $difficulty) {
            if ($difficulty->isActive()) {
                $active = $name;
                break;
            }
        }

        return new self($difficulties, $active);
    }

    public function getActiveName(): string
    {
        return $this->active;
    }

    public function getActiveDifficulty(): ?BaseDifficulty
    {
        return $this->difficulties[$this->active] ?? null;
    }

    public function jsonSerialize(): array
    {
        $temp = $this->difficulties;
        $temp['active'] = $this->active;

        return $temp;
    }

    public function toString(): string
    {
        $active = $this->getActiveDifficulty();
        $act    = $active !== null ? $active->getAct() : '0';

        return sprintf(
            '%s Act %s',
            $this->active,
            self::MAPPING_ACT_ROMAN[$act] ?? '0',
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
