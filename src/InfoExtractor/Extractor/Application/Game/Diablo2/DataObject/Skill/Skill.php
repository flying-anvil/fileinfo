<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\DataObject\Skill;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use Stringable;

final class Skill implements DataObject, StringValue, Stringable
{
    private function __construct(
        private int $id,
        private string $name,
        private int $level,
    ) {}

    public static function createFromIdAndLevel(int $id): Skill
    {
        return new self($id, SkillIdTranslation::translateIdToName($id), 0);
    }

//    public static function createFromClassAndId(CharacterClass $class, int $skillId): Skill
//    {
//        if ($class->getId() === CharacterClass::ID_PALADIN) {
//            return PaladinSkill::createFromId($skillId);
//        }
//    }

    public function toString(): string
    {
        return $this->level > 0
            ? $this->name . ' ' . $this->level
            : $this->name;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function jsonSerialize(): array
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'level' => $this->level,
        ];
    }
}
