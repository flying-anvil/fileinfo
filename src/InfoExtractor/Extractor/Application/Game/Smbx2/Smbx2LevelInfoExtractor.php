<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Smbx2;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\DataObject\Exception\MissingKeyException;
use FlyingAnvil\Libfa\Wrapper\File;

class Smbx2LevelInfoExtractor implements InfoExtractor
{
    private const SECTIONS = [
        'HEAD',
        'SECTION',
        'STARTPOINT',
        'BLOCK',
        'BGO',
        'NPC',
        'DOORS',
        'LAYERS',
        'EVENTS_CLASSIC',
    ];

    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        while (($line = $file->readLine()) !== false) {
            $line = trim($line);

            if (in_array($line, self::SECTIONS, true)) {
                switch ($line) {
                    case 'HEAD':
                        $headInfo = $this->processHead($file);
                        $result->add('head', $headInfo);
                        break;
                    case 'SECTION':
                        $sectionInfo = $this->processSection($file);
                        $result->add('section', $sectionInfo);
                        break;
                    case 'STARTPOINT':
                        $startpointInfo = $this->processStartpoint($file);
                        $result->add('startpoint', $startpointInfo);
                        break;
                    case 'BLOCK':
                        $blockInfo = $this->processBlock($file);
                        $result->add('block', $blockInfo);
                        break;
                    case 'BGO':
                        $bgoInfo = $this->processBgo($file);
                        $result->add('bgo', $bgoInfo);
                        break;
                    case 'NPC':
                        $npcInfo = $this->processNpc($file);
                        $result->add('npc', $npcInfo);
                        break;
                    case 'DOORS':
                        $doorsInfo = $this->processDoors($file);
                        $result->add('doors', $doorsInfo);
                        break;
                    case 'LAYERS':
                        $layersInfo = $this->processLayers($file);
                        $result->add('layers', $layersInfo);
                        break;
                    case 'EVENTS_CLASSIC':
                        $eventsClassicInfo = $this->processEventsClassic($file);
                        $result->add('eventsClassic', $eventsClassicInfo);
                        break;
                }
            }
        }

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $file = File::load($filePath);
        $file->open();

        while (($line = $file->readLine()) !== false) {
            $line = trim($line);

            if (in_array($line, self::SECTIONS, true)) {
                switch ($line) {
                    case 'HEAD':
                        $headInfo  = $this->processHead($file);
                        $titleInfo = $headInfo->get('title');

                        $result->add('title', $titleInfo->get('name'));

                        try {
                            $result->add('showInMarioChallenge', $titleInfo->get('xtra')->get('showInMarioChallenge'));
                        } catch (MissingKeyException) {
                            $result->add('showInMarioChallenge', false);
                        }

                        break;
                }
            }
        }

        return $result;
    }

    private function processHead(File $file): UniversalCollection
    {
        $title  = UniversalCollection::createEmpty()->add('name', '');
        $result = UniversalCollection::createEmpty()->add('title', $title);

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'HEAD_END') {
                break;
            }

            if (str_starts_with($line, 'TL:')) {
                $row = $this->lineToCollection($line);

                $title->add('name', $row->get('TL', ''));
                $this->addXtra($title, $row->get('XTRA', ''));
            }
        }

        return $result;
    }

    private function processSection(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'SECTION_END') {
                break;
            }

            if (str_starts_with($line, 'SC:')) {
                $row = $this->lineToCollection($line);

                $sectionId = $row->get('SC', '-1');

                $section = UniversalCollection::createEmpty();
                $section->add('id', $sectionId);

                $this->addXtra($section, $row->get('XTRA', ''));
                $result->add($sectionId, $section);
            }
        }

        $result->add('count', count($result));

        return $result;
    }

    private function processStartpoint(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'STARTPOINT_END') {
                break;
            }

            if (str_starts_with($line, 'ID:')) {
                $row = $this->lineToCollection($line);

                $startpointId = $row->get('ID', '-1');

                $section = UniversalCollection::createEmpty();
                $section->add('id', $startpointId);

                $this->addXtra($section, $row->get('XTRA', ''));
                $result->add($startpointId, $section);
            }
        }

        $result->add('count', count($result));

        return $result;
    }

    private function processBlock(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $blockCount = [];
        $totalCount = 0;

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'BLOCK_END') {
                break;
            }

            if (str_starts_with($line, 'ID:')) {
                $row     = $this->lineToCollection($line);
                $blockId = $row->get('ID', -1);

                $blockCount[$blockId] = ($blockCount[$blockId] ?? 0) + 1;
                $totalCount++;
            }
        }

        $result->add('totalBlockCount', $totalCount);
        $result->add('blockCountById', UniversalCollection::createFrom($blockCount));

        return $result;
    }

    private function processBgo(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $bgoCount   = [];
        $totalCount = 0;

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'BGO_END') {
                break;
            }

            if (str_starts_with($line, 'ID:')) {
                $row     = $this->lineToCollection($line);
                $blockId = $row->get('ID', -1);

                $bgoCount[$blockId] = ($bgoCount[$blockId] ?? 0) + 1;
                $totalCount++;
            }
        }

        $result->add('totalBgoCount', $totalCount);
        $result->add('bgoCountById', UniversalCollection::createFrom($bgoCount));

        return $result;
    }

    private function processNpc(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $npcCount   = [];
        $totalCount = 0;

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'NPC_END') {
                break;
            }

            if (str_starts_with($line, 'ID:')) {
                $row     = $this->lineToCollection($line);
                $blockId = $row->get('ID', -1);

                $npcCount[$blockId] = ($npcCount[$blockId] ?? 0) + 1;
                $totalCount++;
            }
        }

        $result->add('totalNpcCount', $totalCount);
        $result->add('npcCountById', UniversalCollection::createFrom($npcCount));

        return $result;
    }

    private function processDoors(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $count = 0;

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'DOORS_END') {
                break;
            }

            $count++;
        }

        $result->add('count', $count);

        return $result;
    }

    private function processLayers(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $count = 0;

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'LAYERS_END') {
                break;
            }

            if (str_starts_with($line, 'LR:')) {
                $row   = $this->lineToCollection($line);
                $layer = UniversalCollection::createEmpty();

                $layer->add('name', $row->get('LR', ''));

                $result->add((string)$count, $layer);

                $count++;
            }

        }

        return $result;
    }

    private function processEventsClassic(File $file): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        $count = 0;

        while ($line = $file->readLine()) {
            $line = trim($line);

            if ($line === 'EVENTS_CLASSIC_END') {
                break;
            }

            if (str_starts_with($line, 'ET:')) {
                $row   = $this->lineToCollection($line);
                $layer = UniversalCollection::createEmpty();

                $layer->add('name', $row->get('ET', ''));

                $result->add((string)$count, $layer);

                $count++;
            }

        }

        return $result;
    }

    private function addXtra(UniversalCollection $resultSet, string $xtra): void
    {
        if ($xtra === '') {
            return;
        }

        $cleanedData = str_replace(['\n', '\"', '\:', '\,'], ["\n", '"', ':', ','], $xtra);
        $data        = json_decode($cleanedData, true, flags: JSON_THROW_ON_ERROR);

        $resultSet->add('xtra', UniversalCollection::createFromRecursive($data));
    }

    private function lineToCollection(string $line): UniversalCollection
    {
        $data = explode(';', $line);
        array_pop($data);

        $collection = UniversalCollection::createEmpty();

        foreach ($data as $rawValue) {
            $colon = strpos($rawValue, ':');
            $key   = substr($rawValue, 0, $colon);
            $value = substr($rawValue, $colon + 1);

            if (str_starts_with($value, '"')) {
                $value = substr($value, 1, -1);
            }

            $collection->add($key, $value);
        }

        return $collection;
    }
}
