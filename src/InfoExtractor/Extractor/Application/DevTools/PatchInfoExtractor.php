<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\DevTools;

use FlyingAnvil\Fileinfo\InfoExtractor\CacheInterface;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\DataObject\Counter\MultiCounter;
use FlyingAnvil\Libfa\Wrapper\File;

class PatchInfoExtractor implements InfoExtractor, CacheInterface
{
    public const PATCH_MAGIC = 'diff';

    private const IDENTIFIER_FILE = 'diff';
    private const IDENTIFIER_ADDITION = '+';
    private const IDENTIFIER_DELETION = '-';

    private const COUNT_FILES = 'files';
    private const COUNT_ADDITIONS = 'additions';
    private const COUNT_DELETION = 'deletions';

    private UniversalCollection $cache;

    public function __construct()
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function getInfo(string $filePath): UniversalCollection
    {
        $file = File::load($filePath);
        $file->open();

        $valid = $this->verify($file);

        if (!$valid) {
            throw new NotResponsibleExtractorException('Header mismatch');
        }

        $file->rewind();
        $result = UniversalCollection::createEmpty();

        $result->add(InfoCommonKeys::FILE_COUNT, $this->readFileCount($file));
        $result->add(InfoCommonKeys::ADDITION_COUNT, $this->readAdditionCount($file));
        $result->add(InfoCommonKeys::DELETION_COUNT, $this->readDeletionCount($file));
        $result->add(InfoCommonKeys::CHANGE_COUNT, $this->readChangeCount($file));

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    public function verify(File $file): bool
    {
        $header = $file->read(strlen(self::PATCH_MAGIC));

        return $header === self::PATCH_MAGIC;
    }

    public function readFileCount(File $file): int
    {
        $this->cacheCounts($file);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($file->getFilePath());
        return $counts->get(self::COUNT_FILES);
    }

    public function readAdditionCount(File $file): int
    {
        $this->cacheCounts($file);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($file->getFilePath());
        return $counts->get(self::COUNT_ADDITIONS);
    }

    public function readDeletionCount(File $file): int
    {
        $this->cacheCounts($file);

        /** @var MultiCounter $counts */
        $counts = $this->cache->get($file->getFilePath());
        return $counts->get(self::COUNT_DELETION);
    }

    private function readChangeCount(File $file): int
    {
        return $this->readAdditionCount($file) + $this->readDeletionCount($file);
    }

    private function cacheCounts(File $file): void
    {
        $filePath = $file->getFilePath();
        if ($this->cache->has($filePath)) {
            return;
        }

        $counter = MultiCounter::createEmpty();
        foreach ($file as $line) {
            $line = trim($line);
            if ($line === '') {
                continue;
            }

            if (strpos($line, self::IDENTIFIER_FILE) === 0) {
                $counter->increment(self::COUNT_FILES);
                continue;
            }

            $identifier = mb_substr($line, 0, 1);
            switch ($identifier) {
                case self::IDENTIFIER_ADDITION:
                    $counter->increment(self::COUNT_ADDITIONS);
                    continue 2;
                case self::IDENTIFIER_DELETION:
                    $counter->increment(self::COUNT_DELETION);
                    continue 2;
            }

        }

        $this->cache->add($filePath, $counter);
    }

    public function clearCache(): void
    {
        $this->cache = UniversalCollection::createEmpty();
    }

    public function clearSpecificCache(string $filePath): void
    {
        $this->cache->remove($filePath);
    }
}
