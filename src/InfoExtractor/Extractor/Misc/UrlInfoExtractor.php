<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Misc;

use FlyingAnvil\Fileinfo\InfoExtractor\InfoCommonKeys;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;
use FlyingAnvil\Libfa\Wrapper\File;

class UrlInfoExtractor implements InfoExtractor
{
    public function getInfo(string $filePath): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();
        $file = File::load($filePath);
        $file->open();

        $url = $this->readUrl($file);
        if ($url) {
            $result->add(InfoCommonKeys::URL, $url);
        }

        return $result;
    }

    public function getInfoSummary(string $filePath): UniversalCollection
    {
        return $this->getInfo($filePath);
    }

    private function readUrl(File $file): ?string
    {
        foreach ($file as $line) {
            $line = trim($line);

            if (strlen($line) <= 4) {
                return null;
            }

            if (str_starts_with($line, 'URL=')){
                return substr($line, 4);
            }
        }

        return null;
    }
}
