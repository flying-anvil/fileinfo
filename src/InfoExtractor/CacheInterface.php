<?php

namespace FlyingAnvil\Fileinfo\InfoExtractor;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;

interface CacheInterface
{
    public function clearCache(): void;
    public function clearSpecificCache(string $filePath): void;
}
