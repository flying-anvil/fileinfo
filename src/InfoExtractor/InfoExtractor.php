<?php

namespace FlyingAnvil\Fileinfo\InfoExtractor;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;

interface InfoExtractor
{
    /**
     * Gather all possible information about a given file.
     *
     * @throws NotResponsibleExtractorException if the extractor cannot gather information from the file
     */
    public function getInfo(string $filePath): UniversalCollection;

    /**
     * Gather only the important information about a file.
     * Should be flat, without nested information.
     *
     * @throws NotResponsibleExtractorException if the extractor cannot gather information from the file
     */
    public function getInfoSummary(string $filePath): UniversalCollection;
}
