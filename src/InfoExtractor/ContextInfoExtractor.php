<?php

namespace FlyingAnvil\Fileinfo\InfoExtractor;

use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;

interface ContextInfoExtractor
{
    public function getInfoByContext(string $filePath, Context $context): UniversalCollection;
}
