<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor;

class InfoCommonKeys
{
    public const GAME_TITLE = 'gameTitle';
    public const SONG_TITLE = 'songTitle';

    public const VERSION      = 'version';
    public const COUNTRY_CODE = 'countryCode';
    public const COUNTRY_CODE_TRANSLATION = 'countryCodeTranslation';
    public const COMMENT      = 'comment';
    public const NAME         = 'name';
    public const AUTHOR       = 'author';

    public const ADDITION_COUNT = 'additions';
    public const DELETION_COUNT = 'deletions';
    public const CHANGE_COUNT   = 'changes';

    // Data
    public const META_DATA       = 'metaData';
    public const CHECKSUM        = 'checksum';
    public const HEADER_CHECKSUM = 'headerChecksum';
    public const HEADER_SIZE     = 'headerSize';
    public const FILE_SIZE       = 'fileSize';
    public const FLAGS           = 'flags';
    public const URL             = 'url';
    public const TARGET          = 'target';
    public const LICENSE         = 'license';
    public const LICENSEE        = 'licensee';
    public const DESTINATION     = 'destination';
    public const LOCALIZATION    = 'localization';
    public const DESCRIPTION     = 'description';

    // Multimedia
    //   Video
    public const FRAME_COUNT       = 'frameCount';
    public const FRAME_RATE        = 'frameRate';
    public const FRAMES_PER_SECOND = 'framesPerSecond';
    public const FRAME_DURATION    = 'frameDuration';
    public const RE_RECORD_COUNT   = 'reRecordCount';
    public const VI_COUNT          = 'viCount';     // VI = Vertical Interrupt
    public const VI_PER_SECOND     = 'viPerSecond'; // VI = Vertical Interrupt

    //   Image
    public const RESOLUTION     = 'resolution';
    public const RESOLUTION_X   = 'resolutionX';
    public const RESOLUTION_Y   = 'resolutionY';
    public const ASPECT_RATIO   = 'aspectRatio';
    public const COLOR_DEPTH    = 'colorDepth';
    public const COLOR_COUNT    = 'colorCount';
    public const PIXEL_WIDTH    = 'pixelWidth';
    public const PIXEL_HEIGHT   = 'pixelHeight';
    public const PIXEL_RATIO    = 'pixelRatio';

    //   Audio
    public const DURATION        = 'duration';
    public const AUDIO_TRACKS    = 'audioTracks';
    public const CHANNELS        = 'channels';
    public const CHANNEL_COUNT   = 'channelCount';
    public const SAMPLE_RATE     = 'sampleRate';
    public const BIT_RATE        = 'bitRate';
    public const BYTE_RATE       = 'byteRate';
    public const BITS_PER_SAMPLE = 'bitsPerSample';
    public const VOLUME          = 'volume';
    public const MASTER_VOLUME   = 'masterVolume';
    public const TEMPO           = 'tempo';
    public const LOOP            = 'loop';
    public const LOOP_OFFSET     = 'loopOffset';

    // Date
    public const TIMESTAMP = 'timestamp';
    public const DATETIME  = 'datetime';
    public const DATE      = 'date';

    // ROM stuff
    public const ROM_SIZE       = 'romSize';
    public const RAM_SIZE       = 'ramSize';
    public const GAME_CODE      = 'gameCode';
    public const MAKER_CODE     = 'makerCode';
    public const CARTRIDGE_TYPE = 'cartridgeType';

    // Archive
    public const FILE_COUNT         = 'fileCount';
    public const DIRECTORY_COUNT    = 'directoryCount';
    public const COMPRESSED_SIZE    = 'compressedSize';
    public const UNCOMPRESSED_SIZE  = 'uncompressedSize';
    public const COMPRESSION_RATIO  = 'compressionRation';
    public const COMPRESSION_METHOD = 'compressionMethod';
    public const ORIGINAL_NAME      = 'originalName';

    // Geometry
    public const VERTICES   = 'vertices';
    public const FACES      = 'faces';
    public const LINES      = 'lines';
    public const WORLD_SIZE = 'worldSize';

    public const ENTITY_COUNT   = 'entityCount';
    public const LIGHTMAP_COUNT = 'lightmapCount';
    public const BLENDMAP_COUNT = 'blendmapCount';

    public const ENTITIES  = 'entities';
    public const LIGHTMAPS = 'lightmaps';
    public const BLENDMAPS = 'blendmaps';

    // Math
    public const COORDINATE_X = 'coordinateX';
    public const COORDINATE_Y = 'coordinateY';
    public const COORDINATE_Z = 'coordinateZ';

    // Misc
    public const OPERATING_SYSTEM = 'operatingSystem';
    public const UNKNOWN          = 'unknown';
    public const VARIABLES        = 'variables';
}
