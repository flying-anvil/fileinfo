<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Factory;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\DependencyException;
use Psr\Container\ContainerInterface;
use ReflectionClass;

class IndependentContainer implements ContainerInterface
{
    /** @var object[]  */
    private array $cache = [];

    public function get(string $className)
    {
        if (!$this->has($className)) {
            throw new DependencyException(sprintf(
                '\'%s\' has dependencies, which cannot be resolved.',
                $className,
            ));
        }

        if (!isset($this->cache[$className])) {
            $this->cache[$className] = new $className();
        }

        return $this->cache[$className];
    }

    public function has(string $className): bool
    {
        $reflection  = new ReflectionClass($className);
        $constructor = $reflection->getConstructor();

        return $constructor === null || empty($constructor->getParameters());
    }
}
