<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Factory;

use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\DevTools\PatchInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Gb\GbInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Gba\GbaInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64\M64ReplayCaptureInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64\M64SequenceInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64\Z64InfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Nds\NdsInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Nes\NesInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Snes\SmcInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Snes\SpcInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\WiiU\RpxInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\Diablo2SaveInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Gmod\PackedModInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\N64\SM64SaveInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Sauerbraten\OgzInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Smbx2\Smbx2LevelInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive\GzInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive\ZipInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Geometry\ObjInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio\ExtendedModuleInfoExtractor;
use FlyingAnvil\Fileinfo\FileExtension\KnownExtensions;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio\ModuleInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio\WaveInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Image\AsepriteInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Image\CommonImageInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Video\BinkInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Misc\UrlInfoExtractor;

class DefaultDefinition
{
    public function getDefaultExtensionDefinition(): array
    {
        return [
            // Application
            //   DevTools
            KnownExtensions::APPLICATION_DEVTOOLS_PATCH => [
                PatchInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_DEVTOOLS_DIFF => [
                PatchInfoExtractor::class,
            ],

            //   Games
            KnownExtensions::APPLICATION_GAME_SMBX2_LEVEL => [
                Smbx2LevelInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_GMOD_PACKED_MOD => [
                PackedModInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_DIABLO2_SAVEGAME => [
                Diablo2SaveInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_N64_EEPROM => [
                SM64SaveInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_SAUERBRATEN_MAP => [
                OgzInfoExtractor::class,
            ],

            //     Common
            // Also APPLICATION_GAME_COMMON_M64_REPLAY_CAPTURE
            KnownExtensions::APPLICATION_GAME_COMMON_M64_SEQUENCE => [
                M64SequenceInfoExtractor::class,
                M64ReplayCaptureInfoExtractor::class,
            ],

            //       ROMs
            KnownExtensions::APPLICATION_GAME_COMMON_WIU_EXECUTABLE => [
                RpxInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_SNES_SOUNDTRACK => [
                SpcInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_N64_ROM_BIGENDIAN => [
                Z64InfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_SNES_ROM => [
                SmcInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_SFAM_ROM => [
                SmcInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_NDS_ROM => [
                NdsInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_GBA_ROM => [
                GbaInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_GB_ROM => [
                GbInfoExtractor::class,
            ],
            KnownExtensions::APPLICATION_GAME_COMMON_NES_ROM => [
                NesInfoExtractor::class,
            ],

            // Archive
            KnownExtensions::ARCHIVE_ZIP => [
                ZipInfoExtractor::class,
            ],
            KnownExtensions::ARCHIVE_JAR => [
                ZipInfoExtractor::class,
            ],
            KnownExtensions::ARCHIVE_GZ => [
                GzInfoExtractor::class
            ],

            // Media
            //   Audio
            KnownExtensions::MEDIA_AUDIO_EXTENDED_MODULE => [
                ExtendedModuleInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_AUDIO_MODULE => [
                ModuleInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_AUDIO_WAVE => [
                WaveInfoExtractor::class,
            ],

            //   Video
            KnownExtensions::MEDIA_VIDEO_BINK => [
                BinkInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_VIDEO_BINK_VERSION_2 => [
                BinkInfoExtractor::class,
            ],
            'bik2' => [
                BinkInfoExtractor::class,
            ],

            //   Image
            KnownExtensions::MEDIA_IMAGE_PNG => [
                CommonImageInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_IMAGE_BMP => [
                CommonImageInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_IMAGE_GIF => [
                CommonImageInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_IMAGE_JPG => [
                CommonImageInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_IMAGE_JPEG => [
                CommonImageInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_IMAGE_JFIF => [
                CommonImageInfoExtractor::class,
            ],
            KnownExtensions::MEDIA_IMAGE_ASEPRITE => [
                AsepriteInfoExtractor::class,
            ],

            // Geometry
            KnownExtensions::GEOMETRY_OBJ => [
                ObjInfoExtractor::class,
            ],

            // Misc
            KnownExtensions::MISC_URL => [
                UrlInfoExtractor::class,
            ],
        ];
    }

    public function getDefaultContextDefinition(): array
    {
        return [
            // Application
            //   DevTools
            Context::APPLICATION_DEVTOOLS_PATCH => [
                PatchInfoExtractor::class,
            ],
            Context::APPLICATION_DEVTOOLS_DIFF => [
                PatchInfoExtractor::class,
            ],

            //   Games
            Context::APPLICATION_GAME_SMBX2_LEVEL => [
                Smbx2LevelInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_GMOD_PACKED_MOD => [
                PackedModInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_DIABLO2_SAVEGAME => [
                Diablo2SaveInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_N64_SM64_SAVEGAME => [
                SM64SaveInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_SAUERBRATEN_MAP => [
                OgzInfoExtractor::class,
            ],

            //     Common
            Context::APPLICATION_GAME_COMMON_M64_SEQUENCE => [
                M64SequenceInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_M64_REPLAY_CAPTURE => [
                M64ReplayCaptureInfoExtractor::class,
            ],

            //       ROMs
            Context::APPLICATION_GAME_COMMON_WIU_EXECUTABLE => [
                RpxInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_SNES_SOUNDTRACK => [
                SpcInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_N64_ROM_BIGENDIAN => [
                Z64InfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_SNES_ROM => [
                SmcInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_SFAM_ROM => [
                SmcInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_NDS_ROM => [
                NdsInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_GBA_ROM => [
                GbaInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_GB_ROM => [
                GbInfoExtractor::class,
            ],
            Context::APPLICATION_GAME_COMMON_NES_ROM => [
                NesInfoExtractor::class,
            ],

            // Archive
            Context::ARCHIVE_ZIP => [
                ZipInfoExtractor::class,
            ],
            Context::ARCHIVE_JAR => [
                ZipInfoExtractor::class,
            ],
            Context::ARCHIVE_GZIP => [
                GzInfoExtractor::class
            ],

            // Media
            //   Audio
            Context::MEDIA_AUDIO_EXTENDED_MODULE => [
                ExtendedModuleInfoExtractor::class,
            ],
            Context::MEDIA_AUDIO_MODULE => [
                ModuleInfoExtractor::class,
            ],
            Context::MEDIA_AUDIO_WAVE => [
                WaveInfoExtractor::class,
            ],

            //   Video
            Context::MEDIA_VIDEO_BINK => [
                BinkInfoExtractor::class,
            ],
            Context::MEDIA_VIDEO_BINK_VERSION_2 => [
                BinkInfoExtractor::class,
            ],

            //   Image
            Context::MEDIA_IMAGE_PNG => [
                CommonImageInfoExtractor::class,
            ],
            Context::MEDIA_IMAGE_BMP => [
                CommonImageInfoExtractor::class,
            ],
            Context::MEDIA_IMAGE_GIF => [
                CommonImageInfoExtractor::class,
            ],
            Context::MEDIA_IMAGE_JPG => [
                CommonImageInfoExtractor::class,
            ],
            Context::MEDIA_IMAGE_JPEG => [
                CommonImageInfoExtractor::class,
            ],
            Context::MEDIA_IMAGE_JFIF => [
                CommonImageInfoExtractor::class,
            ],
            Context::MEDIA_IMAGE_ASEPRITE => [
                AsepriteInfoExtractor::class,
            ],

            // Geometry
            Context::GEOMETRY_OBJ => [
                ObjInfoExtractor::class,
            ],

            // Misc
            Context::MISC_URL => [
                UrlInfoExtractor::class,
            ],
        ];
    }
}
