<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\Factory;

use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\DependencyException;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\InvalidExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\UnsupportedContextException;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\UnsupportedExtensionException;
use FlyingAnvil\Fileinfo\InfoExtractor\InfoExtractor;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use Psr\Container\ContainerInterface;

class InfoExtractorSupplier
{
    /** @var string[][] */
    private array $extensionExtractors = [];

    /** @var string[][] */
    private array $contextExtractors = [];

    public function __construct(
        private ?ContainerInterface $container = null,
    ) {
        $this->container = $container ?? new IndependentContainer();
    }

    public function addDefaultExtensionDefinitions(): void
    {
        $defaults = new DefaultDefinition();

        foreach ($defaults->getDefaultExtensionDefinition() as $rawExtension => $handlers) {
            $extension = FileExtension::createFromString($rawExtension);

            foreach ($handlers as $handler) {
                $this->addExtensionDefinition($extension, $handler);
            }
        }
    }

    public function addDefaultContextDefinitions(): void
    {
        $defaults = new DefaultDefinition();

        foreach ($defaults->getDefaultContextDefinition() as $rawContext => $handlers) {
            $context = Context::createFromString($rawContext);

            foreach ($handlers as $handler) {
                $this->addContextDefinition($context, $handler);
            }
        }
    }

    /**
     * @param FileExtension $extension
     * @param string $extractorClass
     * @return InfoExtractorSupplier
     *
     * @throws InvalidExtractorException
     */
    public function addExtensionDefinition(FileExtension $extension, string $extractorClass): self
    {
        if (!is_subclass_of($extractorClass, InfoExtractor::class)) {
            throw new InvalidExtractorException(sprintf(
                '\'%s\' need to implement %s',
                $extractorClass,
                InfoExtractor::class,
            ));
        }

        $this->extensionExtractors[$extension->getExtension()][$extractorClass] = $extractorClass;
        return $this;
    }

    /**
     * @param Context $context
     * @param string $extractorClass
     * @return InfoExtractorSupplier
     *
     * @throws InvalidExtractorException
     */
    public function addContextDefinition(Context $context, string $extractorClass): self
    {
        if (!is_subclass_of($extractorClass, InfoExtractor::class)) {
            throw new InvalidExtractorException(sprintf(
                '\'%s\' need to implement %s',
                $extractorClass,
                InfoExtractor::class,
            ));
        }

        $this->contextExtractors[$context->toString()][$extractorClass] = $extractorClass;
        return $this;
    }

    /**
     * @param FileExtension $extension
     * @return InfoExtractor[]
     *
     * @throws UnsupportedExtensionException
     * @throws DependencyException
     */
    public function getExtractorsForExtension(FileExtension $extension): array
    {
        $rawExtension     = $extension->getExtension();
        $extractorClasses = $this->extensionExtractors[$rawExtension] ?? false;

        if ($extractorClasses === false) {
            throw new UnsupportedExtensionException(sprintf(
               'Extension \'%s\' has no extractor',
                $rawExtension,
            ));
        }

        $extractors = [];
        foreach ($extractorClasses as $extractorClass) {
            if ($this->container instanceof IndependentContainer && !$this->container->has($extractorClass)) {
                throw new DependencyException(sprintf(
                    'Cannot instantiate extractor with a dependency using the default container.' .
                    ' Please specify a dependency container'
                ));
            }

            $extractors[] = $this->container->get($extractorClass);
        }

        return $extractors;
    }

    /**
     * @param Context $context
     * @return InfoExtractor[]
     *
     * @throws DependencyException
     * @throws UnsupportedContextException
     */
    public function getExtractorForContext(Context $context): array
    {
        $rawContext       = $context->toString();
        $extractorClasses = $this->contextExtractors[$rawContext] ?? false;

        if ($extractorClasses === false) {
            throw new UnsupportedContextException(sprintf(
                'Context \'%s\' has no extractor',
                $rawContext,
            ));
        }

        $extractors = [];
        foreach ($extractorClasses as $extractorClass) {
            if ($this->container instanceof IndependentContainer && !$this->container->has($extractorClass)) {
                throw new DependencyException(sprintf(
                    'Cannot instantiate extractor with a dependency using the default container.' .
                    ' Please specify a dependency container'
                ));
            }

            $extractors[] = $this->container->get($extractorClass);
        }

        return $extractors;
    }
}
