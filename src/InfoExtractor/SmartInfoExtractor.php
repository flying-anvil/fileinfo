<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor;

use FlyingAnvil\Fileinfo\InfoExtractor\DataObject\Context;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\InfoExtractionException;
use FlyingAnvil\Fileinfo\InfoExtractor\Exception\NotResponsibleExtractorException;
use FlyingAnvil\Fileinfo\InfoExtractor\Factory\InfoExtractorSupplier;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\DataObject\Collection\UniversalCollection;

class SmartInfoExtractor
{
    public function __construct(
        private InfoExtractorSupplier $infoExtractorSupplier,
    ) {}

    public function getInfoByExtension(string $filePath, FileExtension $overrideExtension = null): UniversalCollection
    {
        $this->checkIfReadable($filePath);

        $extension  = $overrideExtension ?? FileExtension::createFromFilePath($filePath);
        $extractors = $this->infoExtractorSupplier->getExtractorsForExtension($extension);

        return $this->extractInfo($filePath, $extractors);
    }

    public function getInfoByContext(string $filePath, Context $context): UniversalCollection
    {
        $this->checkIfReadable($filePath);

        $extractors = $this->infoExtractorSupplier->getExtractorForContext($context);

        return $this->extractInfo($filePath, $extractors);
    }

    public function getInfoSummaryByExtension(string $filePath, FileExtension $overrideExtension = null): UniversalCollection
    {
        $this->checkIfReadable($filePath);

        $extension  = $overrideExtension ?? FileExtension::createFromFilePath($filePath);
        $extractors = $this->infoExtractorSupplier->getExtractorsForExtension($extension);

        return $this->extractInfoSummary($filePath, $extractors);
    }

    public function getInfoSummaryByContext(string $filePath, Context $context): UniversalCollection
    {
        $this->checkIfReadable($filePath);

        $extractors = $this->infoExtractorSupplier->getExtractorForContext($context);

        return $this->extractInfoSummary($filePath, $extractors);
    }

    private function extractInfo(string $filePath, array $extractors): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        /** @var InfoExtractor[] $extractors */
        foreach ($extractors as $extractor) {
            try {
                $info = $extractor->getInfo($filePath);
            } catch (NotResponsibleExtractorException) {
                continue;
            }

            $result->merge($info);
        }

        return $result;
    }

    private function extractInfoSummary(string $filePath, array $extractors): UniversalCollection
    {
        $result = UniversalCollection::createEmpty();

        /** @var InfoExtractor[] $extractors */
        foreach ($extractors as $extractor) {
            try {
                $info = $extractor->getInfoSummary($filePath);
            } catch (NotResponsibleExtractorException) {
                continue;
            }

            $result->merge($info);
        }

        return $result;
    }

    /**
     * @param string $filePath
     * @throws InfoExtractionException
     */
    private function checkIfReadable(string $filePath): void
    {
        if (!file_exists($filePath) || !is_readable($filePath)) {
            throw new InfoExtractionException(sprintf(
                'Cannot open file (%s), does it exist?',
                $filePath,
            ));
        }
    }
}
