<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\DataObject;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\UnsupportedContextException;
use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use ReflectionClass;
use ReflectionException;
use Stringable;

final class Context implements DataObject, Stringable, StringValue
{
    // Application
    //   DevTools
    public const APPLICATION_DEVTOOLS_PATCH = 'application.devtools.patch';
    public const APPLICATION_DEVTOOLS_DIFF  = 'application.devtools.diff';

    //   Games
    public const APPLICATION_GAME_SMBX2_LEVEL       = 'application.game.smbx2.level';
    public const APPLICATION_GAME_GMOD_PACKED_MOD   = 'application.game.gmod.packedmod';
    public const APPLICATION_GAME_DIABLO2_SAVEGAME  = 'application.game.diablo2.savegame';
    public const APPLICATION_GAME_N64_SM64_SAVEGAME = 'application.game.n64.sm64.savegame';
    public const APPLICATION_GAME_SAUERBRATEN_MAP   = 'application.game.sauerbraten.ogz';

    //     ROMs
    public const APPLICATION_GAME_COMMON_M64_SEQUENCE       = 'application.game.common.m64.sequence';
    public const APPLICATION_GAME_COMMON_M64_REPLAY_CAPTURE = 'application.game.common.m64.replay.capture';

    //       ROMs
    public const APPLICATION_GAME_COMMON_WIU_EXECUTABLE    = 'application.game.common.wiiu.executalbe';
    public const APPLICATION_GAME_COMMON_SNES_SOUNDTRACK   = 'application.game.common.snes.soundtrack';
    public const APPLICATION_GAME_COMMON_SNES_ROM          = 'application.game.common.snes.rom';
    public const APPLICATION_GAME_COMMON_SFAM_ROM          = 'application.game.common.sfam.rom';
    public const APPLICATION_GAME_COMMON_N64_ROM_BIGENDIAN = 'application.game.common.n64.rom.bigendian';
    public const APPLICATION_GAME_COMMON_NDS_ROM           = 'application.game.common.nds.rom';
    public const APPLICATION_GAME_COMMON_GBA_ROM           = 'application.game.common.gba.rom';
    public const APPLICATION_GAME_COMMON_GB_ROM            = 'application.game.common.gb.rom';
    public const APPLICATION_GAME_COMMON_NES_ROM           = 'application.game.common.nes.rom';

    // Archive
    public const ARCHIVE_ZIP  = 'archive.zip';
    public const ARCHIVE_JAR  = 'archive.jar';
    public const ARCHIVE_GZIP = 'archive.gzip';

    // Media
    //   Audio
    public const MEDIA_AUDIO_EXTENDED_MODULE = 'media.audio.extendedmodule';
    public const MEDIA_AUDIO_MODULE          = 'media.audio.module';
    public const MEDIA_AUDIO_WAVE            = 'media.audio.wave';

    //   Video
    public const MEDIA_VIDEO_BINK            = 'media.video.bink';
    public const MEDIA_VIDEO_BINK_VERSION_2  = 'media.video.bink.2';

    //   Image
    public const MEDIA_IMAGE_PNG  = 'media.image.png';
    public const MEDIA_IMAGE_BMP  = 'media.image.bmp';
    public const MEDIA_IMAGE_GIF  = 'media.image.gif';
    public const MEDIA_IMAGE_JPG  = 'media.image.jpg';
    public const MEDIA_IMAGE_JPEG = 'media.image.jpeg';
    public const MEDIA_IMAGE_JFIF = 'media.image.jfif';
    public const MEDIA_IMAGE_ASEPRITE = 'media.image.aseprite';

    // Geometry
    public const GEOMETRY_OBJ = 'geometry.obj';

    // Misc
    public const MISC_URL = 'misc.url';

    public const DOCUMENT_TEXT_PLAIN = 'document.text.plain';

    private function __construct(private string $context) {}

    public static function createFromKnown(string $context): self
    {
        if (!in_array($context, self::getAllContexts(), true)) {
            throw new UnsupportedContextException(sprintf(
                'Context \'%s\' in not known',
                $context,
            ));
        }

        return new self($context);
    }

    public static function createFromString(string $context): self
    {
        return new self($context);
    }

    /**
     * @return string[]
     *
     * @throws ReflectionException
     */
    public static function getAllContexts(): array
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants();
    }

    public function toString(): string
    {
        return $this->context;
    }

    public function __toString(): string
    {
        return $this->context;
    }

    public function jsonSerialize()
    {
        return $this->context;
    }
}
