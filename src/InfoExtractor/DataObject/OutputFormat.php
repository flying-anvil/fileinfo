<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\InfoExtractor\DataObject;

use FlyingAnvil\Fileinfo\InfoExtractor\Exception\UnsupportedFormatException;
use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\Libfa\DataObject\DataObject;
use ReflectionClass;
use Stringable;

final class OutputFormat implements DataObject, StringValue, Stringable
{
    public const TABLE     = 'table';
    public const KEY_VALUE = 'key-value';
    public const JSON      = 'json';

    private function __construct(private string $format) {}

    public static function create(string $format): self
    {
        if (!in_array($format, self::getAllFormats(), true)) {
            throw new UnsupportedFormatException(sprintf(
                'Format \'%s\' in not supported',
                $format,
            ));
        }

        return new self($format);
    }

    public static function getAllFormats(): array
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants();
    }

    public function jsonSerialize(): string
    {
        return $this->format;
    }

    public function toString(): string
    {
        return $this->format;
    }

    public function __toString(): string
    {
        return $this->format;
    }
}
