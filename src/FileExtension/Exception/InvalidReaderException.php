<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension\Exception;

use FlyingAnvil\Fileinfo\Exception\FileinfoException;

class InvalidReaderException extends FileinfoException
{
}
