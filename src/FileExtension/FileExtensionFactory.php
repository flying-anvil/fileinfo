<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension;

use FlyingAnvil\Fileinfo\Exception\FileException;
use FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\FileExtensionMagicByteReader;
use FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\FileExtensionMagicByteSupplier;
use FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\SimpleMagicReader;
use FlyingAnvil\Fileinfo\InfoExtractor\Factory\IndependentContainer;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Wrapper\File;
use Psr\Container\ContainerInterface;

/**
 * https://en.wikipedia.org/wiki/List_of_file_signatures
 * and more
 *
 * TODO:
 * ☒ Script or data to be passed to the program following the shebang (#!).
 * ☒ Libpcap File Format[1]
 * ☒ PCAP Next Generation Dump File Format[2]
 * ☒ RedHat Package Manager (RPM) package [3]
 * ☒ SQLite Database [4]
 * ☒ Amazon Kindle Update Package [5]
 * ☒ Telegram Desktop File
 * ☒ Telegram Desktop Encrypted File
 * ☒ Computer icon encoded in ICO file format[6]
 * ☐ 3rd Generation Partnership Project 3GPP and 3GPP2 multimedia files
 * ☐ compressed file (often tar zip)
 * ☐ using Lempel-Ziv-Welch algorithm
 * ☐ Compressed file (often tar zip)
 * ☐ using LZH algorithm
 * ☐ File or tape containing a backup done with AmiBack on an Amiga.
 * ☐ It typically is paired with an index file (idx) with the table of contents.
 * ☐ Compressed file using Bzip2 algorithm
 * ☐ Image file encoded in the Graphics Interchange Format (GIF)[7]
 * ☐ Tagged Image File Format
 * ☐ Canon RAW Format Version 2[8]
 * ☐ Canon's RAW format is based on the TIFF file format[9]
 * ☐ Kodak Cineon image
 * ☐ Compressed file using Rob Northen Compression (version 1 and 2) algorithm
 * ☐ SMPTE DPX image
 * ☐ OpenEXR image
 * ☐ Better Portable Graphics format[10]
 * ☐ JPEG raw or in the JFIF or Exif file format
 * ☐ IFF Interleaved Bitmap Image
 * ☐ IFF 8-Bit Sampled Voice
 * ☐ Amiga Contiguous Bitmap
 * ☐ IFF Animated Bitmap
 * ☐ IFF CEL Animation
 * ☐ IFF Facsimile Image
 * ☐ IFF Formatted Text
 * ☐ IFF Simple Musical Score
 * ☐ IFF Musical Score
 * ☐ IFF YUV Image
 * ☐ Amiga Fantavision Movie
 * ☐ Audio Interchange File Format
 * ☐ Index file to a file or tape containing a backup done with AmiBack on an Amiga.
 * ☐ lzip compressed file
 * ☐ DOS MZ executable file format and its descendants (including NE and PE)
 * ☐ zip file format and formats based on it, such as EPUB, JAR, ODF, OOXML
 * ☐ RAR archive version 1.50 onwards[11]
 * ☐ RAR archive version 5.0 onwards[12]
 * ☐ DOS ZM executable file format and its descendants (rare)
 * ☐ Executable and Linkable Format
 * ☐ Image encoded in the Portable Network Graphics format[13]
 * ☐ CP/M 3 and higher with overlays[14]
 * ☐ Java class file, Mach-O Fat Binary
 * ☐ UTF-8 encoded Unicode byte order mark, commonly seen in text files.
 * ☐ Mach-O binary (32-bit)
 * ☐ Mach-O binary (64-bit)
 * ☐ JKS JavakeyStore
 * ☐ Mach-O binary (reverse byte ordering scheme, 32-bit)[15]
 * ☐ Mach-O binary (reverse byte ordering scheme, 64-bit)[15]
 * ☐ Byte-order mark for text file encoded in little-endian 16-bit Unicode Transfer Format
 * ☐ Byte-order mark for text file encoded in little-endian 32-bit Unicode Transfer Format
 * ☐ PostScript document
 * ☐ MS Windows HtmlHelp Data
 * ☐ PDF document[16]
 * ☐ Advanced Systems Format[17]
 * ☐ System Deployment Image, a disk image format used by Microsoft
 * ☐ Ogg, an open source media container format
 * ☐ Photoshop Document file, Adobe Photoshop's native file format
 * ☐ Waveform Audio File Format
 * ☐ Audio Video Interleave video format
 * ☐ MPEG-1 Layer 3 file without an ID3 tag or with an ID3v1 tag (which's appended at the end of the file)
 * ☐ MP3 file with an ID3v2 container
 * ☐ BMP file, a bitmap format used mostly in the Windows world
 * ☐ ISO9660 CD/DVD image file[18]
 * ☐ Flexible Image Transport System (FITS)[19]
 * ☐ Free Lossless Audio Codec[20]
 * ☐ MIDI sound file[21]
 * ☐ Compound File Binary Format, a container format used for document by older versions of Microsoft Office.[22] It is however an open format used by other programs as well.
 * ☐ Dalvik Executable
 * ☐ VMDK files[23][24]
 * ☐ Google Chrome extension[25] or packaged app[26]
 * ☐ FreeHand 8 document[27][28][29]
 * ☐ AppleWorks 5 document
 * ☐ AppleWorks 6 document
 * ☐ Roxio Toast disc image file, also some .dmg-files begin with same bytes
 * ☐ Apple Disk Image file
 * ☐ eXtensible ARchive format[30]
 * ☐ Windows Files And Settings Transfer Repository[31]
 * ☐ See also USMT 3.0 (Win XP)[32] and USMT 4.0 (Win 7)[33] User Guides
 * ☐ Nintendo Entertainment System ROM file[34]
 * ☐ tar archive[35]
 * ☐ OAR file archive format, where ?? is the format version.
 * ☐ Open source portable voxel file[36]
 * ☐ Magic Lantern Video file[37]
 * ☐ Windows Update Binary Delta Compression[38]
 * ☐ 7-Zip File Format
 * ☐ GZIP compressed file[39]
 * ☐ XZ compression utility
 * ☐ using LZMA2 compression
 * ☐ LZ4 Frame Format[40]
 * ☐ Remark: LZ4 block format does not offer any magic bytes.[41]
 * ☐ Microsoft Cabinet file
 * ☐ Microsoft compressed file in Quantum format, used prior to Windows XP. File can be decompressed using Extract.exe or Expand.exe distributed with earlier versions of Windows.
 * ☐ Free Lossless Image Format
 * ☐ Matroska media container, including WebM
 * ☐ "SEAN : Session Analysis" Training file. Also used in compatible software "Rpw : Rowperfect for Windows" and "RP3W : ROWPERFECT3 for Windows".
 * ☐ DjVu document
 * ☐ The following byte is either 55 (U) for single-page or 4D (M) for multi-page documents.
 * ☐ DER encoded X.509 certificate
 * ☐ DICOM Medical File Format
 * ☐ WOFF File Format 1.0
 * ☐ WOFF File Format 2.0
 * ☐ eXtensible Markup Language when using the ASCII character encoding
 * ☐ WebAssembly binary format[42]
 * ☐ Lepton compressed JPEG image[43]
 * ☐ flash .swf
 * ☐ linux deb file
 * ☐ Google WebP image file, where ?? ?? ?? ?? is the file size. More informarion on WebP File Header
 * ☐ U-Boot / uImage. Das U-Boot Universal Boot Loader.[44]
 * ☐ Rich Text Format
 * ☐ Microsoft Tape Format
 * ☐ MPEG Transport Stream (MPEG-2 Part 1)
 * ☐ MPEG Program Stream (MPEG-1 Part 1 (essentially identical) and MPEG-2 Part 1)
 * ☐ MPEG Program Stream
 * ☐ MPEG Transport Stream
 * ☐ MPEG-1 video and MPEG-2 video (MPEG-1 Part 2 and MPEG-2 Part 2)
 * ☐ No Compression (no preset dictionary)
 * ☐ Best speed (no preset dictionary)
 * ☐ Default Compression (no preset dictionary)
 * ☐ Best Compression (no preset dictionary)
 * ☐ No Compression (with preset dictionary)
 * ☐ Best speed (with preset dictionary)
 * ☐ Default Compression (with preset dictionary)
 * ☐ Best Compression (with preset dictionary)
 * ☐ LZFSE - Lempel-Ziv style data compression algorithm using Finite State Entropy coding. OSS by Apple.[45]
 * ☐ Apache ORC (Optimized Row Columnar) file format
 * ☐ Apache Avro binary file format
 * ☐ RCFile columnar file format
 * ☐ PhotoCap Object Templates
 * ☐ PhotoCap Vector
 * ☐ PhotoCap Template
 * ☐ Apache Parquet columnar file format
 * ☐ Emulator Emaxsynth samples
 * ☐ Emulator III synth samples
 * ☐ Lua bytecode[46]
 * ☐ macOS file Alias[47] (Symbolic link)
 * ☐ Microsoft Zone Identifier for URL Security Zones[48]
 * ☐ Email Message var5[49]
 * ☐ Tableau Datasource
 * ☐ KDB file
 * ☐ PGP file
 * ☐ Zstandard compressed file[50][51]
 * ☐ Smile file
 * ☐ Preferred Executable Format
 * ☐ SubRip File
 */
class FileExtensionFactory
{
    public function __construct(
        private FileExtensionMagicByteSupplier $supplier,
        private ?ContainerInterface $container = null,
    ) {
        $this->container = $container ?? new IndependentContainer();
    }

    public function createFromMagicBytes(string $filePath): ?FileExtension
    {
        $this->checkIfReadable($filePath);

        $file = File::load($filePath);
        $file->open();

        $simpleReader = new SimpleMagicReader($this->supplier->getSimpleMagicDefinition());
        $extension    = $simpleReader->readExtension($file);

        if ($extension !== null) {
            return $extension;
        }

        foreach ($this->supplier->getReaderDefinitions() as $readerClass) {
            /** @noinspection DisconnectedForeachInstructionInspection */
            $file->rewind();

            /** @var FileExtensionMagicByteReader $reader */
            $reader = $this->container->get($readerClass);

            $extension = $reader->readExtension($file);

            if ($extension !== null) {
                return $extension;
            }
        }

        return null;
    }

    private function checkIfReadable(string $filePath): void
    {
        if (!file_exists($filePath) || !is_readable($filePath)) {
            throw new FileException(sprintf(
                'Cannot open file (%s), does it exist?',
                $filePath,
            ));
        }
    }
}
