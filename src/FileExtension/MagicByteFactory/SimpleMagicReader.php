<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory;

use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Wrapper\File;

class SimpleMagicReader implements FileExtensionMagicByteReader
{
    /** @var string[] */
    private array $simpleMagic;

    public function __construct(array $simpleMagic)
    {
        $this->simpleMagic = $simpleMagic;
    }

    public function readExtension(File $file): ?FileExtension
    {
        $chunk = $file->read(4096);
        $file->rewind();

        foreach ($this->simpleMagic as $expectedMagic => $extension) {
            $magicLength = strlen($expectedMagic);
            $readMagic   = substr($chunk, 0, $magicLength);

            if ($expectedMagic === $readMagic) {
                return FileExtension::createFromString($extension);
            }
        }

        return null;
    }
}
