<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory;

use FlyingAnvil\Fileinfo\FileExtension\Exception\InvalidReaderException;
use FlyingAnvil\Fileinfo\FileExtension\KnownExtensions;
use FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\Reader\MagicReaderZlib;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\DevTools\PatchInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\N64\M64ReplayCaptureInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Nes\NesInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Common\Snes\SpcInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Diablo2\Diablo2SaveInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Application\Game\Gmod\PackedModInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive\GzInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Archive\ZipInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Audio\ExtendedModuleInfoExtractor;
use FlyingAnvil\Fileinfo\InfoExtractor\Extractor\Media\Video\BinkInfoExtractor;

class FileExtensionMagicByteSupplier
{
    private const DEFAULT_READERS = [
        MagicReaderZlib::class,
    ];

    public const DEFAULT_SIMPLE_MAGIC = [
        "HEAD\nTL:\""                         => KnownExtensions::APPLICATION_GAME_SMBX2_LEVEL,
        "\x7fELF\x01\x02\x01\xCA\xFE"         => KnownExtensions::APPLICATION_GAME_COMMON_WIU_EXECUTABLE,
        SpcInfoExtractor::SPC_MAGIC           => KnownExtensions::APPLICATION_GAME_COMMON_SNES_SOUNDTRACK,
        PackedModInfoExtractor::GMA_MAGIC     => KnownExtensions::APPLICATION_GAME_GMOD_PACKED_MOD,
        NesInfoExtractor::NES_MAGIC           => KnownExtensions::APPLICATION_GAME_COMMON_NES_ROM,

        ExtendedModuleInfoExtractor::XM_MAGIC => KnownExtensions::MEDIA_AUDIO_EXTENDED_MODULE,

        BinkInfoExtractor::BINK_MAGIC_BIK => KnownExtensions::MEDIA_VIDEO_BINK,
        BinkInfoExtractor::BINK_MAGIC_KB2 => KnownExtensions::MEDIA_VIDEO_BINK_VERSION_2,

        Diablo2SaveInfoExtractor::D2S_MAGIC => KnownExtensions::APPLICATION_GAME_DIABLO2_SAVEGAME,

        PatchInfoExtractor::PATCH_MAGIC => KnownExtensions::APPLICATION_DEVTOOLS_PATCH,

        M64ReplayCaptureInfoExtractor::M64_MAGIC => KnownExtensions::APPLICATION_GAME_COMMON_M64_REPLAY_CAPTURE,

        ZipInfoExtractor::ZIP_MAGIC => 'zip',
        GzInfoExtractor::GZ_MAGIC   => 'gz',

        // smc => no magic :(
        // sfc => no magic :(
        // z64 => no magic :(
        // nds => no magic :(

        '<?php'        => 'php',
        "Rar!\x1A\x07" => 'rar',

        // BPS Patch
        'BPS1' => 'bps',

        // https://en.wikipedia.org/wiki/List_of_file_signatures
        '#!/'                       => 'sh',
        "\xA1\xB2\xC3\xD4"          => 'pcap', // Libpcap File Format
        "\xD4\xC2\xB2\xA1"          => 'pcap',
        "\x0A\x0D\x0D\x0A"          => 'pcapng', // PCAP Next Generation Dump File Format
        "\xED\xAB\xEE\xDB"          => 'rpm',    // RedHat Package Manager (RPM) package
        "SQLite format 3\0"         => 'sqlite',
        'SP01'                      => 'bin',  // Amazon Kindle Update Package
        'TDF$'                      => 'TDF$', // Telegram Desktop File
        'TDEF'                      => 'TDEF', // Telegram Desktop Encrypted File
        "\0\0\x01\0"                => 'ico',
        "\x1F\x9D"                  => 'z', // compressed file (often tar zip)
        "\x1F\xA0"                  => 'z',
        'BACKMIKEDISK'              => 'bac', // File or tape containing a backup done with AmiBack on an Amiga
        'INDX'                      => 'idx', // Index file to a file or tape containing a backup done with AmiBack on an Amiga
        'BZh'                       => 'bz2', // Compressed file using Bzip2 algorithm
        'GIF87a'                    => 'gif',
        'GIF89a'                    => 'gif',
        "\x49\x49\x2A\x00"          => 'tif', // Tagged Image File Format
        "\x4D\x4D\x00\x2A"          => 'tif', // Tagged Image File Format
        "\x49\x49\x2A\0\x10\0\0\0"  => 'cr2', // Canon RAW Format Version
        "\x43\x52"                  => 'cr2',
        "\x80\x2A\x5F\xD7"          => 'cin', // Kodak Cineon image
        'SDPX'                      => 'dpx', // SMPTE DPX image
        'XPDS'                      => 'dpx',
        "v/1\x01"                   => 'exr', // OpenEXR image
        "BPG\xFB"                   => 'bpg', // Better Portable Graphics format
        "\xFF\xD8\xFF"              => 'jpg',
        'LZIP'                      => 'lz',  // lzip compressed file
        'MZ'                        => 'dll', // DOS MZ executable or library and its descendants (including NE and PE)
        'ZM'                        => 'exe', // DOS ZM executable file format and its descendants (rare)
        "\x7fELF"                   => 'elf',
        "\x89PNG\x0D\x0A\x1A\x0A"   => 'png',
        "\xC9"                      => 'com',
        "\xCA\xFE\xBA\xBE"          => 'class', // Java class file (CAFE BABE)
        '%!PS'                      => 'ps', // PostScript document
        "\x49\x54\x53\x46\x03"      => 'chm', // MS Windows HtmlHelp Data
        '%PDF-'                     => 'pdf',
        "0&²u\x8EfÏ\xA6¦Ù\0ª\0bÎl"  => 'asf', // Advanced Systems Format (or wma, wmv)
        'OggS'                      => 'ogg',
        '8BPS'                      => 'psd',
        "\xFF\xFB"                  => 'mp3',
        "\xFF\xF3"                  => 'mp3',
        "\xFF\xF2"                  => 'mp3',
        'ID3'                       => 'mp3',
        'BM'                        => 'bmp',
        'fLaC'                      => 'flac', // Free Lossless Audio Codes
        'FLIF'                      => 'flif', // Free Lossless Image Codes
        'MThd'                      => 'mid',
        "dex\x0A035\0"              => 'dex',  // Dalvik Executable
        'KDM'                       => 'vmdk', // VMWare Virtual Machine Disk
        'Cr24'                      => 'crx',  // Google Chrome extension or packaged app
        'AGD3'                      => 'fh8',  // FreeHand 8 document
        'xar!'                      => 'xar',  // eXtensible ARchive format
        'PMOCCMOC'                  => 'dat',  // Windows Files And Settings Transfer Repository
        'OAR'                       => 'oar',  // OAR file archive format
        'TOX'                       => 'tox',  // Open source portable voxel file
        'MLVI'                      => 'MLV',  // Magic Lantern Video file
        "7z\xBC\xAF\x27\x1c"        => '7z',
        "\xFD\x37\x7A\x58\x5A\x00"  => 'xz',   // XZ compression utility
        "\x04\x22\x4D\x18"          => 'lz4',  // LZ4 Frame Format
        'MSCF'                      => 'cab',  // Microsoft Cabinet file
        "\x1A\x45\xDF\xA3"          => 'mkv',  // Matroska media container (or mka, mks, mk3d, webm)
        'AT&TFORM'                  => 'djvu', // DjVu document (maybe drop it?)
        "\x30\x82"                  => 'der',  // DER encoded X.509 certificate
        'wOFF'                      => 'woff',
        'wOF2'                      => 'woff2',
        '<?xml '                    => 'xml',
        "\0asm"                     => 'wasm',
        "\xcf\x84\x01"              => 'lep', // Lepton compressed JPEG image
        'CWS'                       => 'swf',
        'FWS'                       => 'swf',
        "!\x3Carch\x3E"             => 'deb',
        '{\rtf1'                    => 'rtf',
        "\x00\x00\x01\xBA"          => 'mpg',
        "\x00\x00\x01\xB3"          => 'mpg',
        'bvx2'                      => 'lzfse', // Lempel-Ziv data compression algo with Finite State Entropy coding
        'ORC'                       => 'orc',   // Apache ORC (Optimized Row Columnar) file format
        "Obj\x01"                   => 'avro',  // Apache Avro binary file format
        'SEQ6'                      => 'rc',    // RCFile columnar file format
        "\x65\x87\x78\x56"          => 'p25',   // PhotoCap Object Templates
        "\x55\x55\xAA\xAA"          => 'pcv',   // PhotoCap Vector
        'EMX2'                      => 'ez2',   // Emulator Emaxsynth samples
        'EMU3'                      => 'ez3',   // Emulator III synth samples
        "\x1BLua"                   => 'luac',  // Lua bytecode
        'Received:'                 => 'eml',   // Email Message var5
        "7H\x03\x02\0\0\0\0X509KEY" => 'kdb',   // KeePass Data Base
        "\x85\x01\x0C\x03"          => 'pgp',
        '(µ/ý'                      => 'zst', // Zstandard compressed file
        ":)\x0A"                    => 'sml', // Smile file
        'Joy!'                      => 'pef', // Preferred Executable Format
    ];

    /** @var string[] */
    private array $readers = [];

    /** @var string[] */
    private array $simpleMagic = [];

    public function addDefaultReaderDefinitions(): void
    {
        foreach (self::DEFAULT_READERS as $reader) {
            $this->addReaderDefinition($reader);
        }
    }

    public function addReaderDefinition(string $extractorClass): self
    {
        if (!is_subclass_of($extractorClass, FileExtensionMagicByteReader::class)) {
            throw new InvalidReaderException(sprintf(
                '\'%s\' needs to implement %s',
                $extractorClass,
                FileExtensionMagicByteReader::class,
            ));
        }

        $this->readers[$extractorClass] = $extractorClass;
        return $this;
    }

    public function addDefaultSimpleDefinitions(): void
    {
        foreach (self::DEFAULT_SIMPLE_MAGIC as $magic => $extension) {
            $this->addSimpleDefinition($magic, $extension);
        }
    }

    public function addSimpleDefinition(string $magic, string $extension): self
    {
        $this->simpleMagic[$magic] = $extension;
        return $this;
    }

    public function getReaderDefinitions(): array
    {
        return $this->readers;
    }

    public function getSimpleMagicDefinition(): array
    {
        return $this->simpleMagic;
    }
}
