<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\Reader;

use FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory\FileExtensionMagicByteReader;
use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Wrapper\File;

class MagicReaderZlib implements FileExtensionMagicByteReader
{
    private const MAGICS = [
        "\x78\x01" => true, // No Compression (no preset dictionary)
        "\x78\x5E" => true, // Best speed (no preset dictionary)
        "\x78\x9C" => true, // Default Compression (no preset dictionary)
        "\x78\xDA" => true, // Best Compression (no preset dictionary)
        "\x78\x20" => true, // No Compression (with preset dictionary)
        "\x78\x7D" => true, // Best speed (with preset dictionary)
        "\x78\xBB" => true, // Default Compression (with preset dictionary)
        "\x78\xF9" => true, // Best Compression (with preset dictionary)
    ];

    public function readExtension(File $file): ?FileExtension
    {
        $magic = $file->read(2);

        if (isset(self::MAGICS[$magic])) {
            return FileExtension::createFromString('zlib');
        }

        return null;
    }
}
