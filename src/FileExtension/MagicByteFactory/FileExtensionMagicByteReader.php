<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension\MagicByteFactory;

use FlyingAnvil\Libfa\DataObject\FileExtension;
use FlyingAnvil\Libfa\Wrapper\File;

interface FileExtensionMagicByteReader
{
    /**
     * @param File $file
     *
     * @return FileExtension
     */
    public function readExtension(File $file): ?FileExtension;
}
