<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\FileExtension;

use ReflectionClass;
use ReflectionClassConstant;

class KnownExtensions
{
    // Application
    //   DevTools
    public const APPLICATION_DEVTOOLS_PATCH = 'patch';
    public const APPLICATION_DEVTOOLS_DIFF  = 'diff';

    //   Games
    public const APPLICATION_GAME_SMBX2_LEVEL      = 'lvlx';
    public const APPLICATION_GAME_GMOD_PACKED_MOD  = 'gma';
    public const APPLICATION_GAME_DIABLO2_SAVEGAME = 'd2s';
    public const APPLICATION_GAME_SAUERBRATEN_MAP  = 'ogz';

    //     Common
    public const APPLICATION_GAME_COMMON_M64_SEQUENCE       = 'm64';
    public const APPLICATION_GAME_COMMON_M64_REPLAY_CAPTURE = 'm64';

    //       ROMs
    public const APPLICATION_GAME_COMMON_WIU_EXECUTABLE    = 'rpx';
    public const APPLICATION_GAME_COMMON_SNES_SOUNDTRACK   = 'spc';
    public const APPLICATION_GAME_COMMON_SNES_ROM          = 'smc';
    public const APPLICATION_GAME_COMMON_SFAM_ROM          = 'sfc';
    public const APPLICATION_GAME_COMMON_N64_ROM_BIGENDIAN = 'z64';
    public const APPLICATION_GAME_COMMON_N64_EEPROM        = 'eep';
    public const APPLICATION_GAME_COMMON_NDS_ROM           = 'nds';
    public const APPLICATION_GAME_COMMON_GBA_ROM           = 'gba';
    public const APPLICATION_GAME_COMMON_GB_ROM            = 'gb';
    public const APPLICATION_GAME_COMMON_NES_ROM           = 'nes';

    // Archive
    public const ARCHIVE_ZIP = 'zip';
    public const ARCHIVE_JAR = 'jar';
    public const ARCHIVE_GZ  = 'gz';

    // Media
    //   Audio
    public const MEDIA_AUDIO_EXTENDED_MODULE = 'xm';
    public const MEDIA_AUDIO_MODULE          = 'mod';
    public const MEDIA_AUDIO_WAVE            = 'wav';

    //   Video
    public const MEDIA_VIDEO_BINK            = 'bik';
    public const MEDIA_VIDEO_BINK_VERSION_2  = 'bk2';

    //   Image
    public const MEDIA_IMAGE_PNG  = 'png';
    public const MEDIA_IMAGE_BMP  = 'bmp';
    public const MEDIA_IMAGE_GIF  = 'gif';
    public const MEDIA_IMAGE_JPG  = 'jpg';
    public const MEDIA_IMAGE_JPEG = 'jpeg';
    public const MEDIA_IMAGE_JFIF = 'jfif';
    public const MEDIA_IMAGE_ASEPRITE = 'aseprite';

    // Geometry
    public const GEOMETRY_OBJ = 'obj';

    // Misc
    public const MISC_URL = 'url';

    public const DOCUMENT_TEXT_PLAIN = 'txt';

    public static function getAllKnownExtension(): array
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants(ReflectionClassConstant::IS_PUBLIC);
    }
}
