<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Formatter;

use FlyingAnvil\Libfa\DataObject\Collection\Collection;

interface KeyValueFormatter
{
    public function formatKeyValue(Collection $data): string;
}
