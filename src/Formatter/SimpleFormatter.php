<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Formatter;

use FlyingAnvil\Libfa\DataObject\Collection\Collection;
use JetBrains\PhpStorm\Deprecated;

/**
 * @internal Might be moved to flying-anvil/libfa
 * @deprecated Has been moved and revamped to libfa
 */
#[Deprecated('Has been moved and revamped to libfa', \FlyingAnvil\Libfa\Utils\Formatter\KeyValueFormatter::class)]
class SimpleFormatter implements KeyValueFormatter
{
    public function formatKeyValue(Collection $data): string
    {
        $rows = [];

        foreach ($data as $key => $value) {
            $rows[] = $key . ': ' . $value;
        }

        return implode(PHP_EOL, $rows);
    }
}
