<?php

declare(strict_types=1);

namespace FlyingAnvil\Fileinfo\Formatter;

use FlyingAnvil\Libfa\DataObject\Collection\Collection;
use JetBrains\PhpStorm\Deprecated;

/**
 * @internal Might be moved to flying-anvil/libfa
 * @deprecated Has been moved and revamped to libfa
 */
#[Deprecated('Has been moved and revamped to libfa', \FlyingAnvil\Libfa\Utils\Formatter\TableFormatter::class)]
class TableFormatter implements KeyValueFormatter
{
    public function formatKeyValue(Collection $data): string
    {
        $keys   = [];
        $values = [];

        $longestKey   = 0;
        $longestValue = 0;

        foreach ($data as $key => $value) {
            $keys[]   = (string)$key;
            $values[] = $this->makeString($value);

            $longestKey   = max($longestKey, mb_strlen((string)$key));
            $longestValue = max($longestValue, mb_strlen($this->makeString($value)));
        }

        $count = count($keys);
        $body  = sprintf(
            '╭%s┬%s╮',
            str_repeat('─', $longestKey + 2),
            str_repeat('─', $longestValue + 2),
        ) . PHP_EOL;

        for ($i = 0; $i < $count; $i++) {
            $key   = $keys[$i];
            $value = $values[$i];

            $body .= sprintf(
                '│ %s ├ %s │',
                $this->mbStrPad($key, $longestKey),
                $this->mbStrPad($value, $longestValue),
            ) . PHP_EOL;
        }

        $body .= sprintf(
            '╰%s┴%s╯',
            str_repeat('─', $longestKey + 2),
            str_repeat('─', $longestValue + 2),
        );

        return $body;
    }

    private function mbStrPad(string $input, int $padLength): string
    {
        $length  = mb_strlen($input);
        $missing = $padLength - $length;

        return $input . str_repeat(' ', $missing);
    }

    private function makeString($in): string
    {
        if (is_bool($in)) {
            return $this->boolToString($in);
        }

        if (is_iterable($in)) {
            return '*multi-value*';
        }

        return (string)$in;
    }

    private function boolToString(bool $bool): string
    {
        return $bool
            ? 'true'
            : 'false';
    }
}
