#!/usr/bin/env php
<?php

use FlyingAnvil\Fileinfo\Samples\SampleDownloader;
use FlyingAnvil\Libfa\DataObject\FileExtension;

require_once __DIR__ . '/../vendor/autoload.php';

if ($argc === 1) {
    echo 'No extension specified!', PHP_EOL;
    exit(1);
}

$downloader   = new SampleDownloader();
$rawExtension = $argv[$argc - 1];

if ($rawExtension === '--all') {
    $downloader->downloadAll();
    return;
}

$extension = FileExtension::createFromFilePath('.' . $rawExtension);

$downloader->download($extension);
