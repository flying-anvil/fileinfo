<?php

$home = getenv('HOME');

$spcFiles = scandir($home . '/shared/spc');
unset($spcFiles[0], $spcFiles[1]);

foreach ($spcFiles as $spcFile) {
    $fullPath = $home . '/shared/spc/' . $spcFile;
    $command  = 'php bin/fileinfo.php "' . $fullPath . '"';
    echo $command, PHP_EOL;
    passthru($command);
}
